#!/bin/bash
#
# Qt for Android swiss army knife script
#
# Author: Sergey A. Galin <sergey.galin@gmail.com>
#
# Distrbuted under The BSD License
#
# Copyright (c) 2010, 2011, DoubleGIS, LLC.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of the DoubleGIS, LLC.  nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#

# Some hard-coded configuration options. Please note that:
# - Only android-5 and NDK 4 are officially supported.
# - If you change anything, you have to edit mkspecs/android/qmakeXXX.conf files accordingly.
# However, it is not recommended to change these settings for now.

# 4 - android 1.6
# 5 - android 2.0 & 2.1
# 8 - android 2.2
# 9 - android 2.3

PLATFORM=android-4

# NDK_VERSION: 4 or 5 for now (QADK and patched Crystax based on NDK4 are also "4")
NDK_VERSION=4

ADB=`which adb`

PATH_ADD=

# Used by paramvalue(), don't use it
function reconcat () {
  shift
  RECONCATTEXT=
  RECONCATFIRST=1
  for X in $* ; do
    if [ "$RECONCATFIRST" = "1" ] ; then
      RECONCATFIRST=
      RECONCATTEXT="$X" ;
    else
      RECONCATTEXT="$RECONCATTEXT=$X" ;
    fi
  done
  echo $RECONCATTEXT
}

# Used by paramnname()
function firstparam () {
  echo "$1"
}

# Extract parameter value from a string like "--option=value"
function paramvalue () {
  PARAMVALUESPLIT=`echo "$1" | tr "=" "\n"`
  echo `reconcat $PARAMVALUESPLIT`
}

# Extract parameter name from a string like "--option=value"
function paramname () {
  PARAMVALUESPLIT=`echo "$1" | tr "=" "\n"`
  echo `firstparam $PARAMVALUESPLIT`
}

#
# Finds program to strip binaries.
#
# TODO: find strip for appropriate hardware platform and toolchain version!
#
function findarmeabistrip () {
  ARMEABISTRIP=
  if [ ! "$ANDROID_NDK_ROOT" = "" ] ; then
    #
    # Try to find strip in ANDROID_NDK_ROOT (should point to NDK root - C.O.)
    # This is the most current branch and the recommended method.
    #
    ARMEABISTRIP="$ANDROID_NDK_ROOT/toolchains/arm-linux-androideabi-4.4.3/prebuilt/linux-x86/bin/arm-linux-androideabi-strip" ;
    #if [ ! -x "$ARMEABISTRIP" ] ; then
    #  echo "Warning: ANDROID_NDK_ROOT is set but doesn't contain arm-linux-androideabi-strip executable!" ;
    #fi
  fi

  if [ ! -x "$ARMEABISTRIP" ] ; then
    if [ ! "$ANDROID_NDK_PATH" = "" ] ; then
      #
      # Try to find strip in ANDROID_NDK_PATH (should point to NDK executable file path).
      # This is kept for compatibility with older NDK4-based system.
      #
      ARMEABISTRIP="$ANDROID_NDK_PATH/bin/arm-eabi-strip"
      #if [ ! -x "$ARMEABISTRIP" ] ; then
      #  echo "Warning: ANDROID_NDK_PATH is set but doesn't contain arm-eabi-strip executable!" ;
      #fi
    fi
  fi

  #
  # Try to find strip in PATH via "which"
  #
  if [ ! -x "$ARMEABISTRIP" ] ; then
    ARMEABISTRIP=`which arm-linux-androideabi-strip`
    if [ ! -x "$ARMEABISTRIP" ] ; then
      ARMEABISTRIP=`which arm-eabi-strip` ;
    fi
  fi

  #
  # Checking for various known locations.
  #
  if [ ! -x "$ARMEABISTRIP" ] ; then
    ARMEABISTRIP="/usr/local/android-ndk-r8b/toolchains/arm-linux-androideabi-4.4.3/prebuilt/linux-x86/bin/arm-linux-androideabi-strip" ;
  fi
  if [ ! -x "$ARMEABISTRIP" ] ; then
    ARMEABISTRIP="/usr/local/android-ndk-r4-crystax/build/prebuilt/linux-x86/arm-eabi-4.4.0/bin/arm-eabi-strip" ;
  fi
  if [ ! -x "$ARMEABISTRIP" ] ; then
    echo "Searching for strip failed. For your reference:"
    echo "  ANDROID_NDK_ROOT=$ANDROID_NDK_ROOT"
    echo "  ANDROID_NDK_PATH=$ANDROID_NDK_PATH"
    exit 1 ;
  fi
  echo "$ARMEABISTRIP"
}

# Collect and strip Android Qt libraries, the only parameter can be set to "push"
# to push the libraries to default device/emulator, or to "rm" to remove the libs from
# device.
function androlibs () {
  if [ ! -d "$PREFIX" ] ; then
    echo "Prefix does not correspond to an existing directory: $PREFIX"
    exit 1 ;
  fi

  if [ ! -x "$ADB" ] ; then
    echo "adb is not in PATH! $ADB"
    exit 1 ;
  fi

  PUSHLIBS="$1"

  if [ "$PUSHLIBS" = "rm" ] ; then
    echo "Removing libs from default device/emulator..."
    adb shell rm -r /data/local/qt
    exit 0 ;
  fi

  ARMEABISTRIP=`findarmeabistrip`
  if [ ! -x "$ARMEABISTRIP" ] ; then
    echo "// ANDROID_NDK_PATH=\"$ANDROID_NDK_PATH\""
    echo "Failed to find strip for ARM - will not prepare Qt libs for devices!"
    exit 1 ;
  fi

  echo "Preparing Qt libs..."

  mkdir -p "$PREFIX/andlibs/lib" || exit 1
  mkdir -p "$PREFIX/andlibs/imports" || exit 1
  mkdir -p "$PREFIX/andlibs/plugins" || exit 1
  cp -Lf $PREFIX/lib/*.so "$PREFIX/andlibs/lib"
  cp -rf $PREFIX/imports "$PREFIX/andlibs/"
  cp -rf $PREFIX/plugins "$PREFIX/andlibs/"

  SOFILELIST=`find $PREFIX/andlibs | grep .so`

  du -hs "$PREFIX/andlibs"
  for LIB in $SOFILELIST ; do
    echo "Stripping $LIB..."
    $ARMEABISTRIP --strip-all $LIB ;
  done
  du -hs "$PREFIX/andlibs"

  if [ "$PUSHLIBS" = "push" ] ; then
    echo "Pushing Qt libs to default device/emulator..."
    SAVEDIR="$PWD"
    adb shell rm -r /data/local/qt
    adb shell mkdir /data/local/qt
    cd "$PREFIX/andlibs" || exit 1
    adb push lib /data/local/qt/lib
    adb push imports /data/local/qt/imports
    adb push plugins /data/local/qt/plugins
    cd "$SAVEDIR" || exit 1
  fi
}

# Update Android plugins on target path and on device.
function androplugins () {
  if [ ! -d "$PREFIX" ] ; then
    echo "Prefix does not correspond to an existing directory: $PREFIX"
    exit 1 ;
  fi
  if [ ! -x "$ADB" ] ; then
    echo "adb is not in PATH! $ADB"
    exit 1 ;
  fi
  ARMEABISTRIP=`findarmeabistrip`
  if [ ! -x "$ARMEABISTRIP" ] ; then
    echo "Failed to find strip for ARM - cannot update Android plugins!"
    exit 1 ;
  fi
  APLIST="libQAndroidCore.so libQwpLiteApi5.so libQwpLiteApi8.so"
  if [ "$SHADOW" = "." ] || [ "$SHADOW" = "" ] ; then
    MAKEDIR="src/plugins/platforms/android"
    APSRCDIR="lib" ;
  else
    MAKEDIR="../$SHADOW/src/plugins/platforms/android"
    APSRCDIR="../$SHADOW/lib" ;
  fi

  if [ ! -d "$APSRCDIR" ] ; then
    echo "Directory not found: $APSRSDIR"
    exit 1 ;
  fi
  if [ ! -d "$MAKEDIR" ] ; then
    echo "Directory not found: $MAKEDIR"
    exit 1 ;
  fi

  SAVEDIR="$PWD"
  cd "$MAKEDIR" || exit 1
  make || exit 1
  cd "$SAVEDIR" || exit 1

  echo ""
  echo "**************************************************************"
  echo "*** Updating Android libraries..."
  echo "**************************************************************"
  echo ""

  echo "libQAndroidEntryPoint.a..."
  cp -f "$APSRCDIR/libQAndroidEntryPoint.a" "$PREFIX/lib/libQAndroidEntryPoint.a"
  mkdir -p "$PREFIX/andlibs/lib"
  for X in $APLIST ; do
    if [ -f "$APSRCDIR/$X" ] ; then
        echo "$X..."
        cp -fv "$APSRCDIR/$X" "$PREFIX/lib/$X" ;
        cp -fv "$APSRCDIR/$X" "$PREFIX/andlibs/lib/$X" ;
        echo "...stripping..."
        "$ARMEABISTRIP" --strip-all "$PREFIX/andlibs/lib/$X"
        echo "...pushing..."
        adb push "$PREFIX/andlibs/lib/$X" "/data/local/qt/lib/$X"
        echo "" ;
    else
        echo "$X: not found (not built?)" ;
    fi
  done
  echo ""
  echo "Done."
}


# Print usage information
function help () {
  echo "Qt for Android configuration & utility script"
  echo "Written by: Sergey A. Galin, 2010-2011"
  echo ""
  echo "USAGE: ./android.sh <configuration or command> [options]"
  echo ""
  echo "Configurations:"
  echo ""
  echo "    std        - normal build without WebKit/scripting/Declarative support."
  echo "    full       - all supported options on."
  echo "    light      - fewer Qt modules, excludes Declarative and these unlikely"
  echo "                 to be used by current Qt/Android apps."
  echo "    min        - extremely lightweight build for apps which use only minimal"
  echo "                 Qt layer (without most standard wigets)."
  echo "                 Recommended for applications with QGraphicsView-based UI."
  echo ""
  echo "By default, specifying a configuration name invokes 'configure' script for"
  echo "the selected configuration, but you can also have Qt compiled by adding"
  echo "--build, or compiled and installed via --install, or disable actual doing"
  echo "of anything via --test."
  echo ""
  echo "Commands:"
  echo ""
  echo "    preplibs   - prepare Qt libraries for distribution in <INSTPREFIX>/andlibs"
  echo "    pushlibs   - push all compiled Qt libraries to default device/emulator;"
  echo "    rmlibs     - remove Qt libraries from default device/emulator;"
  echo "    plugins    - make/install/push Android plugins only (for plugin developers) -"
  echo "                 please specify shadow build directory if you use one."
  echo ""
  echo "Options:"
  echo ""
  echo "    --ndk4       - use NDK4 & Crystax-based NDK"
  echo "    --ndk8       - use NDK8"
  echo "    --shadow=dir - shadow directory name:"
  echo "                 A directory where build will be performed (on the same level"
  echo "                 as the repository; e.g: if your repository is in"
  echo "                 /home/sergey/grym-android-lighthouse and you configure it as:"
  echo "                 \$ ./android.sh std --shadow=build-std"
  echo "                 Qt will be built in: /home/sergey/build-std"
  echo "    --test     - do not configure anything, just print configure options and exit;"
  echo "    --build    - build Qt after successful configuratuion;"
  echo "    --install  - install Qt (implies --build);"
  echo "    --suffix   - add configuration name to install path, e.g.: /data/local/qt-std"
  echo "    --prefix=/path/to/qt - fully specify custom installation path;"
  echo "    --static   - build Qt statically (not supported yet);"
  echo "    -j2..-j6   - multy-threaded compilation (use with --build or --install);"
  echo "                 Please note that using value > 3 may cause compilation errors."
  echo "    --push     - push Qt libs to default device/emulator after install"
  echo "                 (must be running); implies --install."
  echo ""
  echo "EXAMPLE: ./android.sh full --shadow=grym-build-full -j3 --install"
  echo "Current platform (for SSL): $PLATFORM - please edit $0 to change."
  echo "By default, using Android NDK $NDK_VERSION."
  echo ""
}

MODE="$1"

REPODIR="$PWD"
REPODIRBASE=`basename "$PWD"`
SHADOW=
PREFIX=/data/local/qt
OP_TEST=
OP_BUILD=
OP_INSTALL=
OP_PUSHLIBS=
OP_MAKEOPTS=

# Temprorarily excluded - probably causes program to not work on some
# weird Android builds, like BenQ tablet:
# -reduce-relocations

OPS_BASIC="-opensource -confirm-license -pch -release -qpa -arch arm -no-phonon -freetype -fast \
  -xplatform android-g++ -little-endian -no-qt3support -no-largefile \
  -nomake demos -nomake examples \
  -reduce-exports"

OPS_MINIMAL_NOS="
  -no-stl -no-xmlpatterns \
  -no-multimedia -no-phonon -no-phonon-backend \
  -no-webkit -no-script -no-javascript-jit -no-scripttools \
  -no-openssl -no-dbus \
  -no-accessibility -no-declarative -no-gtkstyle -no-opengl -no-glib \
  -no-libmng -no-libtiff -no-libjpeg -no-gif -no-svg \
  -no-sql-sqlite"

OPS_ANDROID_INCLUDEDIR="$PWD/src/3rdparty/android/precompiled/$PLATFORM/arch-arm/include"
OPS_ANDROID_INCLUDES="-I $OPS_ANDROID_INCLUDEDIR"
OPS_ANDROID_LIBS="-L $PWD/src/3rdparty/android/precompiled/$PLATFORM/arch-arm/lib"

#echo "$OPS_ANDROID_INCLUDEDIR"

OPS_LINKTYPE="-shared"
OPS_PREFIX="--prefix=$PREFIX"

##############################################################################
# Processing command-line options
##############################################################################
shift
for OP in $* ; do
  if [ "$OP" = "--build" ] ; then
    OP_BUILD=yes
    echo "Enabled compilation (--build)." ;
  elif [ "$OP" = "--install" ] ; then
    OP_BUILD=yes
    OP_INSTALL=yes
    echo "Enabled compilation and installation (--install)." ;
  elif [ "$OP" = "--suffix" ] ; then
    OPS_PREFIX="$OPS_PREFIX-$MODE"
    PREFIX="$PREFIX-$MODE"
    echo "Using install prefix: $PREFIX (--suffix)" ;
  elif [ "$OP" = "--static" ] ; then
    OPS_LINKTYPE="-static"
    echo "Using statical linking (--static)" ;
  elif [ "$OP" = "--test" ] ; then
    OP_TEST="yes"
    echo "Test mode (don't do anything, --test)." ;
  elif [ "$OP" = "--ndk4" ] ; then
    NDK_VERSION=4
    echo "Selected NDK4." ;
  elif [ "$OP" = "--ndk8" ] ; then
    NDK_VERSION=8
    echo "Selected NDK8." ;
  elif [ "$OP" = "-j2" ] || [ "$OP" = "-j3" ] || [ "$OP" = "-j4" ] || [ "$OP" = "-j5" ] || [ "$OP" = "-j6" ]; then
    OP_MAKEOPTS="$OP_MAKEOPTS $OP"
    echo "Adding make option: $OP"
  elif [ "$OP" = "--push" ] ; then
    OP_BUILD=yes
    OP_INSTALL=yes
    OP_PUSHLIBS=yes
    echo "Will push Qt libs to device after build (--push)." ;
  else

    # Parsing a parameter with a value
    PARAMNAME=`paramname $OP`
    PARAMVALUE=`paramvalue $OP`

    if [ "$PARAMNAME" = "--prefix" ] ; then
      OPS_PREFIX="--prefix=$PARAMVALUE"
      PREFIX="$PARAMVALUE"
      echo "Setting install prefix: $PARAMVALUE" ;
    elif [ "$PARAMNAME" = "--shadow" ] ; then
      SHADOW="$PARAMVALUE"
      echo "Shadow build directory: $SHADOW" ;
    else
      echo "Unknown option: $OP"
      exit 1;
    fi ;
  fi
done

##############################################################################
# Processing configuration name / command
##############################################################################

# Configuration names
if [ "$MODE" = "std" ] ; then
  echo "Selected STANDARD build." ;
elif [ "$MODE" = "full" ] ; then
  echo "Selected FULL build." ;
elif [ "$MODE" = "light" ] ; then
  echo "Selected LIGHT build." ;
elif [ "$MODE" = "minimal" ] || [ "$MODE" = "min" ] ; then
  echo "Selected MINIMAL build." ;

# Commands
elif [ "$MODE" = "preplibs" ] ; then
  androlibs
  exit 0 ;
elif [ "$MODE" = "pushlibs" ] ; then
  androlibs push
  exit 0 ;
elif [ "$MODE" = "rmlibs" ] ; then
  androlibs rm
  exit 0 ;
 elif [ "$MODE" = "plugins" ] ; then
  androplugins rm
  exit 0 ;
elif [ "$MODE" = "" ] || [ "$MODE" = "--help" ] ; then
  help
  exit 0 ;
else
  echo "Unknown command!"
  echo ""
  help
  exit 1 ;
fi

##############################################################################
# Shadow build settings
##############################################################################

if [ "$SHADOW" = "" ] || [ "$SHADOW" = "." ] ; then
   echo "Configuring for non-shadow build..."
   SHADOW=
   CONFIGURE="./configure" ;
else
   echo "Configuring for shadow named $SHADOW..."
   CONFIGURE="../$REPODIRBASE/configure"
   echo "Path to configure script: $CONFIGURE"
   mkdir -p "../$SHADOW" || exit 1
   cd "../$SHADOW" || exit 1 ;
fi

##############################################################################
# Disable SSL if not supported
##############################################################################

if [ "$NDK_VERSION" = "4" ] ; then
  OPS_SSL=
  echo "WARNING: SSL support is disabled for NDK 4!" ;
elif [ -d "$OPS_ANDROID_INCLUDEDIR" ] ; then
  OPS_SSL=-no-openssl
  # FIXME fix SSL
  echo "SSL directory exists, but SSL support is currently disabled!"
#  OPS_SSL="-openssl $OPS_ANDROID_INCLUDES $OPS_ANDROID_LIBS"
#  echo "SSL support is available; using headers in $OPS_ANDROID_INCLUDEDIR" ;
else
  OPS_SSL=
  echo "SSL support is not available because there is no $OPS_ANDROID_INCLUDEDIR" ;
fi


##############################################################################
# Construct final option string
##############################################################################

if [ "$MODE" = "std" ] ; then

  OPS_ALL="$OPS_BASIC $OPS_SSL $OPS_LINKTYPE $OPS_PREFIX -exceptions -no-webkit -no-script"

elif [ "$MODE" = "full" ] ; then

  OPS_ALL="$OPS_BASIC $OPS_SSL $OPS_LINKTYPE $OPS_PREFIX -exceptions -webkit -declarative"

elif [ "$MODE" = "light" ] ; then

  OPS_ALL="$OPS_BASIC $MORE_OPTIONS $OPS_LINKTYPE $OPS_PREFIX $OPS_MINIMAL_NOS -exceptions"

elif [ "$MODE" = "minimal" ] || [ "$MODE" = "min" ] ; then

  echo "Configuring minimal build mode..."
  MORE_OPTIONS=
  MINQCONFDIR="src/plugins/platforms/android/grym/custom"
  MINQCONF="$MINQCONFDIR/qconfig-bareminimum.h"
  if [ -f "../$REPODIRBASE/$MINQCONF" ] ; then
    echo "Minimal config file found ($MINQCONF)"
    if [ ! -f "$MINQCONF" ] ; then
      echo "Creating symlink for config."
      mkdir -p "$MINQCONFDIR"
      ln -s "$PWD/../$REPODIRBASE/$MINQCONF" "$MINQCONF" ;
    else
      echo "Minimal config symlink already exists." ;
    fi
    MORE_OPTIONS="-qconfig $MINQCONF" ;
  else
    echo "Failed to find minimal config file: ../$REPODIRBASE/$MINQCONF"
    exit 1 ;
  fi
  echo "Additional options: $MORE_OPTIONS"

  OPS_ALL="$OPS_BASIC $MORE_OPTIONS $OPS_LINKTYPE $OPS_PREFIX $OPS_MINIMAL_NOS -no-exceptions"

else
  echo "Build type $MODE not implemented!"
  exit 1 ;
fi

echo ""
echo "------------------------------------------------------------------------"
echo "CONFIGURATION OPTIONS"
echo ""
echo $OPS_ALL
echo ""
echo "Path to strip: `findarmeabistrip`"
echo "------------------------------------------------------------------------"
echo ""

if [ ! "$PATH_ADD" = "" ] ; then
  echo ""
  echo "------------------------------------------------------------------------"
  echo "ADD TO PATH"
  echo $PATH_ADD
  echo "------------------------------------------------------------------------"
  echo ""
fi

##############################################################################
# Perform configuration / building / installation
##############################################################################

# Verify that target directory is writeable
if [ "$OP_INSTALL" = "yes" ] ; then
  echo "Checking install path ($PREFIX)...";
  mkdir -p "$PREFIX"
  if [ ! -d "$PREFIX" ] ; then
    echo "ERROR: Failed to create target directory: $PREFIX"
    echo "Please create the directory (with write access) manually,"
    echo "or select another install path via --prefix."
    exit 1 ;
  fi
  if [ ! -w "$PREFIX" ] ; then
    echo "ERROR: Target directory is not writeable: $PREFIX"
    echo "Please change directory write access mode,"
    echo "or select another install path via --prefix."
    exit 1 ;
  fi
  echo "....OK" ;
fi

if [ "$OP_TEST" = "yes" ] ; then
  echo "Test mode - exiting."
  exit 0 ;
fi

# Creating local.conf
LOCALCONF="$REPODIR/mkspecs/android-g++/local.conf"
echo "# This file is generated by config script. Please do not edit it." > "$LOCALCONF"
echo "ANDROID_GRYM=1" >> "$LOCALCONF"
echo "ANDROID_NDK=$NDK_VERSION" >> "$LOCALCONF"

# Cleaning build directory if necessary
if [ "$SHADOW" = "" ] ; then
  echo "Cleaning configuration..."
  # Yes, without using shadow builds it's better to be that evil
  make confclean
  rm -fr include
  rm -fr lib
  git checkout lib ;
else
  echo "*****************************************************************************"
  echo "NOTE: Configuration clean-up is skipped because you are using a shadow build."
  echo "I suppose you know what you're doing then. Please delete the shadow directory"
  echo "or its subdirectories manually if you need to force re-configuration." 
  echo "*****************************************************************************"
  echo "";
fi

PATH="$PATH_ADD:$PATH" "$CONFIGURE" $OPS_ALL || exit 1

if [ "$OP_INSTALL" = "yes" ] ; then
  PATH="$PATH_ADD:$PATH" make $OP_MAKEOPTS || exit 1
  make install || exit 1 ;
  if [ "$OP_PUSHLIBS" = "yes" ] ; then
    androlibs push ;
  else
    androlibs ;
  fi
elif [ "$OP_BUILD" = "yes" ] ; then
  make $OP_MAKEOPTS || exit 1 ;
else
  echo ""
  echo "Now you can chdir to the shadow directory (if you specified shadow build)"
  echo "and build Qt by doing:"
  echo "\$ make -j3 && make install"
  echo "...where 3 is the number of CPU cores on your machine + 1."
  echo "" ;
fi

if [ "$SHADOW" = "" ] ; then
  echo "If everything screws up, use this command to revert to clean repository state:"
  echo "git clean -d -f -x" ;
else
  echo "If everything screws up, remove the shadow directory and start over." ;
fi
