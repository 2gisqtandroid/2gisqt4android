
// Note: see QtAnimatedTiles's Android project for addtional information.

package org.qtandroidexamples.keyboarddemo;

import java.util.List;
import android.util.Log;
import android.os.Bundle;

import org.qt.core.QtApplicationBase;
import org.qt.util.AssetUtil;
import org.qt.lite.QtActivity;

public class QtKeyboardDemo extends QtActivity
{
    public QtKeyboardDemo()
    {
        super("qtandroidkeyboarddemo");
    }

    @Override
    protected void onRuntimeStarted()
    {
        super.onRuntimeStarted();

        // Extracting own font
        if (AssetUtil.extractAssetToExternalStorage(this, "DroidSans.ttf", false))
        {
            // Setting font path in Qt. This should be done after Qt libraries
            // are loaded, that's why this code is put into onRuntimeStarted().
            QtApplicationBase.qt_android_set_font_path(getExternalFilesDir().getPath());
            Log.i("QtKeyboardDemo", "Successfully unpacked own TTF font.");
        }
        else
        {
            Log.e("QtKeyboardDemo", "Failed to extract application font to SD card!");
        }
    }

}

