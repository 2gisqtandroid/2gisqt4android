#!/bin/sh

#ant clean
#ant uninstall
#./libs_to_apk.sh

# This script will build Qt library, Java code, assemble
# apk file and install to Android device or emulator
# (must be connected / running).

#/usr/local/android-sdk-linux_x86
android update project -p . -t 3 --target "android-8"

cd QtSources
PATH="/data/local/qt/bin:$PATH" qmake && make || exit 1
cd ..

ant clean
ant debug install
