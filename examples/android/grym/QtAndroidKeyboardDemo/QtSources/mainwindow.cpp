/*
  Qt/Android Keyboard Demo

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, 2012, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <unistd.h>
#include <QtCore/qdebug.h>
#include <QtCore/QStringList>
#include <QtCore/QDir>
#include <QtGui/QApplication>
#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>
#include <QtGui/QTextEdit>
#include <QtGui/QMessageBox>
#include <QtGui/QClipboard>
#include "mainwindow.h"
#include "ui_mainwindow.h"

// Functions exported by Android plugin Core.
// Note that you have to explicitly link QAndroidCore library.
void qt_android_show_software_keyboard(bool show);
void qt_android_set_advanced_sip(bool advanced);
bool qt_android_get_advanced_sip();
bool qt_android_get_disable_fullscreen_sip();
void qt_android_java_set_disable_fullscreen_sip(bool disable);

// Context menu functions.
void qt_android_context_menu_create(const QString &caption);
long qt_android_context_menu_add(const QString &caption, QObject *obj, const char *on_click);
void qt_android_context_menu_exec();
void qt_android_context_menu_cancel();
bool qt_android_context_menu_call(long item_id);

void qt_android_toast(const QString &message, bool long_duration);

// Show select Input Method.
void qt_android_input_method_select();

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Dump some useful stuff into the big text edit
    QStringList text;

    text<<("Build date: " __DATE__ " " __TIME__);
    text<<("QDir::homePath(): "+QDir::homePath());

    const char* const* e = environ;
    while( *e ){
        text<<*e;
        e++;
    }
    ui->plainTextEdit->appendPlainText(text.join("\n"));
    ui->fsBtn->setChecked(!qt_android_get_disable_fullscreen_sip());
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*
void MainWindow::updateStatus()
{
    ui->status->setText(
        QString("Status: %1 OK: %2").
            arg( (qt_android_soft_keyboard_state())? tr("OPEN"): tr("CLOSED") ).
            arg( (mLastResult)? tr("YES"): tr("NO") )
    );
    ui->status->update();
}
*/

void MainWindow::openKeyboard()
{
    qDebug() << "Open keyboard!";
    qt_android_show_software_keyboard(true);
}

void MainWindow::closeKeyboard()
{
    qDebug() << "Close keyboard!";
    qt_android_show_software_keyboard(false);
}

void MainWindow::autoSip(bool)
{
    static_cast<QApplication*>(QApplication::instance())->
            setAutoSipEnabled(ui->autoSipBtn->isChecked());
}

void MainWindow::advancedSip(bool)
{
    qt_android_set_advanced_sip(ui->advancedSipBtn->isChecked());
}

void MainWindow::clear(bool)
{
    ui->edit1->clear();
    ui->edit2->clear();
}

void MainWindow::fs(bool)
{
    qt_android_java_set_disable_fullscreen_sip(!ui->fsBtn->isChecked());
}

void MainWindow::quitApp()
{
    QApplication::instance()->quit();
}

void MainWindow::about()
{
    QMessageBox::information(
            this,
            tr("About"),
            tr("Qt/Android Keyboard Demo, (C) Sergey A. Galin, 2010-2012\n"
               "http://qt.gitorious.org/+grym/qt/grym-android-lighthouse\n"));
    qDebug()<<"QMessageBox returned";
}

void MainWindow::aboutQt()
{
    QApplication::aboutQt();
}

void MainWindow::copy()
{
    QClipboard *clipboard = QApplication::clipboard();
    if (!clipboard)
    {
        return;
    }

    QString text = ui->edit1->text();
    clipboard->setText(text);
}

void MainWindow::paste()
{
    const QClipboard *clipboard = QApplication::clipboard();
    if (!clipboard)
    {
        return;
    }

    const QMimeData *mimeData = clipboard->mimeData();
    if (!mimeData)
    {
        return;
    }

    if (mimeData->hasText())
    {
        QString text = mimeData->text();
        ui->edit1->setText(text);
    }
}

void MainWindow::toast() const
{
    qt_android_toast(tr("Toast"), true);
}

void MainWindow::inputMethodSelect() const
{
    qt_android_input_method_select();
}

void MainWindow::showMenu()
{
    qt_android_context_menu_create(tr("Context meun"));
    int copy_id = qt_android_context_menu_add(tr("Copy"), this, "copy");
    int paste_id = qt_android_context_menu_add(tr("Paste"), this, "paste");
    int input_method_select_id = qt_android_context_menu_add(tr("Select Input Method"), this, "inputMethodSelect");
    int toast_id = qt_android_context_menu_add(tr("Toast"), this, "toast");
    int open_keyboard_id = qt_android_context_menu_add(tr("Open keyboard"), this, "openKeyboard");
    int close_keyboard_id = qt_android_context_menu_add(tr("Close keyboard"), this, "closeKeyboard");

    qt_android_context_menu_exec();
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    painter.setPen(QColor(0,0,0,50));
    for( int x = 0; x<width(); x+=5)
        painter.drawLine(x, 0, x, height());
    for( int y = 0; y<height(); y+=5)
        painter.drawLine(0, y, width(), y);
    QMainWindow::paintEvent(e);
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
    QMainWindow::resizeEvent(e);
    // updateStatus();
}

