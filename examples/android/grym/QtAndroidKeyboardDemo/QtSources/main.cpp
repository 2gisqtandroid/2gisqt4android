/*
  Qt/Android Keyboard Demo

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, 2012, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <qdebug.h>
#include <qapplication.h>
#include "mainwindow.h"

#ifdef Q_OS_ANDROID
QString qt_android_get_current_plugin();
void qt_android_keyboard_verbose_logs(bool verbose);
#endif

const char* const TAG = "**** main() ****";

int main( int argc, char * argv[] )
{
#ifdef Q_OS_ANDROID
    qt_android_keyboard_verbose_logs(true);
#endif
    int result = -1;
    { // QApplication lifetime
        qDebug()<< TAG <<"Constructing QApplication...";
        QApplication app( argc, argv );

        qDebug()<< TAG <<"Setting initial value of auto-SIP...";
        app.setAutoSipEnabled( true );

        qDebug()<< TAG <<"Constructing main window...";
        MainWindow window;

        qDebug()<< TAG <<"Displaying main window...";
        window.showFullScreen();

        qDebug()<< TAG <<"Executing app...";
        result = app.exec();

        qDebug()<< TAG <<"Destroying main window & app...";
    } // QApplication is destroyed
    qDebug()<< TAG <<"Done!";
    return result;
}

