
This directory contains a number of examples and apps used to test platform pluging for Android.

Working demos:

QtAndroidKeyboardDemo  - a demo / test for Android keyboard support
QtAnimatedTile         - QtAnimatedTiles using Lite plugin

Non-working for now:

QmlViewer             - QmlViewer using Lite plugin, NOT DONE YET
