package org.qmlviewer.qt;

import com.nokia.qt.lite.QtActivity;

public class QmlViewer extends QtActivity
{
    public QmlViewer()
    {
        super("QmlViewer");
        addLibrary("QtSql");
        addLibrary("QtXml");
        addLibrary("QtSvg");
        addLibrary("QtOpenGL");
        addLibrary("QtScript");
        addLibrary("QtNetwork");
        addLibrary("QtXmlPatterns");
        addLibrary("QtOpenGL");
        addLibrary("QtDeclarative");
    }
}
