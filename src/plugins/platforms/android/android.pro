TEMPLATE = subdirs

# android-lighthouse
SUBDIRS += sw

# grym-android-lighthouse
SUBDIRS += grym

# Disabled / obsoleted directories
# grym-android-lighthouse's version of mw,
# heavily patched and converted to use "core"
# SUBDIRS += mw_grym mw_grym_nogl

# SUBDIRS += mw
