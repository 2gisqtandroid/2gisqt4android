
# DEFINES += QT_ANDROID_LITE_NO_MULTYTLW

# Overriden in API-level specific versions
TARGET = QtAndroidLite

DEPENDS += $$QMAKE_LIBDIR_QT/libQAndroidCore.so

include(../../../../qpluginbase.pri)

# Avoid linking to QtNetwork, QtOpenGl and other stuff
# not actually needed by the plug-in
QT = core gui

QMAKE_CXXFLAGS_RELEASE -= -Os
QMAKE_CXXFLAGS_RELEASE += -O3

DEFINES = QT_STATICPLUGIN
CONFIG += dll
DESTDIR = $$QMAKE_LIBDIR_QT

INCLUDEPATH += ../../core

include(../jnigraphics.pri)
include(../common.pri)

LIBS += -lQAndroidCore

SOURCES += \
    ../src/main.cpp \
    ../src/androidjni.cpp \
    ../src/qandroidintegration.cpp \
    ../src/qandroidscreen.cpp \
    ../src/qandroidwindowsurface.cpp \
    ../src/qjniimagepair.cpp \
    ../src/qlitewindowdecorations.cpp

HEADERS = \
    ../src/qandroidintegration.h \
    ../src/qandroidscreen.h \
    ../src/qandroidwindowsurface.h \
    ../src/qjniimagepair.h \
    ../src/qlitewindowdecorations.h

target.path=$$[QT_INSTALL_LIBS]

INSTALLS += target
