/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QANDROIDINTEGRATION_H
#define QANDROIDINTEGRATION_H

#include <jni.h>
#include <android/bitmap.h>
#include <QPlatformIntegration>
#include <QPlatformScreen>
#include <QTimer>
#include <QScopedPointer>
#include <QMutex>
#include <qandroidfontdatabase.h>
#include <qandroidclipboard.h>

QT_BEGIN_NAMESPACE

class QAndroidWindowSurface;
class QAndroidScreen;

class QAndroidIntegration : public QPlatformIntegration
{
public:
    QAndroidIntegration();
    virtual ~QAndroidIntegration();

    // QPlatformIntegration
    QPixmapData *createPixmapData(QPixmapData::PixelType type) const;
    QPlatformWindow *createPlatformWindow(QWidget *widget, WId winId) const;
    QWindowSurface *createWindowSurface(QWidget *widget, WId winId) const;
    QList<QPlatformScreen *> screens() const { return mScreens; }
    QPlatformFontDatabase *fontDatabase() const;
#if !defined(QT_NO_CLIPBOARD)
    QPlatformClipboard *clipboard() const;
#endif

    // custom
    // called from QAndroidWindowSurface, qt main() thread
    jobject surfaceCreate(QImage::Format, QSize) const;
    void surfaceBlit(jobject bitmap, QPoint at, QRect rect) const;

    // jni
    // called from Android/JNI (androidjni.cpp), java thread
    static void androidScreenChanged(QSize size, QSize physical);
    //static void androidScreenLost();

    static void repaint();

    //statics
public:
    static jclass mQtApplicationClass;
    static jmethodID mSurfaceCreateStaticMethod;
    static jmethodID mSurfaceBlitStaticMethod;

private:
    static QAndroidIntegration* mActiveIntegration;
    static QSize mScreenSize, mPhysicalSize;
    friend class QAndroidScreen;

private:
    QList<QPlatformScreen*> mScreens;
    QScopedPointer<QtAndroid::QAndroidPlatformFontDatabase> mFontDB;
#if !defined(QT_NO_CLIPBOARD)
    QScopedPointer<QtAndroid::QAndroidClipboard> mClipboard;
#endif
};

// This class is just to have repaint() in a slot
// TODO: make it less ugly
class QAndroidDelayedRepainter: public QObject
{
    Q_OBJECT
public:
    QAndroidDelayedRepainter();
    virtual ~QAndroidDelayedRepainter();
    static QAndroidDelayedRepainter* instance();
public slots:
    void repaint();
    void delayedRepaint();
    void delayedRepaint(int ms);
};

QT_END_NAMESPACE

#endif
