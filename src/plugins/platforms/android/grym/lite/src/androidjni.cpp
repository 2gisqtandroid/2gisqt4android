/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <jni.h>
#include <QDebug>
#include <QWindowSystemInterface>
#include <qandroidcoreexports.h>
#include <qandroidutils.h>
#include <qandroiddpiworkarounds.h>
#include "qandroidintegration.h"

QT_BEGIN_NAMESPACE
JavaVM* g_javaVm = 0;
QT_END_NAMESPACE

Q_DECL_EXPORT void JNICALL repaint(JNIEnv*, jobject)
{
   QAndroidIntegration::repaint();
}

Q_DECL_EXPORT void JNICALL screenChanged(JNIEnv*, jobject, jint w, jint h, jfloat xdpi, jfloat ydpi, jfloat density)
{
    qreal xd = xdpi, yd = ydpi;
    qDebug()<<"screenChanged("<<w<<","<<h<<","<<xd<<"DPI,"<<yd<<"DPI )";
    if (w <= 0 || h <= 0) // FIXME
        return;

    QtAndroid::fixDpi(qt_android_get_screen_width(), qt_android_get_screen_height(), &xd, &yd, density);

    // Physical size of the screen
    int phw = qRound((double)w / xd * 100.0 / 2.54 );
    int phh = qRound((double)h / yd * 100.0 / 2.54 );
    qDebug()<<"........physical size is:"<<phw<<"x"<<phh<<"mm";

    QAndroidIntegration::androidScreenChanged(QSize(w, h), QSize(phw, phh));

    repaint(0, 0);
}

union UnionJNIEnvToVoid
{
    JNIEnv* nativeEnvironment;
    void* venv;
};

Q_DECL_EXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void*)
{
    qDebug()<<"Grym Lite QPA plugin built"<<__DATE__<<__TIME__;

    // qDebug(".......g_javaVm == %p", vm);
    g_javaVm = vm;

    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK)
    {
        qCritical()<<"GetEnv failed";
        return -1;
    }
    JNIEnv* env = uenv.nativeEnvironment;

    // qDebug("env == %p", env);

    static JNINativeMethod qt_app_screen_methods[] = {
        {"screenChanged", "(IIFFF)V", (void*)screenChanged},
        {"repaint", "()V", (void*)repaint}
    };

    const char c_appClassName[] = "org/qt/lite/QtApplication";

    jclass app_class = env->FindClass(c_appClassName);
    // qDebug("app_class == %p", app_class);
    if( !app_class ){
        qCritical()<<"Failed to find Java class:"<<c_appClassName;
        return -1;
    }

    int errsv = env->RegisterNatives(app_class, qt_app_screen_methods,
            sizeof(qt_app_screen_methods)/sizeof(qt_app_screen_methods[0]) );
    // qDebug()<<"RegisterNatives =="<<errsv;
    Q_UNUSED(errsv);

    QAndroidIntegration::mQtApplicationClass = static_cast<jclass>(env->NewGlobalRef(app_class));
    QAndroidIntegration::mSurfaceCreateStaticMethod = env->GetStaticMethodID(app_class, "surfaceCreate", "(III)Landroid/graphics/Bitmap;");
    QAndroidIntegration::mSurfaceBlitStaticMethod = env->GetStaticMethodID(app_class, "surfaceBlit", "(Landroid/graphics/Bitmap;IIIIII)V");

    // qDebug()<<"........success.";

    return JNI_VERSION_1_4;
}

Q_DECL_EXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void*)
{
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK)
    {
        qCritical()<<"GetEnv failed";
        return;
    }
    JNIEnv* env = uenv.nativeEnvironment;

    if( QAndroidIntegration::mQtApplicationClass ){
        env->DeleteGlobalRef(QAndroidIntegration::mQtApplicationClass);
        QAndroidIntegration::mQtApplicationClass = 0;
    }
}

