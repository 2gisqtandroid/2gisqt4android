/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtCore/qdebug.h>
#include <QDialog>
#include <QMainWindow>
#include <QtGui/private/qapplication_p.h>
#include <qandroidutils.h>
#include <qandroidcoreexports.h>
#include <qandroidcorejava.h>
#include "qandroidwindowsurface.h"
#include "qlitewindowdecorations.h"

QT_BEGIN_NAMESPACE

extern JavaVM* g_javaVm;

inline unsigned long getusec()
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
}

QAndroidWindowSurface::QAndroidWindowSurface(QWidget *window)
    : QWindowSurface(window)
    , mRealTlw(true)
{
    qDebug("tid: %d QAndroidWindowSurface(%p)::QAndroidWindowSurface(%p)", gettid(), this, window);
    mFlushCount = 0;

    if(window)
    {
        QObject::connect(window, SIGNAL(destroyed()),
                         QAndroidDelayedRepainter::instance(), SLOT(delayedRepaint()));

        // Enable an ugly workaround for non-TLW window surfaces.
        // FIXME - see other comments on this matter...
        if( qt_android_get_filter_tlw_widgets() ){
            if( !qt_android_is_tlw_widget(window) ){
                qDebug()<<"........Marking window as non-TLW.";
                mRealTlw = false;
            }
        }
    }

    #if !defined(QT_ANDROID_LITE_NO_MULTYTLW) && !defined(QT_ANDROID_CORE_NOZORDER)
        mZOrderAi = QtAndroid::QZOrderAi::instance().toStrongRef();
        if( mZOrderAi.isNull() ) {
            mZOrderAi = QSharedPointer<QtAndroid::QZOrderAi>( new QtAndroid::QZOrderAi );
            QtAndroid::QZOrderAi::setInstance(mZOrderAi);
        }
    #endif
}

QAndroidWindowSurface::~QAndroidWindowSurface()
{
    // qDebug("tid: %d QAndroidWindowSurface(%p)::~QAndroidWindowSurface()", gettid(), this);

    // Some window is gone - we have to repaint the rest
    // FIXME: DIRTY HACK!!
    // qDebug()<<"SCHEDULE REPAINT";
    QAndroidDelayedRepainter::instance()->delayedRepaint();
}

QPaintDevice* QAndroidWindowSurface::paintDevice()
{
    return &mImage.qimage();
}

void QAndroidWindowSurface::flush(QWidget *widget, const QRegion &region, const QPoint &offset)
{
#if 0 // fps printing
    static QTime fps_timer;
    static unsigned long fps_count = 0;
    if (!fps_timer.isValid())
      fps_timer.start();
    else if (fps_timer.elapsed() > 1000)
    {
      float fps = float(fps_count) / (fps_timer.elapsed() / 1000.0f);
      qWarning()<<"fps ="<<fps;
      fps_count = 0;
      fps_timer.restart();
    }
    fps_count++;
#endif

    if( !qt_android_is_application_active() || !widget )
        return;

    if( widget->width() < 1 || widget->height() < 1 ) {
        qDebug()<<"Won't flush widget with zero size:"<<widget->width()<<"x"<<widget->height();
        return;
    }

    QPlatformIntegration* qpi = QApplicationPrivate::platformIntegration();
    if( !qpi ){
        qWarning()<<__FUNCTION__<<"QPlatformIntegration is NULL!";
        return;
    }
    QAndroidIntegration* qapi = static_cast<QAndroidIntegration*>(qpi);

#ifndef QT_ANDROID_LITE_NO_MULTYTLW
    // If we are in multi-window mode and have many top-level widgets,
    // perform multi-window flush (slow).
    // When we have just one TLW, simply flush that one TLW
    // and save time and battery.
    QWidgetList tlws = QApplication::topLevelWidgets();
    if( tlws.size() > 1 ){
        if( qt_android_get_filter_tlw_widgets() ) { // Filtering mode!
            // Do we actually have any other TLW's?
            foreach( QWidget* tlw, tlws ) {
                // TODO: use mRealTwl here?
                if( tlw != widget && qt_android_is_tlw_widget(tlw) ) { // Yes, another TLW
                    flushMultipleWindows(tlws, qapi, offset);
                    return;
                }
            }
            // No other TLW found => continue to single-window flush
        } else {
            flushMultipleWindows(tlws, qapi, offset);
            return;
        }
    }
#endif

    //
    // Here we have just one top-level widget to flush
    //
    QMutexLocker locker(&mImageResizeMutex);
    if( mImage.jbitmap() ) {
        // Calculate widget's coordinates on the screen
        QPoint globalOff = widget->mapToGlobal(QPoint(0,0));
        QPoint off( offset.x() + globalOff.x(), offset.y() + globalOff.y() );
        // FIXME: something is wrong with partial blit!
        // Using whole widget area for now!!!!!110
        // Partial blit version:
        // qapi->surfaceBlit(mImage.jbitmap(), off, region.boundingRect());
        qapi->surfaceBlit(mImage.jbitmap(), off, QRect(0, 0, widget->width(), widget->height()) );
        Q_UNUSED(region);
    }
}

#ifndef QT_ANDROID_LITE_NO_MULTYTLW

QWeakPointer<QJniImagePair> QAndroidWindowSurface::sMultiwindowImage;

#define TRACE_FLUSHING(x)
//#define TRACE_FLUSHING(x) x

void QAndroidWindowSurface::flushTlwToImage(QPainter* p, QWidget* tlw, const QPoint &offset)
{
    TRACE_FLUSHING(qDebug()<<"WINDOW"<<tlw->metaObject()->className()<<tlw->objectName());
    if( !tlw->isVisible() )
        return;
    QPoint globalOff = tlw->mapToGlobal(QPoint(0,0));
    QPoint off( offset.x() + globalOff.x(), offset.y() + globalOff.y() );
    TRACE_FLUSHING(qDebug()<<"......Global off:"<<globalOff.x()<<globalOff.y()
                           <<"off:"<<off.x()<<off.y());
    QWindowSurface* qws = tlw->windowSurface();
    if( !qws ) {
        return;
    }
    QAndroidWindowSurface* qaws = dynamic_cast<QAndroidWindowSurface*>(qws);
    if( !qaws ) {
        return;
    }
    const QImage& subwindow = qaws->mImage.qimage();
    TRACE_FLUSHING(qDebug()<<".......Window size:"<<subwindow.width()<<subwindow.height());
    if( QtAndroid::isDecoratedWindow(tlw) ) {
        QtAndroid::drawDecorations(
                p,
                QRect(off.x(), off.y(), subwindow.width(), subwindow.height()),
                tlw->windowTitle());
    }
    p->drawImage(off, subwindow);
}

void QAndroidWindowSurface::flushMultipleWindows(QWidgetList& tlws, QAndroidIntegration* qapi, const QPoint &offset)
{
    TRACE_FLUSHING(qDebug()<<"PAINT =============================================================");
    int sw = qt_android_get_screen_width(), sh = qt_android_get_screen_height();

    if( sw < 1 || sh < 1 ) {
        qCritical()<<"Invalid screen size information:"<<sw<<"x"<<sh;
        return;
    }

    if( mMultiTlwBuffer.isNull() ) {
        if( sMultiwindowImage.isNull() ) {
            mMultiTlwBuffer = QSharedPointer<QJniImagePair>( new QJniImagePair() );
            sMultiwindowImage = mMultiTlwBuffer;
        } else
           mMultiTlwBuffer = sMultiwindowImage.toStrongRef();
        if( mMultiTlwBuffer.isNull() ) {
            qCritical()<<"Failed to allocate full-screen buffer!";
            screenBufferError();
            return;
        }
    }

    // Buffer for (sw, sh) crashes and (sw+1, sh+1) doesn't, FIXME: find out why
    if( !mMultiTlwBuffer->ensureSize(sw+1, sh+1) ) {
        qCritical()<<"Failed to resize full-screen buffer!";
        screenBufferError();
        return;
    }
    QPainter p(&(mMultiTlwBuffer->qimage()));
    mMultiTlwBuffer->qimage().fill(0);
#if !defined(QT_ANDROID_CORE_NOZORDER)
    QtAndroid::QZOrder::sort(&tlws);
#endif
    bool filter = qt_android_get_filter_tlw_widgets();
    for( int i = 0; i < tlws.size(); i++ ) {
        QWidget* tlw = tlws.at(i);
        // TODO: use mRealTlw here? It would work a bit faster.
        if( filter ) {
            if( !qt_android_is_tlw_widget( tlw ) )
                continue;
        }
        if( tlw->isVisible() )
            flushTlwToImage(&p, tlw, offset);
    }
    qapi->surfaceBlit(mMultiTlwBuffer->jbitmap(), QPoint(0, 0), QRect(0, 0, sw, sh));
}

#endif

void QAndroidWindowSurface::resize(const QSize &size)
{
    if( !mRealTlw )
    {
        // FIXME this is a workaround. Non-TLW widgets should not create real surfaces,
        // but simply not creating any sufrace may cause Lighthouse to crash.
        // This really should be fixed on Lighthouse level.
        mImage.dummy();
    }
    else
    {
        if( !mImage.hasSize(size.width(), size.height()) )
        {
            if( size.width() < 1 || size.height() < 1 ) {
                qDebug()<<"Window surface resize: "<<size.width()<<"x"<<size.height();
                mImage.dummy();
            } else {
                QMutexLocker locker(&mImageResizeMutex);
                if( !mImage.resize(size) )
                    screenBufferError();

                if( mImage.qimage().isNull() )
                   screenBufferError(QCoreApplication::translate("Android",
                           "Resize returned invalid QImage."));
            }
         }
    }

    QWindowSurface::resize(size);
}

void QAndroidWindowSurface::screenBufferError(const QString& additionalInfo)
{
    QString msg = QCoreApplication::translate("Android",
            "Failed to create screen buffer. Please try to reboot your device. "
            "If it doesn't help, please report this problem along with the "
            "device model and firmware version to the application developers.");

    if( !additionalInfo.isEmpty() ) {
       msg += QLatin1String("\n\n");
       msg += QCoreApplication::translate("Android", "Additional Information:"),
       msg += QLatin1String("\n");
       msg += additionalInfo;
    }

    qt_android_crash_message(
        QCoreApplication::translate("Android", "Screen Buffer Error"),
        msg,
        QCoreApplication::translate("Android", "Exit"),
        true);

    abort();
}

QT_END_NAMESPACE
