/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <jni.h>
#include <time.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <android/bitmap.h>

#include <QtGui/private/qpixmap_raster_p.h>
#include <QtGui/QPlatformWindow>
#include <QWindowSystemInterface>

#include <QtCore/qdebug.h>
#include <QtCore/qthread.h>
#include <QMutexLocker>
#include <QFile>
#include <QDir>
#include <QApplication>
#include <QDesktopWidget>

#include <qandroidinputcontext.h>
#include <qandroidutils.h>

#include "qandroidwindowsurface.h"
#include "qandroidscreen.h"
#include "qandroidintegration.h"

QT_BEGIN_NAMESPACE

// set initial values
QAndroidIntegration* QAndroidIntegration::mActiveIntegration = NULL;
QSize QAndroidIntegration::mScreenSize = QSize(240, 240);
QSize QAndroidIntegration::mPhysicalSize = QSize(100, 100);
jclass QAndroidIntegration::mQtApplicationClass = 0;
jmethodID QAndroidIntegration::mSurfaceCreateStaticMethod = 0;
jmethodID QAndroidIntegration::mSurfaceBlitStaticMethod = 0;

extern JavaVM* g_javaVm;

inline unsigned long getusec()
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
}

QAndroidIntegration::QAndroidIntegration()
    : mFontDB( new QtAndroid::QAndroidPlatformFontDatabase() )
#if !defined(QT_NO_CLIPBOARD)
    , mClipboard( new QtAndroid::QAndroidClipboard() )
#endif
{
    qDebug("QAndroidIntegration::QAndroidIntegration(): %p ", this);

    // wait for everyone to complete
    QMutexLocker locker(&QtAndroid::jniMutex);
    mActiveIntegration = this;
#if 1
    ((QApplication*)QApplication::instance())->setInputContext( new QAndroidInputContext(QApplication::instance()) );
#endif
    mScreens << new QAndroidScreen(16); // 32 is slower on simple test
}

QAndroidIntegration::~QAndroidIntegration()
{
    // wait for everyone to complete
    QMutexLocker locker(&QtAndroid::jniMutex);

    foreach(QPlatformScreen* p, mScreens)
        delete p;
    mScreens.clear();

    // this application is no more~!
    mActiveIntegration = NULL;
}

QPixmapData *QAndroidIntegration::createPixmapData(QPixmapData::PixelType type) const
{
    return new QRasterPixmapData(type);
}

QPlatformWindow *QAndroidIntegration::createPlatformWindow(QWidget *widget, WId winId) const
{
    Q_UNUSED(winId);
    return new QPlatformWindow(widget);
}

QWindowSurface *QAndroidIntegration::createWindowSurface(QWidget *widget, WId winId) const
{
    qDebug("tid:%d %p->%s(%p, %d)", gettid(), this, __PRETTY_FUNCTION__, widget, (int)winId);
    Q_UNUSED(winId);
    return new QAndroidWindowSurface(widget);
}

QPlatformFontDatabase *QAndroidIntegration::fontDatabase() const
{
    return mFontDB.data();
}

#if !defined(QT_NO_CLIPBOARD)
QPlatformClipboard *QAndroidIntegration::clipboard() const
{
    return mClipboard.data();
}
#endif

jobject QAndroidIntegration::surfaceCreate(QImage::Format fmt, QSize size) const
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    qDebug()<<"surfaceCreate:"<<size.width()<<","<<size.height();
    int depth;
    switch (fmt)
    {
    //case QImage::Format_ARGB32_Premultiplied:
    case QImage::Format_RGB32:
        depth = 32;
        break;
    case QImage::Format_RGB16:
        depth = 16;
        break;
    default:
        qCritical("Unsupported image format %d, will not create surface", (int)fmt);
        return 0;
    }

    JNIEnv* env = 0;
    g_javaVm->AttachCurrentThread(&env, 0);
    if( env ){
        jobject ret = env->CallStaticObjectMethod(
               mQtApplicationClass,
               mSurfaceCreateStaticMethod,
               size.width(),
               size.height(),
               depth);
        JNI_CHECK_EXCEPTION(env);
        return ret;
    }else{
        qCritical()<<__FUNCTION__<<"- Failed to attach to thread!";
        return 0;
    }
}

void QAndroidIntegration::surfaceBlit(jobject bitmap, QPoint at, QRect rect) const
{
    JNIEnv* env = 0;
    g_javaVm->AttachCurrentThread(&env, 0);
    if( env && bitmap ) {
        env->CallStaticVoidMethod(
                mQtApplicationClass,
                mSurfaceBlitStaticMethod,
                bitmap,
                at.x(),
                at.y(),
                rect.x(),
                rect.y(),
                rect.width(),
                rect.height());
        JNI_CHECK_EXCEPTION(env);
    } else {
        qCritical()<<__FUNCTION__<<"- Failed to attach to thread!";
    }
}

// jni
// called from Android/JNI (androidjni.cpp)
void QAndroidIntegration::androidScreenChanged(QSize size, QSize physical)
{
    qDebug("tid: %d QAndroidIntegration(%p)::androidScreenChanged(%dx%dpx, %dx%dmm)",
              gettid(), mActiveIntegration,
              size.width(), size.height(),
              physical.width(), physical.height());
    if (size.width() == 0 || size.height() == 0)
        return; // TODO: notify everyone of screen loss

    QMutexLocker locker(&QtAndroid::jniMutex);
    mScreenSize = size;
    mPhysicalSize = physical;
    //locker.unlock();

    //if application was started
    if (mActiveIntegration != NULL)
    {
        locker.unlock();
        qDebug()<<"tid:"<<gettid()<<"will touch QWindowSystemInterface::handleScreenGeometryChange(0)";
        QWindowSystemInterface::handleScreenGeometryChange(0);
        QWindowSystemInterface::handleScreenAvailableGeometryChange(0);
    }
    qDebug("tid: %d exiting QAndroidIntegration::androidScreenChanged", gettid());
}

void QAndroidIntegration::repaint()
{
    // qDebug()<<"QAndroidIntegration::repaint() >>>>";
    QMutexLocker locker(&QtAndroid::jniMutex);
    if( !qt_android_is_in_main() || !QApplication::instance() ){
        // qDebug()<<"<<<< QAndroidIntegration::repaint() not needed";
        return;
    }
    foreach (QWidget *widget, QApplication::allWidgets()){
        if( widget )
            if( widget->isVisible() )
                widget->update();
    }
    if( mActiveIntegration != NULL ){
        for( int i = 0; i < mActiveIntegration->mScreens.size(); i++ ){
             QPlatformScreen* ps = mActiveIntegration->mScreens[i];
             if( ps ){
                 // qDebug()<<"....invalidating #"<<i;
                 ps->setDirty(ps->geometry());
             }
        }
    }
    // qDebug()<<"<<<< QAndroidIntegration::repaint() done";
}


QAndroidDelayedRepainter::QAndroidDelayedRepainter()
{
}

QAndroidDelayedRepainter::~QAndroidDelayedRepainter()
{
}

QAndroidDelayedRepainter* QAndroidDelayedRepainter::instance()
{
    static QAndroidDelayedRepainter inst;
    return &inst;
}

void QAndroidDelayedRepainter::repaint()
{
    // qDebug()<<"DELAYED REPAINT!";
    QAndroidIntegration::repaint();
}

void QAndroidDelayedRepainter::delayedRepaint()
{
    // QTimer::singleShot(1, instance(), SLOT(repaint()));
    QMetaObject::invokeMethod(instance(), "repaint", Qt::QueuedConnection);
}

void QAndroidDelayedRepainter::delayedRepaint(int ms)
{
    QTimer::singleShot(ms, instance(), SLOT(repaint()));
}

QT_END_NAMESPACE
