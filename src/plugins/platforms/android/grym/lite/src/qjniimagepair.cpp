/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <unistd.h>
#include <QtCore/QDebug>
#include <QtGui/private/qapplication_p.h>
#include "qandroidwindowsurface.h"
#include <qandroidutils.h>
#include <qandroidcorejava.h>
#include "qjniimagepair.h"

QT_BEGIN_NAMESPACE

extern JavaVM* g_javaVm;

QJniImagePair::QJniImagePair()
    : mBitmap(0)
    , mImageOnBitmap()
{
}

QJniImagePair::~QJniImagePair()
{
    deallocate();
}

void QJniImagePair::dummy()
{
    if( mImageOnBitmap.width() == 1 && mImageOnBitmap.height() == 1 && mBitmap == 0 )
        return; // Already a dummy
    deallocate();
    mImageOnBitmap = QImage(1, 1, QImage::Format_RGB16);
}

void QJniImagePair::deallocate()
{
    qDebug()<<"Deallocating dual bitmap:"<<mImageOnBitmap.width()<<"x"<<mImageOnBitmap.height();
    mImageOnBitmap = QImage(); // In case deallocate() is called not from destructor...
    if( mBitmap ) {
        if( g_javaVm ) {
            JNIEnv* env = 0;
            g_javaVm->AttachCurrentThread(&env, 0);
            if( env ) {
                env->DeleteGlobalRef(mBitmap);
                mBitmap = 0;
            } else {
                qCritical()<<__FUNCTION__<<"- Failed to free Java bitmap (thread attachment error)!";
            }
        } else {
            qCritical()<<__FUNCTION__<<"- Failed to free Java bitmap (no Java VM)!";
        }
    }
}

bool QJniImagePair::resize(QImage& imageOnBitmap, jobject& bitmap, const QSize &size)
{
    qDebug("tid: %d QAndroidWindowSurface::resizeImagePair(%dx%d)",
           gettid(), size.width(), size.height());

    if( size.width() < 1 || size.height() < 1 ) {
        qCritical()<<__FUNCTION__<<"- Supplied image dimenstions are invalid!";
        return false;
    }

    JNIEnv* env = 0;
    g_javaVm->AttachCurrentThread(&env, 0);
    if( !env ) {
        qCritical()<<__FUNCTION__<<"- Failed to attach thread!";
        return false;
    }

    // We'll need a new bitmap for the new size
    QAndroidIntegration* aint = static_cast<QAndroidIntegration*>(QApplicationPrivate::platformIntegration());
    QImage::Format format = aint->screens().first()->format();

    //
    // Create Android bitmap object by calling appropriate Java function over JNI
    //

    jobject newBitmap = aint->surfaceCreate(format, size);
    if (newBitmap == 0)
    {
        qCritical("Could not create %dx%d surface", size.width(), size.height());
        if (bitmap != 0)
        {
            env->DeleteGlobalRef(bitmap);
            bitmap = 0;
        }
        imageOnBitmap = QImage();
        return false;
    }

    //
    // Request image format for the bitmap created
    //

    // AndroidBitmap_getInfo() does not work on Android 1.6, so
    // on API4 we are using a stub.
    // On newer systems, we honestly ask Android bitmap for its
    // actual dimensions, format and stride. It didn't make any
    // real differenece so far because we have RGB16 anywhere but
    // let's try to be on the more advanced side.
    uint32_t bwidth, bheight, bstride;
    if( qt_android_get_api_level() > 4 ) {
        // Android > 1.6
        AndroidBitmapInfo binfo;
        memset(&binfo, 0, sizeof(binfo)); // Important!
        int errsv = AndroidBitmap_getInfo(env, newBitmap, &binfo);
        if (errsv != 0) {
            qCritical()<<"Could not get new surface info, error:"<<errsv;
            env->DeleteLocalRef(newBitmap);
            if (bitmap != 0)
            {
                env->DeleteGlobalRef(bitmap);
                bitmap = 0;
            }
            imageOnBitmap = QImage();
            return false;
        }
        bwidth = binfo.width;
        bheight = binfo.height;
        bstride = binfo.stride;
        // binfo.flags is typically 0 and is not used.

        // Here, we have to make a workaround for some sorry devices.
        // For example, Huawei MediaPad returns: stride instead of width, width instead of height,
        // zero instead of stride. In such case we just try to assume that the image format is
        // standard. Worked good so far.
        if (binfo.format == 0 || bstride == 0 || bwidth == 0 || bheight == 0 || bstride < bwidth) {
            qCritical()<<"AndroidBitmapInfo returned zeroes, falling back to standard RGB16 bitmap! "
                         "Result: width:"<<bwidth<<"height:"<<bheight
                       <<"stride:"<<bstride<<"format:"<<binfo.format<<"flags:"<<binfo.flags;
            bwidth = size.width();
            bheight = size.height();
            bstride = size.width() * 2;
            format = QImage::Format_RGB16;
        } else {
            format = QtAndroid::qAndroidBitmapFormat_to_QImageFormat(binfo.format);
            if (format == QImage::Format_Invalid) {
                qCritical()<<"Don't know how to create bitmap of this Android bitmap format:"<<binfo.format;
                env->DeleteLocalRef(newBitmap);
                if (bitmap != 0)
                {
                    env->DeleteGlobalRef(bitmap);
                    bitmap = 0;
                }
                imageOnBitmap = QImage();
                return false;
            }
        }
        if (size.width() != bwidth || size.height() != bheight)
            qWarning()<<"Android bitmap size:"<<bwidth<<"x"<<bheight
                      <<"is different from the requested size:"<<size.width()<<"x"<<size.height();
    } else {
        // Android 1.6 / API4
        // Supporting only 16 bit modes.
        bwidth = size.width();
        bheight = size.height();
        bstride = size.width() * 2;
        format = QImage::Format_RGB16;
    }

    qDebug()<<"AndroidBitmapInfo: width:"<<bwidth<<"height:"<<bheight
            <<"stride:"<<bstride<<"QImage::format:"<<static_cast<int>(format);

    //
    // Lock Android bitmap's pixels so we could create a QImage over it
    //

    void* ptr = 0;
    int errsv = AndroidBitmap_lockPixels(env, newBitmap, &ptr);
    if (errsv != 0) {
        qCritical()<<"Could not get new surface pointer, error:"<<errsv;
        env->DeleteLocalRef(newBitmap);
        if (bitmap != 0) {
            env->DeleteGlobalRef(bitmap);
            bitmap = 0;
        }
        imageOnBitmap = QImage();
        return false;
    }
    if( !ptr ) {
        qCritical()<<"Could not get new surface pointer, null pointer returned.";
        env->DeleteLocalRef(newBitmap);
        if (bitmap != 0) {
            env->DeleteGlobalRef(bitmap);
            bitmap = 0;
        }
        imageOnBitmap = QImage();
        return false;
    }
    // qDebug("Locked pixels address: %p", ptr);


    //
    // Create QImage in the same memory area as the Android bitmap
    //

    // "Constructs an image with the given width, height and format,
    // that uses an existing memory buffer, data. The width and height
    // must be specified in pixels. bytesPerLine specifies the number
    // of bytes per line (stride)."
    qDebug()<<"Constructing QImage buffer:"<<bwidth<<"x"<<bheight
            <<bstride<<static_cast<int>(format);
    imageOnBitmap = QImage(
            static_cast<uchar*>(ptr),
            bwidth,
            bheight,
            bstride,
            format);
    if (imageOnBitmap.isNull())
        qCritical()<<"Error: called QImage constructor but got null image! Memory error?";

    //
    // Delete reference to the old Android bitmap and relink to the new one
    //

    if (bitmap != 0)
        env->DeleteGlobalRef(bitmap);
    bitmap = env->NewGlobalRef(newBitmap);
    env->DeleteLocalRef(newBitmap);

    return true;
}

bool QJniImagePair::resize(const QSize& size)
{
    bool result = resize(mImageOnBitmap, mBitmap, size);
    if( !result )
        dummy();
    return result;
}


QT_END_NAMESPACE
