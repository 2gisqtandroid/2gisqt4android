/*
  Lite - a QPA windowing plugin for Qt/Android.

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QDialog>
#include <QtGui/QMainWindow>
#include <QtGui/QFont>
#include <QtGui/QPen>
#include <QtGui/QBrush>
#include "qlitewindowdecorations.h"

#ifndef QT_ANDROID_LITE_NO_MULTYTLW

namespace QtAndroid {

bool isDecoratedWindow(const QWidget* widget)
{
    if( widget->windowFlags() & Qt::FramelessWindowHint )
        return false;
    if( widget->windowFlags() & Qt::Window ||
        widget->windowFlags() & Qt::Dialog )
        return true;

/*
    if( dynamic_cast<const QDialog*>(widget)!=0 )
        return true;
#ifndef QT_NO_MAINWINDOW
    if( dynamic_cast<const QMainWindow*>(widget)!=0 )
        return true;
#endif
*/
    return false;
}

void drawDecorations(QPainter* p, const QRect& rect, const QString& caption)
{
    // TODO - implement better decorations, probably incl. window header
    p->setPen(QPen(QBrush(Qt::black), 5));
    p->drawRect(rect);

    if( caption.length() ) {
        int hh = 30; // TODO calculate taking screen params into account
        // TODO better font size calculation
        p->setFont(QFont("Sans", 9, QFont::Bold));
        QRect hdrect(rect.left(), rect.top()-hh, rect.width(), hh);
        p->drawRect(hdrect);
        p->setBrush(Qt::gray);
        p->fillRect(hdrect, Qt::darkGray);
        p->setBrush(Qt::white);
        p->setPen(Qt::white);
        // TODO - use QStaticText
        p->drawText(hdrect, Qt::AlignHCenter | Qt::AlignVCenter, caption );
    }
}

} // namespace QtAndroid

#endif
