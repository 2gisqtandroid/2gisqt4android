#ifndef QT_NO_QUUID_STRING
#  define QT_NO_QUUID_STRING
#endif

//#ifndef QT_NO_STL
//#  define QT_NO_STL
//#endif

/* Dialogs */
#ifndef QT_NO_COLORDIALOG
#  define QT_NO_COLORDIALOG
#endif
#ifndef QT_NO_ERRORMESSAGE
#  define QT_NO_ERRORMESSAGE
#endif
#ifndef QT_NO_FONTDIALOG
#  define QT_NO_FONTDIALOG
#endif
#ifndef QT_NO_INPUTDIALOG
#  define QT_NO_INPUTDIALOG
#endif
#ifndef QT_NO_MESSAGEBOX
#  define QT_NO_MESSAGEBOX
#endif
#ifndef QT_NO_PRINTDIALOG
#  define QT_NO_PRINTDIALOG
#endif
#ifndef QT_NO_PROGRESSDIALOG
#  define QT_NO_PROGRESSDIALOG
#endif
#ifndef QT_NO_TABDIALOG
#  define QT_NO_TABDIALOG
#endif
#ifndef QT_NO_WIZARD
#  define QT_NO_WIZARD
#endif
#ifndef QT_NO_FILEDIALOG
#  define QT_NO_FILEDIALOG
#endif

/* File I/O */
#ifndef QT_NO_DOM
#  define QT_NO_DOM
#endif
#ifndef QT_NO_FILESYSTEMWATCHER
#  define QT_NO_FILESYSTEMWATCHER
#endif
#ifndef QT_NO_PROCESS
#  define QT_NO_PROCESS
#endif

/* Fonts */
#ifndef QT_NO_QWS_QPF
#  define QT_NO_QWS_QPF
#endif
#ifndef QT_NO_QWS_QPF2
#  define QT_NO_QWS_QPF2
#endif

/* Images */
#ifndef QT_NO_IMAGEFORMATPLUGIN
#  define QT_NO_IMAGEFORMATPLUGIN
#endif
#ifndef QT_NO_IMAGEFORMAT_BMP
#  define QT_NO_IMAGEFORMAT_BMP
#endif
#ifndef QT_NO_IMAGEFORMAT_JPEG
#  define QT_NO_IMAGEFORMAT_JPEG
#endif
#ifndef QT_NO_IMAGEFORMAT_PPM
#  define QT_NO_IMAGEFORMAT_PPM
#endif
#ifndef QT_NO_IMAGEFORMAT_XBM
#  define QT_NO_IMAGEFORMAT_XBM
#endif
#ifndef QT_NO_IMAGEFORMAT_XPM
#  define QT_NO_IMAGEFORMAT_XPM
#endif
#ifndef QT_NO_IMAGE_HEURISTIC_MASK
#  define QT_NO_IMAGE_HEURISTIC_MASK
#endif
#ifndef QT_NO_IMAGE_TEXT
#  define QT_NO_IMAGE_TEXT
#endif
#ifndef QT_NO_MOVIE
#  define QT_NO_MOVIE
#endif

/* Internationalization */
#ifndef QT_NO_BIG_CODECS
#  define QT_NO_BIG_CODECS
#endif
#ifndef QT_NO_QWS_INPUTMETHODS
#  define QT_NO_QWS_INPUTMETHODS
#endif
#ifndef QT_NO_TEXTCODECPLUGIN
#  define QT_NO_TEXTCODECPLUGIN
#endif

/* ItemViews */
#ifndef QT_NO_DATAWIDGETMAPPER
#  define QT_NO_DATAWIDGETMAPPER
#endif
#ifndef QT_NO_LISTVIEW
#  define QT_NO_LISTVIEW
#endif
#ifndef QT_NO_STRINGLISTMODEL
#  define QT_NO_STRINGLISTMODEL
#endif
#ifndef QT_NO_TABLEVIEW
#  define QT_NO_TABLEVIEW
#endif
#ifndef QT_NO_TREEVIEW
#  define QT_NO_TREEVIEW
#endif

/* Kernel */
#ifndef QT_NO_CONCURRENT
#  define QT_NO_CONCURRENT
#endif
#ifndef QT_NO_CSSPARSER
#  define QT_NO_CSSPARSER
#endif
#ifndef QT_NO_CURSOR
#  define QT_NO_CURSOR
#endif
#ifndef QT_NO_EFFECTS
#  define QT_NO_EFFECTS
#endif
#ifndef QT_NO_SESSIONMANAGER
#  define QT_NO_SESSIONMANAGER
#endif
#ifndef QT_NO_SHORTCUT
#  define QT_NO_SHORTCUT
#endif
#ifndef QT_NO_SOUND
#  define QT_NO_SOUND
#endif
#ifndef QT_NO_TABLETEVENT
#  define QT_NO_TABLETEVENT
#endif
#ifndef QT_NO_XMLSTREAM
#  define QT_NO_XMLSTREAM
#endif

/* Networking */
#ifndef QT_NO_COP
#  define QT_NO_COP
#endif
#ifndef QT_NO_NETWORKDISKCACHE
#  define QT_NO_NETWORKDISKCACHE
#endif
// Needed for UDP sockets
#ifndef QT_NO_NETWORKINTERFACE
#  define QT_NO_NETWORKINTERFACE
#endif
#ifndef QT_NO_SOCKS5
#  define QT_NO_SOCKS5
#endif
#ifndef QT_NO_UDPSOCKET
#  define QT_NO_UDPSOCKET
#endif
#ifndef QT_NO_FTP
#  define QT_NO_FTP
#endif

/* Painting */
#ifndef QT_NO_DIRECTPAINTER
#  define QT_NO_DIRECTPAINTER
#endif
#ifndef QT_NO_PAINTONSCREEN
#  define QT_NO_PAINTONSCREEN
#endif
#ifndef QT_NO_PAINT_DEBUG
#  define QT_NO_PAINT_DEBUG
#endif
#ifndef QT_NO_PICTURE
#  define QT_NO_PICTURE
#endif
#ifndef QT_NO_PRINTER
#  define QT_NO_PRINTER
#endif

/* Phonon */
#ifndef QT_NO_PHONON_ABSTRACTMEDIASTREAM
#  define QT_NO_PHONON_ABSTRACTMEDIASTREAM
#endif
#ifndef QT_NO_PHONON_AUDIOCAPTURE
#  define QT_NO_PHONON_AUDIOCAPTURE
#endif
#ifndef QT_NO_PHONON_EFFECT
#  define QT_NO_PHONON_EFFECT
#endif
#ifndef QT_NO_PHONON_MEDIACONTROLLER
#  define QT_NO_PHONON_MEDIACONTROLLER
#endif
#ifndef QT_NO_PHONON_OBJECTDESCRIPTIONMODEL
#  define QT_NO_PHONON_OBJECTDESCRIPTIONMODEL
#endif
#ifndef QT_NO_PHONON_PLATFORMPLUGIN
#  define QT_NO_PHONON_PLATFORMPLUGIN
#endif
#ifndef QT_NO_PHONON_SEEKSLIDER
#  define QT_NO_PHONON_SEEKSLIDER
#endif
#ifndef QT_NO_PHONON_SETTINGSGROUP
#  define QT_NO_PHONON_SETTINGSGROUP
#endif
#ifndef QT_NO_PHONON_VIDEO
#  define QT_NO_PHONON_VIDEO
#endif
#ifndef QT_NO_PHONON_VOLUMESLIDER
#  define QT_NO_PHONON_VOLUMESLIDER
#endif

/* Qt for Embedded Linux */
#ifndef QT_NO_QWSEMBEDWIDGET
#  define QT_NO_QWSEMBEDWIDGET
#endif
#ifndef QT_NO_QWS_ALPHA_CURSOR
#  define QT_NO_QWS_ALPHA_CURSOR
#endif
#ifndef QT_NO_QWS_DECORATION_DEFAULT
#  define QT_NO_QWS_DECORATION_DEFAULT
#endif
#ifndef QT_NO_QWS_KEYBOARD
#  define QT_NO_QWS_KEYBOARD
#endif
#ifndef QT_NO_QWS_MOUSE
#  define QT_NO_QWS_MOUSE
#endif
#ifndef QT_NO_QWS_MOUSE_AUTO
#  define QT_NO_QWS_MOUSE_AUTO
#endif
#ifndef QT_NO_QWS_MULTIPROCESS
#  define QT_NO_QWS_MULTIPROCESS
#endif
#ifndef QT_NO_QWS_PROXYSCREEN
#  define QT_NO_QWS_PROXYSCREEN
#endif

/* Styles */
#ifndef QT_NO_STYLE_MOTIF
#  define QT_NO_STYLE_MOTIF
#endif
#ifndef QT_NO_STYLE_ANDROID
#  define QT_NO_STYLE_ANDROID
#endif

/* Utilities */
#ifndef QT_NO_ACCESSIBILITY
#  define QT_NO_ACCESSIBILITY
#endif
#ifndef QT_NO_COMPLETER
#  define QT_NO_COMPLETER
#endif
//#ifndef QT_NO_DESKTOPSERVICES
//#  define QT_NO_DESKTOPSERVICES
//#endif
#ifndef QT_NO_GESTURES
#  define QT_NO_GESTURES
#endif
#ifndef QT_NO_STATEMACHINE
#  define QT_NO_STATEMACHINE
#endif
#ifndef QT_NO_SYSTEMTRAYICON
#  define QT_NO_SYSTEMTRAYICON
#endif
#ifndef QT_NO_UNDOCOMMAND
#  define QT_NO_UNDOCOMMAND
#endif

/* Widgets */
#ifndef QT_NO_GROUPBOX
#  define QT_NO_GROUPBOX
#endif
#ifndef QT_NO_LCDNUMBER
#  define QT_NO_LCDNUMBER
#endif
#ifndef QT_NO_SPINBOX
#  define QT_NO_SPINBOX
#endif
#ifndef QT_NO_MAINWINDOW
#  define QT_NO_MAINWINDOW
#endif
#ifndef QT_NO_MENUBAR
#  define QT_NO_MENUBAR
#endif
#ifndef QT_NO_PROGRESSBAR
#  define QT_NO_PROGRESSBAR
#endif
#ifndef QT_NO_RESIZEHANDLER
#  define QT_NO_RESIZEHANDLER
#endif
#ifndef QT_NO_SPLITTER
#  define QT_NO_SPLITTER
#endif
#ifndef QT_NO_SIGNALMAPPER
#  define QT_NO_SIGNALMAPPER
#endif
#ifndef QT_NO_SIZEGRIP
#  define QT_NO_SIZEGRIP
#endif
#ifndef QT_NO_GRAPHICSEFFECT
#  define QT_NO_GRAPHICSEFFECT
#endif
#ifndef QT_NO_MDIAREA
#  define QT_NO_MDIAREA
#endif
#ifndef QT_NO_TEXTEDIT
#  define QT_NO_TEXTEDIT
#endif
#ifndef QT_NO_SPINWIDGET
#  define QT_NO_SPINWIDGET
#endif
#ifndef QT_NO_SPLASHSCREEN
#  define QT_NO_SPLASHSCREEN
#endif
#ifndef QT_NO_STACKEDWIDGET
#  define QT_NO_STACKEDWIDGET
#endif
#ifndef QT_NO_STATUSBAR
#  define QT_NO_STATUSBAR
#endif
#ifndef QT_NO_STATUSTIP
#  define QT_NO_STATUSTIP
#endif
#ifndef QT_NO_TOOLBUTTON
#  define QT_NO_TOOLBUTTON
#endif
#ifndef QT_NO_TOOLTIP
#  define QT_NO_TOOLTIP
#endif
#ifndef QT_NO_VALIDATOR
#  define QT_NO_VALIDATOR
#endif

/* Windows */
#ifndef QT_NO_WIN_ACTIVEQT
#  define QT_NO_WIN_ACTIVEQT
#endif

/* Symbian */
#ifndef QT_NO_SOFTKEYMANAGER
#  define QT_NO_SOFTKEYMANAGER
#endif

//#ifndef QT_NO_STYLE_S60
//# define QT_NO_STYLE_S60
//#endif

#ifndef QT_NO_MENU
#  define QT_NO_MENU
#endif
#ifdef QT_SOFTKEYS_ENABLED
#undef QT_SOFTKEYS_ENABLED
#endif

#ifndef QT_NO_WHATSTHIS
#  define QT_NO_WHATSTHIS
#endif

#ifndef QT_NO_ACTION
#  define QT_NO_ACTION
#endif

#ifndef QT_NO_DRAGANDDROP
#  define QT_NO_DRAGANDDROP
#endif

#ifndef QT_NO_CODECS
#  define QT_NO_CODECS
#endif

#ifndef QT_NO_ICONV
#  define QT_NO_ICONV
#endif

#ifndef QT_NO_QFUTURE
#  define QT_NO_QFUTURE
#endif

#ifndef QT_NO_FILESYSTEMWATCHER
#  define QT_NO_FILESYSTEMWATCHER
#endif

//#ifndef QT_NO_TEMPORARYFILE
//#  define QT_NO_TEMPORARYFILE
//#endif

//#ifndef QT_NO_DATASTREAM
//#  define QT_NO_DATASTREAM
//#endif

#ifndef QT_DATASTREAM_CLASS
#  define QT_DATASTREAM_CLASS
#endif

//#ifndef QT_NO_DEBUG_STREAM
//#  define QT_NO_DEBUG_STREAM
//#endif

#ifndef QT_NO_USERDATA
#  define QT_NO_USERDATA
#endif

#ifndef QT_NO_FILESYSTEMMODEL
#  define QT_NO_FILESYSTEMMODEL
#endif

#ifndef QT_NO_FSCOMPLETER
#  define QT_NO_FSCOMPLETER
#endif

//#ifndef QT_NO_RUBBERBAND
//#  define QT_NO_RUBBERBAND
//#endif

//убрать
//#ifndef QT_NO_ITEMVIEWS
//#  define QT_NO_ITEMVIEWS
//#endif

//#ifndef QT_NO_COLUMNVIEW
//#  define QT_NO_COLUMNVIEW
//#endif

//#ifndef QT_NO_DIRMODEL
//#  define QT_NO_DIRMODEL
//#endif

#ifndef QT_NO_FILEICONPROVIDER
#  define QT_NO_FILEICONPROVIDER
#endif

//#ifndef QT_NO_LISTWIDGET
//#  define QT_NO_LISTWIDGET
//#endif

//#ifndef QT_NO_STANDARDITEMMODEL
//#  define QT_NO_STANDARDITEMMODEL
//#endif

#ifndef QT_NO_TOOLBAR
#  define QT_NO_TOOLBAR
#endif

//#ifndef 
//#  define QT_NO_MATRIX4X4
//#endif

#ifndef QT_NO_QUATERNION
#  define QT_NO_QUATERNION
#endif

//#ifndef QT_NO_VECTOR2D
//#  define QT_NO_VECTOR2D
//#endif

//#ifndef QT_NO_VECTOR3D
//#  define QT_NO_VECTOR3D
//#endif

//#ifndef QT_NO_VECTOR4D
//#  define QT_NO_VECTOR4D
//#endif

#ifndef QT_NO_PRINTPREVIEWWIDGET
#  define QT_NO_PRINTPREVIEWWIDGET
#endif

#ifndef QT_NO_TABBAR
#  define QT_NO_TABBAR
#endif

//#ifndef QT_NO_STYLE_PROXY
//#  define QT_NO_STYLE_PROXY
//#endif

#ifndef QT_NO_SYNTAXHIGHLIGHTER
#  define QT_NO_SYNTAXHIGHLIGHTER
#endif

#ifndef QT_NO_TEXTODFWRITER
#  define QT_NO_TEXTODFWRITER
#endif

#ifndef QT_NO_UNDOGROUP
#  define QT_NO_UNDOGROUP
#endif

#ifndef QT_NO_UNDOVIEW
#  define QT_NO_UNDOVIEW
#endif

#ifndef QT_NO_CALENDARWIDGET
#  define QT_NO_CALENDARWIDGET
#endif

#ifndef QT_NO_COMBOBOX
#  define QT_NO_COMBOBOX
#endif

#ifndef QT_NO_DATETIMEEDIT
#  define QT_NO_DATETIMEEDIT
#endif

#ifndef QT_NO_DIAL
#  define QT_NO_DIAL
#endif

#ifndef QT_NO_DOCKWIDGET
#  define QT_NO_DOCKWIDGET
#endif

#ifndef QT_NO_FONTCOMBOBOX
#  define QT_NO_FONTCOMBOBOX
#endif

#ifndef QT_NO_TABWIDGET
#  define QT_NO_TABWIDGET
#endif

#ifndef QT_NO_TEXTBROWSER
#  define QT_NO_TEXTBROWSER
#endif

#ifndef QT_NO_TOOLBOX
#  define QT_NO_TOOLBOX
#endif

#ifndef QT_NO_WORKSPACE
#  define QT_NO_WORKSPACE
#endif

//#ifndef QT_NO_ICON
//#  define QT_NO_ICON
//#endif

//#ifndef QT_NO_NETWORKPROXY
//#  define QT_NO_NETWORKPROXY
//#endif

#ifndef QT_NO_OPENSSL
#  define QT_NO_OPENSSL
#endif


