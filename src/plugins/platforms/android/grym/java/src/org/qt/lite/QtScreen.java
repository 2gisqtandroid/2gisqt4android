/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, 2012 DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.lite;

import android.util.Log;
import android.content.Context;

import android.view.View;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.MotionEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
//import android.graphics.Paint;

import org.qt.util.SplashScreenPainter;
import org.qt.core.QtApplicationBase;
import org.qt.core.QtInputCommon;
import org.qt.core.QtLibraryLoader;


public class QtScreen extends SurfaceView implements SurfaceHolder.Callback
{
    private QtApplication mApplication;
    private int mWidth, mHeight;
    private boolean valid = false;
    static private boolean qt_started_to_paint = false;
    private Rect mBlitSrc, mBlitDst, mBlitLocked;

    private String tag = "";
    private String tag() {
        return tag;
    }

    public QtScreen(Context context, QtApplication app) {
        super(context);
        Log.d(tag(), "QtScreen()");
        getHolder().addCallback(this);
        mApplication = app;
        tag = QtActivity.mTag + " QtScreen"; 

        setFocusable(true);
        setFocusableInTouchMode(true); // this is very required for soft keyboard
    }

    public int width() { return mWidth; }
    public int height() { return mHeight; }

    // SurfaceHolder.Callback: "This is called immediately after any structural changes
    // (format or size) have been made to the surface. You should at this point update
    // the imagery in the surface."
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        Log.d(QtActivity.mTag, "surfaceChanged: format="+format+", "+width+"x"+height);
        mWidth = width; 
        mHeight = height;
        valid = true;
        mApplication.screenChanged(this);

        mBlitSrc = new Rect(0, 0, mWidth, mHeight);
        mBlitDst = new Rect(0, 0, mWidth, mHeight);
        mBlitLocked = new Rect(0, 0, mWidth, mHeight);

        DrawSplash();
    }

    public void repaintQt()
    {
        Log.d(QtActivity.mTag, "QtScreen::repaintQt()");
        if (QtLibraryLoader.isApplicationLoaded() && qt_started_to_paint){
            mApplication.repaint();
        }
        else
        {
            DrawSplash();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        Log.d(QtActivity.mTag, "onSizeChanged: "+w+"x"+h+", old: "+oldw+"x"+oldh);
        super.onSizeChanged(w, h, oldw, oldh);
        repaintQt();
    }

/*
    API Level 8

    @Override
    protected void onVisibilityChanged(View changedView, int visibility)
    {
        Log.d(QtActivity.mTag, "onVisibilityChanged("+visibility+")");
        super.onVisibilityChanged(changedView, visibility);
        if( visibility == VISIBLE )
            repaintQt();
    }
*/

    @Override
    protected void onWindowVisibilityChanged(int visibility)
    {
        Log.d(QtActivity.mTag, "onWindowVisibilityChanged("+visibility+")");
        super.onWindowVisibilityChanged(visibility);
        if( visibility == VISIBLE )
            repaintQt();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        Log.d(QtActivity.mTag, "surfaceCreated");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        Log.d(QtActivity.mTag, "surfaceDestroyed");
        valid = false;
        mApplication.screenDestroyed(this);
    }

    // tell us to update
    // avoiding GC:
    public void blitRect(final Bitmap bmp, final int px, final int py, final int ax, final int ay, final int w, final int h)
    {
        if (!valid || bmp == null || mBlitDst == null || mBlitSrc == null || mBlitLocked == null) {
            return;
        }
        qt_started_to_paint = true;
        int dw = ax + w;
        int dh = ay + h;
        mBlitDst.set(px+ax, py+ay, px+dw, py+dh);
        mBlitSrc.set(ax, ay, dw, dh);
        SurfaceHolder holder = getHolder();
        mBlitLocked.set(mBlitDst);
        Canvas canvas = holder.lockCanvas(mBlitLocked);
        canvas.drawBitmap(bmp, mBlitSrc, mBlitDst, null);
        holder.unlockCanvasAndPost(canvas);
    }

    private void DrawSplash()
    {
        try
        {
            if (!qt_started_to_paint)
            {
                SplashScreenPainter painter = QtApplicationBase.GetSplashScreenPainter();
                if (painter == null)
                    return;
                SurfaceHolder holder = getHolder();
                if (holder == null)
                    return;
                Canvas canvas = holder.lockCanvas();
                if (canvas == null)
                    return;
                painter.DrawSplash(canvas, getWidth(), getHeight());
                holder.unlockCanvasAndPost(canvas);
            }
            else
            {
                QtApplicationBase.SetSplashScreenPainter(null);
            }
        }
        catch(Exception e)
        {
            Log.w(QtActivity.mTag, "Lite DrawSplash exception: "+e);
        }
    }

    /*
    This function is never called in SurfaceView:
    @Override
    protected void onDraw(Canvas canvas)
    }
    */

    //
    // Soft keyboard stuff
    //

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs)
    {
        InputConnection ic = QtInputCommon.onCreateInputConnection(this, outAttrs);
        if( ic != null )
            return ic;
        return super.onCreateInputConnection(outAttrs);
    }

    @Override
    public boolean onCheckIsTextEditor()
    {
        return QtInputCommon.onCheckIsTextEditor();
    }

    //
    // Controllers
    //

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        return QtInputCommon.onTouchEvent(event);
    }

    @Override
    public boolean onTrackballEvent(MotionEvent event)
    {
        if( !QtInputCommon.onTrackballEvent(event) )
            return super.onTrackballEvent(event);
        return true;
    }

}
