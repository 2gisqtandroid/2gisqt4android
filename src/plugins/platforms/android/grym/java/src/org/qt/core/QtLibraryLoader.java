/*
  Qt native library loader.

  Author: Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.core;

import java.lang.ref.WeakReference;
import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import android.util.Log;
import android.content.Context;
import android.app.Activity;
import android.app.Application;
import org.qt.util.AssetUtil;
import org.qt.util.LibraryLoadListener;
import org.qt.core.QtMutex;

public class QtLibraryLoader
{
    // **********************************************************************
    // Requested and Loaded library sets
    // **********************************************************************

    // MINIMUM set of shared libraries to start (when using dynamic linking, of course).
    // Order is important: if A depends on B than B should be listed first.
    private final static String[] m_defaultLibs = {
        "QtCore",
        "QtGui",
        "QAndroidCore"
    };
    private final static LinkedList<String> m_defaultLibsList = new LinkedList<String>(Arrays.asList(m_defaultLibs));
    private static LinkedList<String> m_libsList = m_defaultLibsList;
    private static String m_plugin = "QtAndroidNoGl";
    private static String m_applicationLibrary = "";
    private static Set<String> mLoadedLibraries = new TreeSet<String>();
    private static boolean m_ApplicationLoaded = false;
    private static boolean m_PluginLoaded = false;
    private static WeakReference<LibraryLoadListener> m_Listener = null;

    // **********************************************************************
    // General library loading functions
    // **********************************************************************

    public static void setLoadListener(LibraryLoadListener listener)
    {
        m_Listener = new WeakReference<LibraryLoadListener>( listener );
    }

    public static String apkLibraryDir(Application app)
    {
        if( app == null )
            return "";
        return "/data/data/" + app.getPackageName() + "/lib";
    }

    public static String apkLibraryPath(final String lib, Application app)
    {
        if( app == null )
            return "";
        return apkLibraryDir(app) + "/lib" + lib + ".so";
    }

    public static String qtLibraryPath(final String lib)
    {
        return "/data/local/qt/lib/lib" + lib + ".so";
    }

    public static String systemLocalLibraryPath(final String lib)
    {
        return "/data/local/lib/lib" + lib + ".so";
    }

    public static String systemLibraryPath(final String lib)
    {
        return "/system/lib/lib" + lib + ".so";
    }

    public static boolean isLibraryLoaded(final String lib)
    {
        synchronized( QtMutex.instance ) {
            return mLoadedLibraries.contains(lib);
        }
    }

    // A structure which contains information necessary to verify integrity of a static library.
    static public class QLibraryVerificationInfo
    {
        public String name; // Library file name
        public int size;    // File size in bytes

        QLibraryVerificationInfo()
        {
            name = new String();
            size = 0;
        }
    }

    // Integrity verification table
    private static Map<String, QLibraryVerificationInfo> m_libraryVerificationInfo = null;

    /*
       Add library verification information from a text file of the following format:

       12345 libSomeLib.so
       6789 libSomeOther.so
       ...

       To create such list of files, you may use (run from libs/armeabi):
       $ stat lib*.so* -c "%n %s" > ../../assets/libs-armeabi.txt
       Please note that to satisfy requirements of the LGPL license, the file
       should be editable by a hacker, i.e. not put into binary file or compiled
       as a part of proprietary Java code. A good idea is to use assets.
    */
    public static void addLibrarySizeInformationTextTable(final String file)
    {
        synchronized( QtMutex.instance ){
            if( file==null )
                return;
            String[] lines = file.split("[\\r\\n]+"); // Freaking regexps!
            int count = lines.length;
            if( count==0 )
                return;
            if( m_libraryVerificationInfo==null )
                m_libraryVerificationInfo = new HashMap<String, QLibraryVerificationInfo>();
            for( int i = 0; i<count; i++ ){
                if( lines[i].length()<1 )
                    continue;
                String[] fields = lines[i].split(" ");
                if( fields.length < 2 ){
                    Log.d(QtApplicationBase.QtTAG, "Warning - bad line in library size table: "+lines[i]);
                    continue;
                }
                String n = fields[0];
                String s = fields[1];
                int size = Integer.parseInt(s);
                // Log.d(QtApplicationBase.QtTAG, "*** Library: "+n+", size: "+size);
                if( size <= 0 ){
                    Log.d(QtApplicationBase.QtTAG, "Warning - bad file size in library size table: "+lines[i]);
                    continue;
                }
                QLibraryVerificationInfo vi = new QLibraryVerificationInfo();
                vi.name = n;
                vi.size = size;
                m_libraryVerificationInfo.put(n, vi);
            }
        }
    }

    // Verifies file only if there's an information about it in m_libraryVerificationInfo
    public static boolean verifyFile(final String fullPath)
    {
        synchronized( QtMutex.instance ){
            if( m_libraryVerificationInfo==null )
                return true; // Assume file is OK
            File f = new File(fullPath);
            String name = f.getName();
            if( name.length() < 1 ) // String.isEmpty(): since API Level 9!
                return false; // Bad path
            QLibraryVerificationInfo info = m_libraryVerificationInfo.get(name);
            if( info==null ) {
                Log.d(QtApplicationBase.QtTAG, "No verification information about this library: "+name);
                return true; // Assume file is OK
            }
            long l = f.length();
            if( (long)info.size != l ) {
                Log.d(QtApplicationBase.QtTAG, "File has wrong size: "+name+", expected: "+info.size+", actual: "+l);
                return false;
            }
            Log.d(QtApplicationBase.QtTAG, "File passed check: "+name);
            return true; // Passed the check
        }
    }

    // Load one native dynamic library, located in assets.
    // This function is called by loadLibrary() only and is not synchronized.
    private static boolean loadAssetLibrary(final String lib, final Activity a)
    {
        String tempLib = null;
        try{
            String dll = "lib"+lib+".so";
            // Check if the library exists in assets
            if( !AssetUtil.splitAssetExists(a, dll) )
                return false;
            // OK, the library has been found, now we extract it, load it
            // and then remove it (if not using cache).
            // TODO - proper error reporting: say there's not enough disk space to user
            // if it fails to extract library.
            final boolean useCacheDirectory = false; // TODO: make this selectable elsewhere
            String tempLibDir = (!useCacheDirectory)?
                 a.getApplication().getFilesDir().getPath() + "/tmplib" :
                 a.getApplication().getCacheDir().getPath();
            tempLib = tempLibDir + "/" + dll;
            Log.d(QtApplicationBase.QtTAG, "======== Loading '" + lib + "' as '" + tempLib +
                "' (AssetLibrary)... ========");
            if( !AssetUtil.extractSplitAsset(a, tempLibDir, dll, !useCacheDirectory) ) {
                Log.e(QtApplicationBase.QtTAG, "Failed to extract asset library!");
                return false;
            }
            File f = new File(tempLib);
            // f.setExecutable(true, true); // Available only at API level 9, and not actually necessary
            System.load(tempLib);
            // Register the fact that the library has been loaded successfully
            mLoadedLibraries.add(lib);
            Log.d(QtApplicationBase.QtTAG, "........" + lib + " loaded successfully.");
            if( !useCacheDirectory ) // For cache, assume that system will clean it if necessary
            {
                // Clean up temporary file
                if( !f.delete() ) {
                    Log.e(QtApplicationBase.QtTAG, "Failed to delete asset library temp file: " + tempLib);
                } else {
                    Log.e(QtApplicationBase.QtTAG, "Cleaned up temporary file.");
                }
            }
            return true;
        }
        catch(UnsatisfiedLinkError e) {
            Log.e(QtApplicationBase.QtTAG, "Could not load '" + lib +
               "' because linking failed. Please check that "+
               "all native libraries it requires have been loaded before. Exception: "+e);
        }
        catch(SecurityException e) {
            Log.e(QtApplicationBase.QtTAG, "Could not load '" + lib + "' because of a security exception: "+e);
        }
        catch (Exception e) {
            Log.e(QtApplicationBase.QtTAG, "Could not load '" + lib + "' because of an exception: "+e);
        }
        // Clean up temporary file
        if( tempLib != null ) {
           File f = new File(tempLib);
           f.delete();
        }
        return false;
    }

    // Load one native dynamic library, identified by its name (not file name),
    // e.g.: loadLibrary("QtCore");
    public static synchronized boolean loadLibrary(final String lib, final Activity a)
    {
        final Application app = a.getApplication();
        if( lib.length()<1 ){
            Log.d(QtApplicationBase.QtTAG, "Empty library name!");
            return false;
        }
        synchronized( QtMutex.instance ){
            if(isLibraryLoaded(lib)){
                Log.d(QtApplicationBase.QtTAG, "Library already loaded: "+lib);
                return true;
            }
            try
            {
                //
                // Resolving library path in the following order:
                //
                // 1) APK package libs
                // 2) APK package assets
                // 3) Global Qt lib directory
                // 4) Path returned by System.mapLibraryName()
                // 5) System library directory
                // 6) Using name passed to the function "as is"
                //

                // APK libs
                String resolvedName = apkLibraryPath(lib, app), resolvedAs = "APK";
                File f = new File(resolvedName);
                if (!f.exists())
                {
                    // APK assets
                    if( loadAssetLibrary(lib, a) )
                    {
                         return true;
                    }
                    // Qt library directory
                    resolvedName = qtLibraryPath(lib);
                    f = new File(resolvedName);
                    resolvedAs = "QtLibDir";
                }
                // System.mapLibraryName()
                if (!f.exists())
                {
                    resolvedName = System.mapLibraryName(lib);
                    f = new File(resolvedName);
                    resolvedAs = "mapLibraryName()";
                }
                // System local library directory (/data/local/lib)
                if (!f.exists())
                {
                    resolvedName = systemLocalLibraryPath(lib);
                    f = new File(resolvedName);
                    resolvedAs = "SystemLocalLib";
                }
                // System library directory (/system/lib)
                if (!f.exists())
                {
                    resolvedName = systemLibraryPath(lib);
                    f = new File(resolvedName);
                    resolvedAs = "SystemLib";
                }
                // As Is
                if (!f.exists())
                {
                    resolvedName = lib;
                    f = new File(resolvedName);
                    resolvedAs = "AsIs";
                }
                //
                // Finally, loading the library we've found
                //
                if (f.exists())
                {
                    Log.d(QtApplicationBase.QtTAG, "======== Loading '" + lib + "' as '" + resolvedName + 
                        "' (" + resolvedAs + ")... ========");

                    if( !verifyFile(f.getPath()) ){
                        Log.d(QtApplicationBase.QtTAG,
                              "Could not load '" + lib + "' because the file seems damaged. ***************************************");
                        return false;
                    }

                    // Will throw an exception in case of error
                    System.load(resolvedName);
                    // Register the fact that the library has been loaded successfully
                    mLoadedLibraries.add(lib);
                    mLoadedLibraries.add(resolvedName);
                    Log.d(QtApplicationBase.QtTAG, "........" + lib + " loaded successfully.");
                    return true;
                }
                // TODO: try System.loadLibrary()?
                Log.d(QtApplicationBase.QtTAG,
                    "Could not load '" + lib + "' because the file could not be located. ***************************************");
            }
            catch(UnsatisfiedLinkError e)
            {
                Log.e(QtApplicationBase.QtTAG, "Could not load '" + lib +
                   "' because linking failed. Please check that "+
                   "all native libraries it requires have been loaded before. Exception: "+e);
            }
            catch(SecurityException e){
                Log.e(QtApplicationBase.QtTAG, "Could not load '" + lib + "' because of a security exception: "+e);
            }
            catch (Exception e)
            {
                Log.e(QtApplicationBase.QtTAG, "Could not load '" + lib + "' because of an exception: "+e);
            }
            return false;
        }
    }

    // A handy function to load a set of native dynamic libraries
    public static boolean loadLibraries(final List<String> libraries, final Activity a)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "loadLibraries");

            // Debug
            String llist = "Loading "+libraries.size()+" libraries: ";
            for( Iterator<String> it = libraries.iterator(); it.hasNext(); ){
                llist += it.next();
                if( it.hasNext() )
                    llist += " ";
            }
            Log.d(QtApplicationBase.QtTAG, "........"+llist);
            // End Debug

            LibraryLoadListener listener = null;
            if( m_Listener != null )
                listener = m_Listener.get();

            boolean allOk = true;
            int i = 0;
            for( Iterator<String> it = libraries.iterator(); it.hasNext(); i++ ){
                String lib = it.next();
                if( listener != null )
                    listener.onProgress(i, libraries.size(), lib);
                if( isLibraryLoaded(lib) )
                    continue;
                if( !loadLibrary(lib, a) )
                    allOk = false;
            }
            if( listener != null )
                listener.onProgress(libraries.size(), libraries.size(), null);
            return allOk;
        }
    }

    // **********************************************************************
    // Library set operations
    // **********************************************************************

    public static final LinkedList<String> defaultSharedQtLibraries()
    {
        synchronized( QtMutex.instance ) {
            return m_defaultLibsList;
        }
    }

    public static LinkedList<String> sharedLibraries()
    {
        synchronized( QtMutex.instance ) {
            return m_libsList;
        }
    }

    public static void setLibraries(final List<String> libs)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "setLibraries");
            m_libsList.clear();
            m_libsList.addAll(libs);
        }
    }

    public static void clearLibraries()
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "clearLibraries");
            m_libsList.clear();
        }
    }

    public static void addLibrary(final String lib)
    {
        synchronized( QtMutex.instance ) {
            if (!m_libsList.contains(lib)){
                Log.d(QtApplicationBase.QtTAG, "addLibrary(\""+lib+"\"): adding");
                m_libsList.add(lib);
            }else{
                Log.d(QtApplicationBase.QtTAG, "addLibrary(\""+lib+"\"): library is already loaded");
            }
        }
    }

    public static void removeLibrary(final String lib)
    {
        synchronized( QtMutex.instance ) {
            int index = m_libsList.indexOf(lib);
            if (index != -1){
                Log.d(QtApplicationBase.QtTAG, "removeLibrary(\""+lib+"\"): library unlisted");
                m_libsList.remove(index);
            }else{
                Log.d(QtApplicationBase.QtTAG, "removeLibrary(\""+lib+"\"): library was not listed");
            }
        }
    }

    public static String getPlugin()
    {
        return m_plugin;
    }

    public static void setPlugin(final String pluginLib)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "setPlugin: "+pluginLib+", previous plugin: "+m_plugin);
            m_plugin = pluginLib;
        }
    }

    public static void setApplicationName(String app)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "setApplicationName: "+app+", previous name: "+m_applicationLibrary);
            m_applicationLibrary = app;
        }
    }


    // **********************************************************************
    // Final loading methods
    // **********************************************************************

    public static boolean loadSharedLibraries(final Activity a)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "loadSharedLibraries");
            return loadLibraries(sharedLibraries(), a);
        }
    }

    public static boolean loadPlugin(final Activity a)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "loadPlugin; name=\""+m_plugin+"\"");

            // FIXME: This is an extremely nasty workaround, maybe it should be
            // taken out in future when one figures out WTH happens when some app crash
            // when being restored after sitting in background for few days
            // (although it's harmless for correct apps).
            if( !isQAndroidCoreLoaded() ) {
                Log.d(QtApplicationBase.QtTAG, "........ERROR: QAndroidCore is not loaded! Engaging bad bad workaround...");
                loadLibrary("QtCore", a);
                loadLibrary("QtGui", a);
                loadLibrary("QtNetwork", a);
                loadLibrary("QAndroidCore", a);
            }

            if( m_PluginLoaded ) {
                Log.d(QtApplicationBase.QtTAG, "........Already loaded!");
                return true;
            }
            m_PluginLoaded = loadLibrary(m_plugin, a);
            return m_PluginLoaded;
        }
    }

    public static boolean loadAllRuntime(final Activity a)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "loadAllRuntime");
            boolean shar = loadSharedLibraries(a);
            Log.d(QtApplicationBase.QtTAG, "........Shared libraries loaded: "+shar);
            boolean plug = loadPlugin(a);
            Log.d(QtApplicationBase.QtTAG, "........Plugin: "+plug);
            return shar && plug;
        }
    }

    // Must be called after all runtime is loaded.
    public static boolean loadQtApplication(final Activity a)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "loadQtApplication; name=\""+m_applicationLibrary+"\"");
            m_ApplicationLoaded = loadLibrary(m_applicationLibrary, a);
            return m_ApplicationLoaded;
        }
    }

    public static boolean isQAndroidCoreLoaded()
    {
        synchronized( QtMutex.instance ) {
            return isLibraryLoaded("QAndroidCore");
        }
    }

    public static boolean isPluginLoaded()
    {
        synchronized( QtMutex.instance ) {
            return m_PluginLoaded;
        }
    }

    public static boolean isApplicationLoaded()
    {
        synchronized( QtMutex.instance ) {
            return m_ApplicationLoaded;
        }
    }


}
