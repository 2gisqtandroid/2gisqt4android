/*
  A class which provides Qt/Android input intergation (touch, mouse, keyboard).

  Author:
    Sergey A. Galin, <sergey.galin@gmail.com>

  Contributors:
    Bogdan Vatra, <bog_dan_ro@yahoo.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.core;

import android.util.Log;
import android.view.MotionEvent;
import org.qt.core.QtApplicationBase;
import org.qt.core.QtInputCommon;

public class QtInputCommonMultitouch
{
    // "index" is the index of the finger in the event.
    // Determine finger status for index "index". Return values:
    // 0 - Qt::TouchPointPressed,
    // 1 - Qt::TouchPointMoved,
    // 2 - Qt::TouchPointStationary,
    // 3 - Qt::TouchPointReleased.
    private static int getAction(int index, MotionEvent event)
    {
        int action=event.getAction();
        if (action == MotionEvent.ACTION_MOVE)
        {
            int hsz=event.getHistorySize();
            if (hsz>0)
            {
                float adx = Math.abs(event.getX(index)-event.getHistoricalX(index, hsz-1));
                float ady = Math.abs(event.getY(index)-event.getHistoricalY(index, hsz-1));
                // if( QtInputCommon.mVerboseTouch ) 
                //     Log.d(QtApplicationBase.QtTAG, "........getAction "+index+": adx="+adx+", ady="+ady);
                if ( adx>=1.0 || ady>=1.0 ) {
                    return 1; // Moved
                } else {
                    return 2; // Stationary
                }
            }
            return 1; // Moved
        }

        switch(index)
        {
        case 0:
            if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_1_DOWN)
                return 0; // Pressed
            if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_1_UP)
                return 3; // Released
            break;
        case 1:
            if (action == MotionEvent.ACTION_POINTER_2_DOWN || action == MotionEvent.ACTION_POINTER_DOWN)
                return 0; // Pressed
            if (action == MotionEvent.ACTION_POINTER_2_UP || action == MotionEvent.ACTION_POINTER_UP)
                return 3; // Released
            break;
        case 2:
            if (action == MotionEvent.ACTION_POINTER_3_DOWN || action == MotionEvent.ACTION_POINTER_DOWN)
                return 0; // Pressed
            if (action == MotionEvent.ACTION_POINTER_3_UP || action == MotionEvent.ACTION_POINTER_UP)
                return 3; // Released
            break;
        }
        return 2; // Stationary
    }

    public static void sendTouchEvents(MotionEvent event)
    {
        long time = event.getEventTime ();

        // Begin registering touches
        QtApplicationBase.qt_android_touch_begin();
        // Log.d(QtApplicationBase.QtTAG, "sendTouchEvents "+event.getPointerCount());
        boolean unreleasedFingers = false;

        // Sending all touches
        for (int i=0; i<event.getPointerCount(); i++) {
            int fingerAction = getAction(i, event); // Action (Up/Down/Moved/Stationary)
            if( fingerAction != 3 ) // Not released
                unreleasedFingers = true;
            QtApplicationBase.qt_android_touch_add(
                    event.getPointerId(i), // Pointer Id
                    fingerAction,
                    i==0, // Primary?
                    (int)event.getX(i),
                    (int)event.getY(i),
                    event.getSize(i),
                    event.getPressure(i));
            // Log.d(QtApplicationBase.QtTAG, "........"+i+" pressure="+event.getPressure(i));
        }

        // End sending touches.
        // Using qt_android_touch_end(action), where action is:
        // 0 - QEvent::TouchBegin,
        // 1 - QEvent::TouchUpdate,
        // 2 - QEvent::TouchEnd.
        switch(event.getAction())
        {
        case MotionEvent.ACTION_DOWN:
            //if( QtInputCommon.mVerboseTouch )
            //    Log.d(QtApplicationBase.QtTAG, "sendTouchEvents TouchBegin");
            QtApplicationBase.qt_android_touch_end(0, time); // TouchBegin
            break;
        case MotionEvent.ACTION_UP:
            if( unreleasedFingers ) {
                //if( QtInputCommon.mVerboseTouch )
                //    Log.d(QtApplicationBase.QtTAG, "sendTouchEvents TouchUpdate (finger up)");
                QtApplicationBase.qt_android_touch_end(1, time); // TouchUpdate
            } else { // No unreleased fingers, it's all over
                //if( QtInputCommon.mVerboseTouch )
                //    Log.d(QtApplicationBase.QtTAG, "sendTouchEvents TouchEnd");
                QtApplicationBase.qt_android_touch_end(2, time); // TouchEnd
            }
            break;
        default:
            // if( QtInputCommon.mVerboseTouch )
            //    Log.d(QtApplicationBase.QtTAG, "sendTouchEvents TouchUpdate");
            QtApplicationBase.qt_android_touch_end(1, time); // TouchUpdate
        }
    }
}

