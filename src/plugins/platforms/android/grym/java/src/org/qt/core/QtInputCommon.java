/*
  A class which provides Qt/Android input intergation (touch, mouse, keyboard).

  Authors:
    Sergey A. Galin, <sergey.galin@gmail.com>
    Ivan 'w23' Avdeev, <marflon@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.core;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder; 
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import android.view.KeyEvent;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.EditorInfo;
import org.qt.core.QtWindowInputConnection;
import org.qt.core.QtApplicationBase;
import org.qt.core.QtInputCommonMultitouch;

/*
    Add the following lines into constructor of your View:

    setFocusable(true);
    setFocusableInTouchMode(true); // this is very required for soft keyboard


    Implement the following 2 functions in your View:

    import android.view.inputmethod.EditorInfo;
    import android.view.inputmethod.InputConnection;

    ...

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs)
    {
        InputConnection ic = QtInputCommon.onCreateInputConnection(this, outAttrs);
        if( ic != null )
            return ic;
        return super.onCreateInputConnection(outAttrs);
    }

    @Override
    public boolean onCheckIsTextEditor()
    {
        return QtInputCommon.onCheckIsTextEditor();
    }
*/

public class QtInputCommon
{
    public final static boolean mVerboseTouch = false;
    private static int oldx = 0, oldy = 0; // Used by touch event
    private static boolean mDisableFullscreenSIP = true;
    private static float m_trackballAccDx = 0, m_trackballAccDy = 0;

    // Resets internal input controller-related counters,
    // e.g. when activity is restarted or new view is displayed.
    // It is not fatal if this this function is not called,
    // however, sometimes it may improve user experience.
    public static void resetInput()
    {
        m_trackballAccDx = 0;
        m_trackballAccDy = 0;
    }

    // Usage: ShowSoftwareKeyboard(getContext(), show)
    public static void ShowSoftwareKeyboard(View view, boolean show)
    {
        Log.d(QtApplicationBase.QtTAG, "ShowSoftwareKeyboard("+show+")");
        try {
            if( view == null ){
                Log.e(QtApplicationBase.QtTAG, "ShowSoftwareKeyboard("+show+"): View is null");
                return;
            }
            InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm == null){
                Log.e(QtApplicationBase.QtTAG, "ShowSoftwareKeyboard("+show+"): InputMethodManager is null");
                return;
            }

            // Clear current composing text on SIP/IMM side,
            // so it won't pop up when the SIP/IMM is be open
            // again on the same (or another) text field
            try { // TODO: probably the "try" is an extra, clean this out if the exception never shows up
                imm.restartInput(view);
            } catch ( Exception e ) {
                Log.e(QtApplicationBase.QtTAG,
                    "ShowSoftwareKeyboard("+show+
                    "): Exception in input connection ShowSoftwareKeyboard("+
                    show+"), restartInput: "+e);
            }

            if (show) {
                boolean res = imm.showSoftInput(view, InputMethodManager.SHOW_FORCED, null);
                // Log.d(QtApplicationBase.QtTAG, "ShowSoftwareKeyboard("+show+"): imm.showSoftInput result="+res);
            } else {
                IBinder token = view.getWindowToken();
                if( token != null ) {
                    boolean res = imm.hideSoftInputFromWindow(token, 0);
                    // Log.d(QtApplicationBase.QtTAG, "ShowSoftwareKeyboard("+show+"): imm.hideSoftInputFromWindow result: "+res);
                } else {
                    Log.e(QtApplicationBase.QtTAG, "ShowSoftwareKeyboard("+show+"): Window token is null");
                }
            }
        } catch( Exception e ) {
            Log.e(QtApplicationBase.QtTAG, "ShowSoftwareKeyboard("+show+"): Exception in input connection ShowSoftwareKeyboard("+show+"): "+e);
        }
    }

    public static void ResetIMM(View view)
    {
        try {
            if( view == null ){
                Log.e(QtApplicationBase.QtTAG, "ResetIMM: View is null");
                return;
            }
            InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm == null){
                Log.e(QtApplicationBase.QtTAG, "ResetIMM: InputMethodManager is null");
                return;
            }
            imm.restartInput(view);
        } catch( Exception e ) {
            Log.e(QtApplicationBase.QtTAG, "ResetIMM: Exception in input connection's restartInput: "+e);
        }
    }

    public static boolean getDisableFullscreenSIP()
    {
        return mDisableFullscreenSIP;
    }

    public static void setDisableFullscreenSIP( boolean dis )
    {
        mDisableFullscreenSIP = dis;
    }

    public static void setDisableFullscreenSIP( View view, boolean dis )
    {
        if( mDisableFullscreenSIP != dis ) {
            ShowSoftwareKeyboard(view, false);
            setDisableFullscreenSIP(dis);
        }
    }

    // Function for use in your View; if it returns null, return super's function
    public static InputConnection onCreateInputConnection(View v, EditorInfo outAttrs)
    {
        synchronized( QtMutex.instance ) {
            if( QtApplicationBase.qt_android_java_get_advanced_sip() ){
                Log.d(QtApplicationBase.QtTAG, "onCreateInputConnection");

                //
                // Abort creation of input connection if app/activity is in wrong state.
                //
                QtApplicationBase appinst = QtApplicationBase.getImplementationInstance();
                if( appinst==null ){
                    Log.d(QtApplicationBase.QtTAG, "....ignoring because of null application instance.");
                    return null;
                }
                QtActivityBase ab = appinst.getActivity();
                if( ab==null || ab.isPaused() ){
                    Log.d(QtApplicationBase.QtTAG, "....ignoring because no activity or activity is in wrong state.");
                    return null;
                }

                outAttrs.imeOptions = EditorInfo.IME_NULL;
                if( mDisableFullscreenSIP ) {
                    outAttrs.imeOptions |= EditorInfo.IME_FLAG_NO_EXTRACT_UI;
                }
                outAttrs.inputType = InputType.TYPE_CLASS_TEXT;
                outAttrs.initialCapsMode = 0;
                return new QtWindowInputConnection(v);
            }
            return null;
        }
    }

    // Function for use in your View
    public static boolean onCheckIsTextEditor()
    {
        Log.d(QtApplicationBase.QtTAG, "onCheckIsTextEditor()");
        return QtApplicationBase.qt_android_java_get_advanced_sip();
    }

    public static boolean onTouchEvent(MotionEvent event)
    {
        if( !QtLibraryLoader.isApplicationLoaded() )
            return false;

        if( QtApplicationBase.qt_android_java_get_api_level() > 4 ) {
            // Sending touch events as touches to Qt
            QtInputCommonMultitouch.sendTouchEvents(event);
        }

        // Duplicating touch events as mouse emulation
        long time = event.getEventTime ();

        switch (event.getAction())
        {
        case MotionEvent.ACTION_UP:
            if( mVerboseTouch )
                Log.d(QtApplicationBase.QtTAG, "MotionEvent.ACTION_UP");
            QtApplicationBase.qt_android_mouse_up((int)event.getX(), (int)event.getY(), time);
            return true;

        /*
        // EXP
        case MotionEvent.ACTION_OUTSIDE:
            if( mVerboseTouch )
                Log.d(QtApplicationBase.QtTAG, "MotionEvent.ACTION_OUTSIDE");
            QtApplicationBase.qt_android_mouse_up((int)event.getX(), (int)event.getY());
            return true;
        */

        case MotionEvent.ACTION_DOWN:
            if( mVerboseTouch )
                Log.d(QtApplicationBase.QtTAG, "MotionEvent.ACTION_DOWN");
            QtApplicationBase.qt_android_mouse_down((int)event.getX(), (int)event.getY(), time);
            oldx = (int) event.getX();
            oldy = (int) event.getY();
            return true;

        case MotionEvent.ACTION_MOVE:
            if( mVerboseTouch )
                Log.d(QtApplicationBase.QtTAG, "MotionEvent.ACTION_MOVE");

            /*
            API Level 9
            int flags = event.getFlags();
            Log.d(QtApplicationBase.QtTAG, "MotionEvent.ACTION_MOVE Flags: "+flags); 
            */

            /*
            int eflags = event.getEdgeFlags();
            Log.d(QtApplicationBase.QtTAG, "MotionEvent.ACTION_MOVE Flags: "+eflags);
            */

            int dx = (int) (event.getX() - oldx);
            int dy = (int) (event.getY() - oldy);
            QtApplicationBase.qt_android_mouse_move((int)event.getX(), (int)event.getY(), time);
            oldx = (int) event.getX();
            oldy = (int) event.getY();
            return true;

        default:
            if( mVerboseTouch )
                Log.d(QtApplicationBase.QtTAG, "Unhandled MotionEvent: "+event.getAction());
        }

        return true;
    }

    /*
        // Return super.onTrackballEvent(event) if returned false:
        public boolean onTrackballEvent(MotionEvent event)
        {
            if( !QtInputCommon.onTrackballEvent(event) )
                return super.onTrackballEvent(event);
            return true;
        }
    */


    public static boolean onTrackballEvent(MotionEvent event)
    {
        if( !QtLibraryLoader.isApplicationLoaded() )
            return false;
        switch (event.getAction())
        {
        case MotionEvent.ACTION_UP:
            QtApplicationBase.qt_android_key_up(KeyEvent.KEYCODE_DPAD_CENTER, 0, 0, 0);
            return true;

        case MotionEvent.ACTION_DOWN:
            QtApplicationBase.qt_android_key_down(KeyEvent.KEYCODE_DPAD_CENTER, 0, 0, 0);
            return true;

        case MotionEvent.ACTION_MOVE:
            // getX()/getY() return rotation of trackball equal to the number
            // of DPAD key presses, e.g.: getX() = -3 is equal to 3 presses of LEFT.
            // But, the events can be read from trackball & sent here more frequently,
            // for instance, instead of 1 we can receive 0.1, 0.2, 0.3, 0.1, 0.3.
            // That's why we have to accumulate the values in the float variables.
            m_trackballAccDx += event.getX();
            m_trackballAccDy += event.getY();
            int dx = (int)m_trackballAccDx, dy = (int)m_trackballAccDy;
            m_trackballAccDx -= dx;
            m_trackballAccDy -= dy;
            if( dx > 0 ) {
                for( int i = 0; i < dx; i++ )
                    QtApplicationBase.qt_android_control_key(KeyEvent.KEYCODE_DPAD_RIGHT);
            } else if( dx < 0 ) {
                dx = -dx;
                for( int i = 0; i < dx; i++ )
                   QtApplicationBase.qt_android_control_key(KeyEvent.KEYCODE_DPAD_LEFT);
            }
            if( dy > 0 ) {
                for( int i = 0; i < dy; i++ )
                    QtApplicationBase.qt_android_control_key(KeyEvent.KEYCODE_DPAD_DOWN);
            } else if( dy < 0 ) {
                dy = -dy;
                for( int i = 0; i < dy; i++ )
                   QtApplicationBase.qt_android_control_key(KeyEvent.KEYCODE_DPAD_UP);
            }
            return true;
        }
        return false;
    }
}

