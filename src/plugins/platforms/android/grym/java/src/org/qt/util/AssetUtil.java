/*
  Misc utilites used in Java part of Qt for Android

  Author:
    Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.util;

import java.io.IOException;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
//import java.util.List;
import android.os.Bundle;
import android.app.Activity;
import android.content.res.AssetManager;
import android.util.Log;
import android.os.Environment;
import android.content.Context;

import org.qt.util.StreamUtil;

public class AssetUtil
{
    public static final String TAG = "AssetUtil";

    // We need this for API level < 8
    public static String getExternalFilesDir_My(final Activity a)
    {
        return Environment.getExternalStorageDirectory() + "/Android/data/" +
            a.getApplication().getPackageName() +"/files";
    }

    private static boolean ensureDirectoryExists(final String outPath)
    {
        File outPathFile = new File(outPath);
        if( !outPathFile.exists() ) {
            String dir = outPath;
            if (!dir.endsWith("/"))
                dir += "/";
            Log.d(TAG, "Creating directory: \""+dir+"\"");
            File path = new File(dir);
            try {
                if (!path.mkdirs() ) {
                    Log.e(TAG, "Error creating directory for: \""+path.getPath()+
                         "\" (do I have WRITE_EXTERNAL_STORAGE permission?)");
                    // File.mkdirs() also returns false if the directory already exists.
                    // So in fact we have to re-check again :-/
                    return outPathFile.exists();
                }
            } catch(SecurityException e) {
                Log.e(TAG, "Security exception while creating: \""+path.getPath()+"\", "+e);
                return false;
            } catch(Exception e) {
                Log.e(TAG, "Exception while creating: \""+path.getPath()+"\", "+e);
                return false;
            }
        }
        return true;
    }

    public static boolean extractAsset(final Activity a, final String outPath, final String assetName, final boolean dontCheckExistingFileSize)
    {
        Log.d(TAG, "Checking asset: \""+assetName+"\"; output path: \""+outPath+"\"");
        // Create a path where we will place our private file on external storage.
        if( !ensureDirectoryExists(outPath) )
            return false;
        String outFile = outPath + "/" + assetName;
        File file = new File(outFile);
        try {
            InputStream is = a.getAssets().open(assetName);

            if( !dontCheckExistingFileSize ) {
                if ( file.exists() ) {
                    long asset_size = is.available();
                    long file_size = file.length();
                      if( asset_size == file_size ) {
                        Log.d(TAG, "....File already exists and has the same size: " + file_size);
                        return true;
                    }

                    // TODO: Also check for file checksum or other property?
                    Log.d(TAG, "....Replacing: old size: " + file_size + " new size: " + asset_size);
                } else {
                    Log.d(TAG, "....File does not exist yet, must be extracted: \""+outFile+"\"");
                }
            }

            OutputStream os = new FileOutputStream(file);
            StreamUtil.copyData(is, os);
            is.close();
            os.flush();
            os.close();
        } catch( IOException e ) {
            // Unable to create file, likely because external storage has not enough free space
            // or simply not mounted (SD card removed or mounted as USB storage to a PC).
            Log.e(TAG, "......Error extracting resource to '" + outFile + "', "+e);
            return false;
        }
        return true;
    }

    // Convenience wrapper for a very common use case: extracting assets
    // to application's directory on SD card.
    public static boolean extractAssetToExternalStorage(final Activity a, final String assetName, final boolean dontCheckExistingFileSize)
    {
        String outPath = getExternalFilesDir_My(a);
        return extractAsset(a, outPath, assetName, dontCheckExistingFileSize);
    }


    // For split to chunks assets, generate i-th chunk file name (as created by "split") (i starts from 0).
    // This function supports only 27 chunks, just like split command when it uses 1-char suffix (-a 1).
    private static String chunkName(final String assetName, final int i) throws Exception
    {
        final String chars = "abcdefghijklmnopqrstuvwxyz"; // It's dumb!
        if( i>=chars.length() ) {
            throw new Exception("Assets with more than "+chars.length()+" chunks are not supported!");
        }
        return assetName+"."+chars.charAt(i);
    }

    // Check asset size, returns 0 if asset doesn't exist
    public static long assetSize(final Activity a, final String name)
    {
        try {
            InputStream is = a.getAssets().open(name);
            if( is != null )
                return is.available();
            else
                return 0;
        } catch( IOException e ) {
            return 0;
        }
    }

    // Check if asset which can be extracted via extractSplitAsset() exists
    public static boolean splitAssetExists(final Activity a, final String name)
    {
        try {
            return (assetSize(a, name) > 0) || (assetSize(a, chunkName(name, 0)) > 0);
        } catch (Exception e) { // Eat this, Java compiler!
            return false;
        }
    }

    // Extract an asset split with a command like this:
    // split -b 1048576 -a 1 $FILENAME $FILENAME.
    // This produces chunks named like:
    // $FILENAME.a
    // $FILENAME.b
    // $FILENAME.c
    // ...
    // The important thing is that the chunks should had suffixes like that.
    // -b (chunk size) can be of any value not larger than 1048576, but there's
    // no reason for using anything except 1048576.
    public static boolean extractSplitAsset(final Activity a, final String outPath, final String assetName, final boolean dontCheckExistingFileSize)
    {
        Log.d(TAG, "Extract multi-chunk asset: "+assetName);

        File outFile = new File(outPath + "/" + assetName);
        try {
            // Zero stage - check that output directory exists and writeable
            if( !ensureDirectoryExists(outPath) )
                return false;
            // First stage - scan assets to find how many chunks we have, and their summary size
            long size = 0;
            int count = 0;
            for(;;) {
                String chunk = chunkName(assetName, count);
                // Log.d(TAG, "....Checking for chunk #"+count+": "+chunk);
                long s = assetSize(a, chunk);
                if( s > 0 ) {
                    count++;
                    size += s;
                } else
                    break;
            }
            Log.d(TAG, "....Asset has "+count+" chunks, total size="+size+" bytes");
            if( count==0 ) {
                // Check for non-split asset with the same name
                if( assetSize(a, assetName) > 0 ) {
                    // Fall back to non-split asset
                    return extractAsset(a, outPath, assetName, dontCheckExistingFileSize);
                }
                // Failed to find anything
                return false;
            }

            // Second stage - verify output file
            if( !dontCheckExistingFileSize ) {
                if( outFile.length()==size ) {
                    Log.d(TAG, "....File already exists and has the same size: " + size);
                    return true;
                }
            }

            // Third stage - extract all chunks
            Log.d(TAG, "....Concatenating the chunks...");
            OutputStream os = new FileOutputStream(outFile);
            for( int i = 0; i < count; i++ ) {
                String inn = chunkName(assetName, i);
                Log.d(TAG, "....Chunk #"+i+": "+inn);
                InputStream is = a.getAssets().open(inn);
                StreamUtil.copyData(is, os);
                is.close();
            }
            os.flush();
            os.close();
            Log.d(TAG, "....Success!");
        }catch( IOException e ){
            Log.e(TAG, "....I/O error extracting split resource to '" + outFile + "', "+e);
            return false;
        }catch( Exception e ){
            Log.e(TAG, "....Error extracting split resource to '" + outFile + "', "+e);
            return false;
        }
        return true;
    }

    public static boolean extractSplitAssetToExternalStorage(final Activity a, final String assetName, final boolean dontCheckExistingFileSize)
    {
        String outPath = getExternalFilesDir_My(a);
        return extractSplitAsset(a, outPath, assetName, dontCheckExistingFileSize);
    }

    // Load asset file as a String
    public static String getAssetAsString(final Activity a, final String name, final String charset)
    {
        try {
            InputStream is = a.getAssets().open(name);
            if( is==null ) {
                Log.e(TAG, "....Failed to open asset: '"+name+"'");
                return null;
            }
            int avail = is.available();
            if( avail<=0 ) {
                Log.d(TAG, "....Asset is empty: '"+name+"'");
                return null;
            }
            byte[] buffer = new byte[avail];
            is.read(buffer, 0, avail);
            return new String(buffer, charset);
        }catch( IOException e ){
            Log.e(TAG, "....I/O error reading asset: '"+name+"', "+e);
            return null;
        }catch( Exception e ){
            Log.e(TAG, "....Error reading asset: '"+name+"', "+e);
            return null;
        }
    }

    // Load asset file as a String, using default charset (UTF-8) 
    public static String getAssetAsString(final Activity a, final String name)
    {
        return getAssetAsString(a, name, "UTF-8");
    }

}

