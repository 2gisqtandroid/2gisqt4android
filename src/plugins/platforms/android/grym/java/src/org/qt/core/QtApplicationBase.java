/*
  A prototype for Qt/Android application class which implements the basic
  functionality.

  Author: Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.core;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.util.Iterator;
import java.io.File;
import android.os.Build;
import android.os.Build.VERSION;
import android.app.Activity;
import android.app.Application;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Intent;
import android.graphics.Rect;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import android.content.Context;
import android.provider.Settings.Secure;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import org.qt.core.QtActivityBase;
import org.qt.core.QtClipboard;
import org.qt.core.QtInputCommon;
import org.qt.core.QtLibraryLoader;
import org.qt.util.SplashScreenPainter;

public abstract class QtApplicationBase
{
    public static final String QtTAG = "Qt JAVA";
    private static QtApplicationBase m_ImplementationInstance = null;
    private static boolean m_InputConnectionSip = true;
    private static int m_apiLevel = 0;
    private static SplashScreenPainter m_SplashScreenPainter = null;
    QtLibraryLoader m_Loader = null; // EXP
    Object m_ActivityBaseClass = null; // EXP

    public QtApplicationBase()
    {
        m_Loader = new QtLibraryLoader(); // EXP
        m_ActivityBaseClass = QtActivityBase.class; // EXP
    }

    /*
        This function must be called before running app to set QtApplicationBase implementation.
        For example, one might add the following function in implementation, and call it from
        activity implementation's onCreate().

        public static QtApplicationBase instance()
        {
            if( getImplementationInstance()==null )
                 setImplementationInstance( new QtApplication() );
            return getImplementationInstance();
        }
     */
    protected static void setImplementationInstance( QtApplicationBase inst )
    {
        m_ImplementationInstance = inst;
    }

    public static QtApplicationBase getImplementationInstance()
    {
        return m_ImplementationInstance;
    }

    // **********************************************************************
    // Methods to be reimplemented
    // **********************************************************************

    public abstract QtActivityBase getActivity();

    // Main view, also one used to open SIP
    public abstract View getView();

    // **********************************************************************
    // Utility Methods
    // **********************************************************************

    public static QtActivityBase getActivityStatic()
    {
        QtApplicationBase impl = getImplementationInstance();
        if( impl==null )
            return null;
        return impl.getActivity();
    }

    // **********************************************************************
    // Java-side helpers
    // **********************************************************************

    public static void SetSplashScreenPainter(final SplashScreenPainter painter)
    {
        m_SplashScreenPainter = painter;
    }

    public static SplashScreenPainter GetSplashScreenPainter()
    {
        return m_SplashScreenPainter;
    }

    // **********************************************************************
    // Common Java JNI methods
    // **********************************************************************

    // Native code requests to finish activity.
    // NOTE: this function is used when Qt main() runs in a non-Java thread.
    // If it runs in a thread created by Java, then it simply returns to that
    // thread and it should do finishing activity.
    @SuppressWarnings("unused")
    public static void qt_android_java_finish_activity()
    {
        QtApplicationBase inst = m_ImplementationInstance;
        if( inst == null ){
            Log.d(QtTAG, "qt_android_java_finish_activity(): implementation instance is null");
            System.exit(0);
            return;
        }
        QtActivityBase activity = inst.getActivity();
        if( activity == null )
        {
            Log.d(QtTAG, "qt_android_java_finish_activity() - no activity!");
            System.exit(0);
            return;
        }
        Log.d(QtTAG, "qt_android_java_finish_activity()");
        activity.finish();
    }

    //
    // Enable SIP with text block functions? Default: true.
    // These functions are typically not needed and probably will be taken out in future.
    //
    public static boolean qt_android_java_get_advanced_sip()
    {
        return m_InputConnectionSip;
    }

    public static void qt_android_java_set_advanced_sip(boolean inputconnsip)
    {
        Log.d(QtTAG, "qt_android_java_set_advanced_sip("+inputconnsip+")");
        if( inputconnsip !=m_InputConnectionSip ){
            qt_android_java_set_sip(false);
            m_InputConnectionSip = inputconnsip;
        }
    }

    //
    // Prevent SIP from opening in fullscreen mode? Default: true.
    // Apps which don't use multi-line text editing do better without
    // fullscreen SIP, because it uses a lot of non-trivial, tricky code.
    //
    public static boolean qt_android_java_get_disable_fullscreen_sip()
    {
        return QtInputCommon.getDisableFullscreenSIP();
    }

    public static void qt_android_java_set_disable_fullscreen_sip(final boolean disable)
    {
        if( disable != QtInputCommon.getDisableFullscreenSIP() ) {
            QtApplicationBase inst = m_ImplementationInstance;
            if( inst == null ){
                QtInputCommon.setDisableFullscreenSIP(disable);
                return;
            }
            View v = inst.getView();
            if( v == null )
            {
                QtInputCommon.setDisableFullscreenSIP(disable);
                return;
            }
            QtInputCommon.setDisableFullscreenSIP(v, disable);
        }
    }

    // Show/hide SIP
    public static void qt_android_java_set_sip(final boolean show) // 'show' accessed from inner class
    {
        if( m_ImplementationInstance==null ){
            Log.d(QtTAG, "qt_android_java_set_sip(): ignoring because implementation instance is null.");
            return;
        }
        // Log.d(QtTAG, "qt_android_java_set_sip("+show+") with advanced_sip=="+m_InputConnectionSip);
        final View v = m_ImplementationInstance.getView();
        final QtActivityBase a = m_ImplementationInstance.getActivity(); 
        if (a == null || v == null){
            Log.d(QtTAG, "qt_android_java_set_sip("+show+"): ignoring because View or Activity is null.");
            return;
        }

        //
        // Do not allow to open keyboard if application is already exiting
        //
        if( show && a.isFinishing() ){
            Log.d(QtTAG, "qt_android_java_set_sip("+show+"): ignoring because Activity is finishing.");
            return;
        }
        if( show && a.isPaused() ){
            Log.d(QtTAG, "qt_android_java_set_sip("+show+"): ignoring because Activity is paused.");
            return;
        }

        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                QtInputCommon.ShowSoftwareKeyboard(v, show);
            }
        });
    }

    // Reset SIP/IMM
    public static void qt_android_java_reset_sip()
    {
        if( m_ImplementationInstance==null ){
            Log.d(QtTAG, "qt_android_java_reset_sip(): ignoring because implementation instance is null.");
            return;
        }
        final View v = m_ImplementationInstance.getView();
        final QtActivityBase a = m_ImplementationInstance.getActivity();
        if (a == null || v == null){
            Log.d(QtTAG, "qt_android_java_reset_sip(): ignoring because View or Activity is null.");
            return;
        }
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                QtInputCommon.ResetIMM(v);
            }
        });
    }

    // Kill whole virtual machine. This aborts both native and Java parts of the app.
    public static void qt_android_java_abort_vm()
    {
        if( m_ImplementationInstance==null ){
            Log.d(QtTAG, "qt_android_java_abort_vm(): implementation instance is null");
            return;
        }
        Log.d(QtTAG, "qt_android_java_abort_vm()");
        try {
            if( m_ImplementationInstance.getActivity() != null )
                m_ImplementationInstance.getActivity().setKeepaliveService(false);
            qt_android_java_set_sip(false);
        } catch(Exception e) {
            Log.e(QtTAG, "qt_android_java_abort_vm(): Exception during VM abort procedure: "+e);
        } finally {
            System.exit(0);
        }
    }

    // "Minimize" application.
    public static void qt_android_java_exit_to_main_menu()
    {
        if( m_ImplementationInstance==null ){
            Log.d(QtTAG, "qt_android_java_exit_to_main_menu(): implementation instance is null");
            return;
        }
        Log.d(QtTAG, "qt_android_java_exit_to_main_menu()");
        if( m_ImplementationInstance.getActivity()!=null ){
            Intent homeIntent = new Intent(); 
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setAction( Intent.ACTION_MAIN );
            m_ImplementationInstance.getActivity().startActivity(homeIntent);
        }
    }

    // Get current plugin name. This can be used for information purposes.
    public static String qt_android_java_get_current_plugin()
    {
        return QtLibraryLoader.getPlugin();
    }

    // Get all OS version information as a string-indexed map.
    public static String qt_android_java_get_version_data()
    {
        Build b = new Build();
        String ret = "";
        ret += "BOARD=" + b.BOARD + "\n";
        // ret += "BOOTLOADER=" + b.BOOTLOADER + "\n";  -  API Level 8
        ret += "BRAND=" + b.BRAND + "\n";
        ret += "CPU_ABI=" + b.CPU_ABI + "\n";
        // ret += "CPU_ABI2=" + b.CPU_ABI2 + "\n"; -  API Level 8
        ret += "DEVICE=" + b.DEVICE + "\n";
        ret += "DISPLAY=" + b.DISPLAY + "\n";
        ret += "FINGERPRINT=" + b.FINGERPRINT + "\n";
        // ret += "HARDWARE=" + b.HARDWARE + "\n"; -  API Level 8
        ret += "HOST=" + b.HOST + "\n";
        ret += "ID=" + b.ID + "\n";
        ret += "MANUFACTURER=" + b.MANUFACTURER + "\n";
        ret += "MODEL=" + b.MODEL + "\n";
        ret += "PRODUCT=" + b.PRODUCT + "\n";
        // ret += "RADIO=" + b.RADIO + "\n"; -  API Level 8
        // ret += "SERIAL=" + b.SERIAL + "\n"; - API Level 9 
        ret += "TAGS=" + b.TAGS + "\n";
        ret += "TIME=" + b.TIME + "\n";
        ret += "TYPE=" + b.TYPE + "\n";
        // ret += "UNKNOWN=" + b.UNKNOWN + "\n"; - API Level 8; Value used for when a build property is unknown
        ret += "USER=" + b.USER + "\n";
        Build.VERSION ver = new Build.VERSION();
        ret += "VERSION_CODENAME=" + ver.CODENAME + "\n";
        ret += "VERSION_INCREMENTAL=" + ver.INCREMENTAL + "\n";
        ret += "VERSION_RELEASE=" + ver.RELEASE + "\n";
        ret += "VERSION_SDK=" + ver.SDK + "\n";
        ret += "VERSION_SDK_INT=" + ver.SDK_INT + "\n";
        // Log.d(QtTAG, ret);
        return ret;
    }

    // A faster method to obtain API level of the system only
    public static int qt_android_java_get_api_level()
    {
        if( m_apiLevel < 1 ) {
            Build b = new Build();
            Build.VERSION ver = new Build.VERSION();
            m_apiLevel = ver.SDK_INT;
        }
        return m_apiLevel;
    }

    // Check if system is currently low on memory (RAM)
    // This function may be called from the native app to check
    // whether system is in "low memory" state. If it returns true,
    // app may decide to free some memory or even exit if being
    // in background (see qt_android_application_is_active()).
    public static boolean qt_android_java_low_on_memory()
    {
        try {
            QtActivityBase a = getActivityStatic();
            if( a==null ){
                Log.d(QtTAG, "qt_android_java_low_on_memory(): No activity.");
                return false;
            }
            ActivityManager activityManager = (ActivityManager)a.getSystemService( a.ACTIVITY_SERVICE );
            if( activityManager==null ) {
                Log.e(QtTAG, "qt_android_java_low_on_memory(): Failed to resolve ActivityManager!");
                return false;
            }
            ActivityManager.MemoryInfo mInfo = new ActivityManager.MemoryInfo();
            if( mInfo==null ){
                Log.e(QtTAG, "qt_android_java_low_on_memory(): Failed to create MemoryInfo!");
                return false;
            }
            activityManager.getMemoryInfo( mInfo );
            // Log.e(QtTAG, "qt_android_java_low_on_memory(): "+mInfo.lowMemory);
            return mInfo.lowMemory;
        } catch(Exception e) {
            Log.e(QtTAG, "qt_android_java_low_on_memory(): Exception: "+e);
            return false;
        }
    }

    // Show crash message
    public static void qt_android_java_crash_message(String title, String explanation, String buttonText, boolean halt)
    {
        Log.e(QtTAG, "CRASH: "+title+": "+explanation);
        try {
            QtActivityBase a = getActivityStatic();
            if( a==null ){
                Log.d(QtTAG, "qt_android_java_crash_message(): No activity.");
                if( QtLibraryLoader.isApplicationLoaded() ){
                    qt_android_exit_qtmain();
                }
                System.exit(0);
            }
            a.showCrashMessage(title, explanation, buttonText, halt);
        } catch(Exception e) {
            Log.e(QtTAG, "qt_android_java_crash_message(): Exception: "+e);
            if( QtLibraryLoader.isApplicationLoaded() ){
                qt_android_exit_qtmain();
            }
            System.exit(0);
        }
    }

    // Get package name, e.g.: "org.qt.example.animatedtiles"
    public static String qt_android_java_package_name()
    {
        try {
            QtActivityBase a = getActivityStatic();
            if( a==null )
                return "";
            return a.getApplication().getPackageName();
        } catch (Exception e) {
            Log.e(QtTAG, "qt_android_java_allow_non_market_apps(): Exception: "+e);
            return "";
        }
    }

    // Check if installation of applications not from Market is currently allowed.
    // Note that if it's not allowed and an app is trying to install an APK, the
    // system will just pop up a messagebox with a prompt to change the system
    // setting (or cancel installation). However, this function is useful to handle
    // more on application side and reduce user confusion.
    public static boolean qt_android_java_allow_non_market_apps_installation()
    {
        try {
            QtActivityBase a = getActivityStatic();
            if( a==null )
                return false;
            String allowed = Secure.getString( a.getContentResolver(), Secure.INSTALL_NON_MARKET_APPS );
            int iallowed = Integer.parseInt( allowed );
            return iallowed != 0;
        } catch (Exception e) {
            Log.e(QtTAG, "qt_android_java_allow_non_market_apps(): Exception: "+e);
            return false;
        }
    }

    public static boolean qt_android_java_start_activity(final String action, final Uri data)
    {
        try {
            QtActivityBase a = getActivityStatic();
            if( a==null )
                return false;
            Intent theIntent = new Intent(action);
            if( data != null )
                theIntent.setData(data);
            a.startActivity(theIntent);
            return true;
        } catch (Exception e) {
            Log.e(QtTAG, "qt_android_java_start_activity(\""+action+"\"): Exception: "+e);
            return false;
        }
    }

    public static boolean qt_android_java_open_app_settings()
    {
        return qt_android_java_start_activity(android.provider.Settings.ACTION_APPLICATION_SETTINGS, null);
    }

    public static boolean qt_android_java_open_location_source_settings()
    {
        return qt_android_java_start_activity(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS, null);
    }

    public static boolean qt_android_java_open_market(final String packageName)
    {
        return qt_android_java_start_activity(Intent.ACTION_VIEW, Uri.parse("market://details?id="+packageName));
    }

    // Open Android Market on this application's page
    public static boolean qt_android_java_open_market_self()
    {
        String pkg = qt_android_java_package_name();
        if( pkg.length() < 1 )
            return false;
        return qt_android_java_open_market(pkg);
    }

    // Set text in clipboard
    public static void qt_android_to_clipboard(final String text)
    {
        try {
            QtActivityBase a = getImplementationInstance().getActivity();
            if(a == null) {
                Log.e(QtTAG, "qt_android_to_clipboard(\""+text+"\"): no current activity.");
                return;
            }

            QtClipboard clipboard = new QtClipboard(a);
            clipboard.setText(text);
        } catch (Exception e) {
            Log.e(QtTAG, "qt_android_to_clipboard(\""+text+"\"): Exception: " + e);
        } catch (LinkageError e) {
            Log.e(QtTAG, "qt_android_to_clipboard(\""+text+"\"): Exception: " + e);
        }
    }

    // Get text from clipboard
    public static String qt_android_from_clipboard()
    {
        try {
            QtActivityBase a = getImplementationInstance().getActivity();
            if(a == null) {
                Log.e(QtTAG, "qt_android_from_clipboard(): no current activity.");
                return "";
            }

            QtClipboard clipboard = new QtClipboard(a);
            return clipboard.getText();
        } catch (Exception e) {
            Log.e(QtTAG, "qt_android_from_clipboard(): Exception: " + e);
        } catch (LinkageError e) {
            Log.e(QtTAG, "qt_android_from_clipboard(): Exception: " + e);
        }
        return "";
    }

    // Create context menu.
    public static void qt_android_java_context_menu_create(String caption)
    {
        Log.d(QtTAG, "qt_android_java_context_menu_create("+caption+")");

        getImplementationInstance().contextMenu.clear();
        getImplementationInstance().contextMenu.caption = caption;
    }

    // Clear all context menu data
    public static void qt_android_java_context_menu_clear()
    {
        Log.d(QtTAG, "qt_android_java_context_menu_clear");
        getImplementationInstance().contextMenu.clear();
    }

    // Add new menu item.
    public static void qt_android_java_context_menu_add(String text, int item_id)
    {
        Log.d(QtTAG, "qt_android_java_context_menu_add("+text+", "+item_id+")");
        try
        {
            getImplementationInstance().contextMenu.add(item_id, text);
        } catch (Exception e) {
            Log.e(QtTAG, "qt_android_java_context_menu_add exception: "+e);
        }
    }

    public static void qt_android_java_context_menu_show(final boolean show)
    {
        Log.d(QtTAG, "qt_android_java_context_menu_show(\""+show+"\")");
        final String logPrefix = "qt_android_java_context_menu_show";
        try
        {
            final QtActivityBase act = getImplementationInstance().getActivity();
            if(act == null) {
                Log.e(QtTAG, "qt_android_java_context_menu_show(\""+show+"\"): no current activity.");
                return;
            }
            act.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    try {
                        boolean hide = !show;

                        // Bugfix: menu should always be hidden before opening on Android 2.0.x
                        // or activity's onContextMenuClosed() won't be called if menu is closed
                        // via Back key. See for some detail here:
                        // https://groups.google.com/forum/?fromgroups#!topic/android-developers/ILTECchzZBs
                        // On other versions we do not enable the workaround as it breaks opening
                        // context menu with a hardware key.
                        int api = QtApplicationBase.qt_android_java_get_api_level();
                        if (api >= 5 && api <= 6)
                            hide = true;

                        if (hide)
                        {
                            act.closeContextMenu();
                        }

                        if (show)
                        {
                            QtApplicationBase inst = getImplementationInstance();
                            if (inst == null) {
                                Log.w(QtTAG, logPrefix + ": null instance in run()."); 
                                return;
                            }
                            View v = inst.getView();
                            if (v == null) {
                                Log.w(QtTAG, logPrefix + ": null view in run()."); 
                                return;
                            }
                            act.openContextMenu(v);
                        }
                    } catch (Exception e) {
                        Log.e(QtTAG, logPrefix + ": exception in run(): "+e);
                    }
                }
            });
        }
        catch (Exception e)
        {
            Log.e(QtTAG, logPrefix + "("+show+"): Exception:", e);
        }
        catch (LinkageError e)
        {
            Log.e(QtTAG, logPrefix + "("+show+"): Exception:", e);
        }
    }

    // Create system context menu.
    public static void qt_android_context_menu_create(ContextMenu menu, View v, ContextMenuInfo menuInfo)
    {
        try
        {
            QtContextMenu contextMenu = getImplementationInstance().contextMenu;
            if (!contextMenu.isValid())
            {
                Log.e(QtTAG, "qt_android_context_menu_create: context menu is not valid!");
                return;
            }

            if (contextMenu.caption.trim().length() > 0)
                menu.setHeaderTitle(contextMenu.caption);

            for (int i = 0; i < contextMenu.getItemCount(); ++i)
            {
                menu.add(0, contextMenu.getItemId(i), 0, contextMenu.getItemText(i));
            }
        } catch (Exception e) {
            Log.e(QtTAG, "qt_android_context_menu_create exception: "+e);
        }
    }

    private class QtContextMenu
    {
        public String caption = "Menu";
        private ArrayList<Integer> menu_id = new ArrayList<Integer>();
        private ArrayList<String> menu_text = new ArrayList<String>();

        public void clear()
        {
            caption = null;
            menu_id.clear();
            menu_text.clear();
        }

        public boolean isValid()
        {
            return !menu_id.isEmpty() ;
        }

        public void add(final Integer id, final String text)
        {
            menu_id.add(id);
            menu_text.add(text);
        }

        public int getItemCount()
        {
            return menu_id.size();
        }

        public int getItemId(final Integer idx)
        {
            return menu_id.get(idx);
        }

        public String getItemText(final Integer idx)
        {
            return menu_text.get(idx);
        }
    }

    private QtContextMenu contextMenu = new QtContextMenu();

    // Toast notifications.
    private static Toast toast = null;

    public static void qt_android_toast(final String message, final boolean long_duration)
    {
        final String logPrefix = "qt_android_toast(\""+message+"\", \""+long_duration+"\")";
        Log.d(QtTAG, logPrefix);

        try {
            final QtActivityBase act = getImplementationInstance().getActivity();
            if(act == null) {
                Log.e(QtTAG, logPrefix + ": no current activity.");
                return;
            }

            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (message.trim().length() > 0) {
                            final int duration = long_duration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
                            if (null == toast) {
                                final Context context = getImplementationInstance().getActivity();
                                toast = Toast.makeText(context, message, duration);
                            } else {
                                toast.setText(message);
                                toast.setDuration(duration);
                            }
                            toast.show();
                        } else {
                            if (null != toast) {
                                toast.cancel();
                            }
                        }
                    } catch (Exception e) {
                        Log.e(QtTAG, logPrefix, e);
                    }
                }
            });
        } catch (Exception e) {
            Log.e(QtTAG, logPrefix, e);
        } catch (LinkageError e) {
            Log.e(QtTAG, logPrefix, e);
        }
    }

    public static void qt_android_input_method_select()
    {
        final String logPrefix = "qt_android_input_method_select()";
        Log.d(QtTAG, logPrefix);

        try {
            final QtActivityBase act = getImplementationInstance().getActivity();
            if(act == null) {
                Log.e(QtTAG, logPrefix + ": no current activity.");
                return;
            }

            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        InputMethodManager im = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
                        im.showInputMethodPicker();
                    } catch (Exception e) {
                        Log.e(QtTAG, logPrefix, e);
                    }
                }
            });
        } catch (Exception e) {
            Log.e(QtTAG, logPrefix, e);
        } catch (LinkageError e) {
            Log.e(QtTAG, logPrefix, e);
        }
    }

    public static void doTest()
    {
        // FIXME - clean this up
    }


    // **********************************************************************
    // Common native JNI functions (not plugin-specific)
    // **********************************************************************

    // ----------------------------------------------------------------------
    // libQtAndroidMain.a
    // ----------------------------------------------------------------------

    // Java code wants to start native code main().
    // There are two versions:
    // (1) Native code will spawn a thread and run main() in it.
    // This method is used by mw_grym and Necessitas plugins. It is bad and will be
    // probably taken out of here in future.
    public static native void qt_android_start_qt_application(Object jniProxyObject);
    // (2) Native code will run in current thread (created in Java).
    public static native void qt_android_start_qt_application_this_thread(Object jniProxyObject);


    // ----------------------------------------------------------------------
    // libQAndroidCore.so
    // ----------------------------------------------------------------------

    // Application control and notification methods
    public static native void qt_android_exit_qtmain();
    public static native void qt_android_send_application_activated();
    public static native void qt_android_send_application_deactivated();

    // Screen methods
    public static native void qt_android_set_screen_metrics(int screenWidthPixels,
            int screenHeightPixels, float xdpi, float ydpi, float densityDpi);

    // Pointing device methods
    public static native void qt_android_mouse_down(int x, int y, long eventTime);
    public static native void qt_android_mouse_up(int x, int y, long eventTime);
    public static native void qt_android_mouse_move(int x, int y, long eventTime);
    public static native void qt_android_touch_begin();
    public static native void qt_android_touch_add(int pointerId,
            int action, boolean primary, int x, int y, float size, float pressure);
    public static native void qt_android_touch_end(int action, long eventTime);

    // Keyboard methods
    public static native void qt_android_key_down(int key, int unicode, int modifier, int repeatCount);
    public static native void qt_android_key_up(int key, int unicode, int modifier, int repeatCount);

    // Input connection (soft keyboard)
    public static native void qt_android_input_key_event(int keyCode, int metaStates);
    public static native void qt_android_input_composing_text(String text, int pos);
    public static native void qt_android_input_commit_text(String text, int pos);
    public static native void qt_android_input_apply_text();
    public static native void qt_android_input_reset_text();
    public static native String qt_android_editor_text();
    public static native String qt_android_composing_text();
    public static native int qt_android_cursor_position();
    public static native void qt_android_delete_surrounding_text(int leftLength, int rightLength);

    // This function must be called immediately before the next 3
    public static native boolean qt_android_update_selection_data();
    public static native int qt_android_get_selection_start();
    public static native int qt_android_get_selection_length();
    public static native boolean qt_android_get_is_multiline_edit();
    public static native String qt_android_get_selection_text();
    public static native void qt_android_set_selection(int start, int length);

    // Filesystem layout
    public static native void qt_android_set_internal_files_path(String dir);
    public static native void qt_android_set_external_files_path(String dir);
    public static native void qt_android_set_external_storage_path(String dir);
    public static native void qt_android_set_cache_path(String dir);
    public static native void qt_android_set_apk_lib_path(String dir);

    // Customize initial font location, useful if you distribute fonts
    // along with your application. These functions are not called
    // within Core and default plugin implementations, but can be used
    // in your implementation of Android Activity.
    public static native void qt_android_set_font_path(String dir);
    public static native void qt_android_set_font_mask(String dir);

    // Locale
    public static native void qt_android_set_locale(String lc);

    // This method should be called when item of context menu has been clicked.
    public static native boolean qt_android_java_context_menu_call(int item_id);
    // This method should be called when context menu has been closed.
    public static native boolean qt_android_java_context_menu_closed();

    // **********************************************************************
    // Convenience wrappers for common native JNI functions
    // **********************************************************************

    public static void qt_android_key(int key, int unicode, int modifiers)
    {
        QtApplicationBase.qt_android_key_down(key, unicode, modifiers, 0);
        QtApplicationBase.qt_android_key_up(key, unicode, modifiers, 0);
    }

    public static void qt_android_control_key(int key)
    {
        qt_android_key(key, 0, 0);
    }

}

