/*
  InputConnection implementation used by Qt/Android intergration.

  Authors:
    Sergey A. Galin, <sergey.galin@gmail.com>
  Loosely based on the initial version written by:
    Ivan 'w23' Avdeev, <marflon@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
  The quoted comments are taken from Android API documentation:
  http://developer.android.com/
  Used under terms of Creative Commons license.
*/

package org.qt.core;

import java.util.Map;
import java.util.HashMap;
import java.util.WeakHashMap;
import java.lang.reflect.Array;
import java.lang.Appendable;
import java.lang.CharSequence;
import android.util.Log;
import android.content.Context;
import android.view.View;
import android.view.KeyEvent;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.CompletionInfo;
import android.text.Editable;
import android.text.GetChars;
import android.text.Spannable;
import android.text.InputFilter;
import android.os.Bundle;
import org.qt.core.QtApplicationBase;

// InputConnection for soft keyboard.
// Using InputConnection allows us to support all advanced input methods,
// like predictive input, phonetic input of hieroglyphs and voice recognition.
public class QtWindowInputConnection extends BaseInputConnection
{
    private static String tag;
    private static boolean mFullscreen = false;
    private View mView;
    private static final String c_composingSpanClass = "android.view.inputmethod.ComposingText";

    public QtWindowInputConnection(View targetWindow) 
    {
        super(targetWindow, true);
        mView = targetWindow;
        tag = QtApplicationBase.QtTAG+".QtWindowInputConnection/QAKB";
    }

    // "API to send private commands from an input method
    // to its connected editor. This can be used to provide
    // domain-specific features that are only known between
    // certain input methods and their clients. Note that
    // because the InputConnection protocol is asynchronous,
    // you have no way to get a result back or know if the
    // client understood the command; you can use the information
    // in EditorInfo to determine if a client supports a
    // particular command."
    @Override
    public boolean performPrivateCommand(String action, Bundle data)
    {
        Log.d(tag, "performPrivateCommand ("+action+")");
        // "Returns true if the command was sent (whether or not
        // the associated editor understood it), false if the
        // input connection is no longer valid."
        return true;
    }

    // When device is rotated, and keyboard switches to/from fullscreen
    // modes, this function is called by the IME with "enabled" set to true/false.
    @Override
    public boolean reportFullscreenMode(boolean enabled)
    {
        Log.d(tag, "reportFullscreenMode ("+enabled+")");
        // At least some of 2.1 buggy freaks report fullscreen SIP in horizontal
        // orientation, even if fullscreen mode is totally disabled.
        // I.e. they set "enabled" to true if they WOULD LIKE to use fullscreen
        // mode despite the application disabled it.
        mFullscreen = enabled && !QtInputCommon.getDisableFullscreenSIP();
        return true;
    }

    // TODO: add proper support for the token (it's not used currently)
    int mToken = 0;

    // "Retrieve the current text in the input connection's editor,
    // and monitor for any changes to it. This function returns
    // with the current text, and optionally the input connection
    // can send updates to the input method when its text changes."
    //
    // "This method may fail either if the input connection has become
    // invalid (such as its process crashing) or the client is taking
    // too long to respond with the text (it is given a couple
    // seconds to return). In either case, a null is returned."
    //   - we don't "crash" at the moment (in case the app is not
    //     running properly we just return "empty input"; also we
    //     won't get stuck here unless Qt GUI thread is hanging).
    //
    // "token: Arbitrary integer that can be supplied in the request,
    // which will be delivered back when reporting updates."
    @Override
    public ExtractedText getExtractedText(ExtractedTextRequest request, int flags)
    {
        ExtractedText extx = new ExtractedText();
        int max_chars = 0, max_lines = 0;  // 0 = no limit (for both)
        if( request != null ) {
            Log.d(tag,
                  "getExtractedText with request: flags="+flags+
                  ", request.flags=" + request.flags +
                  ", request.hintMaxChars=" + request.hintMaxChars +
                  ", request.hintMaxLines=" + request.hintMaxLines +
                  ", request.hintMaxChars=" + request.hintMaxChars +
                  ", request.token=" + request.token +
                  " ************");
            // Using 'request' fields:
            // int flags - either 0 or GET_TEXT_WITH_STYLES (ignored for now)
            max_chars = request.hintMaxChars; // 0 = no limit
            max_lines = request.hintMaxLines; // 0 = no limit
            mToken = request.token;
        } else {
            Log.d(tag, "getExtractedText: null request");
        }

        extx.text = fullQtText();
        int cursorPos = fullQtTextCursor();

        // Truncating the text, if specified by request's hints
        if (max_chars > 0 && extx.text.length() > max_chars) {
            // Removing extra characters
            // TODO: probably not FIRST max_chars should be used?
            extx.text = extx.text.subSequence(0, max_chars);
        }
        if (max_lines > 0) {
            // Removing extra lines
            // TODO: probably not FIRST max_lines should be used?
            final String line_separator = "\n";
            String[] lines = extx.text.toString().split(line_separator, max_lines+1);
            if (lines.length > max_lines) { // Otherwise, left extx.text alone
                // Recombine first 'max_lines' into a string.
                // This is not the fastest way but who cares (for now).
                String new_text = "";
                for (int i = 0; i < max_lines; i++) {
                    if (i>0)
                        new_text += line_separator;
                    new_text += lines[i];
                }
                extx.text = new_text;
            }
        }

        Log.d(tag, "getExtractedText ...[text: \""+extx.text+"\"] flags="+flags);

        //
        // Setting partial text offset fields.
        // This is necessary to replace whole text in the view.
        //

        // "The offset in the overall text at which the extracted text starts."
        extx.startOffset = 0;

        // "If the content is a report of a partial text change,
        // this is the offset where the change starts and it runs
        // until partialEndOffset. If the content is the full text,
        // this field is -1."
        // For now, we support only "full text".
        extx.partialStartOffset = -1;

        // "If the content is a report of a partial text change,
        // this is the offset where the change ends."
        extx.partialEndOffset = 0;

        // Possible flags: FLAG_SELECTING, FLAG_SINGLE_LINE.
        extx.flags = 0;

        //
        // Selection fields.
        //
        // Important: "selection" is also used as a cursor position.

        // "The offset where the selection currently starts
        // _within the extracted text_. The real selection start position
        // is at startOffset+selectionStart."
        extx.selectionStart = cursorPos - extx.startOffset;

        // "The real selection end position is at startOffset+selectionEnd."
        extx.selectionEnd = cursorPos - extx.startOffset;

        // If there's a real text selection, use it
        if( QtApplicationBase.qt_android_update_selection_data() ) {
            // Returns -1 if there's no selection
            int start = QtApplicationBase.qt_android_get_selection_start();
            int length = QtApplicationBase.qt_android_get_selection_length();
            boolean ml = QtApplicationBase.qt_android_get_is_multiline_edit();
            if (!ml)
                extx.flags |= ExtractedText.FLAG_SINGLE_LINE;
            if (start >= 0 && length > 0) { // Selection set in Qt
                extx.selectionStart = start - extx.startOffset;
                extx.selectionEnd = start+length - extx.startOffset;
                extx.flags |= ExtractedText.FLAG_SELECTING;
            }
            Log.d(tag, "getExtractedText ...RESULT: "+
                "cursor: "+cursorPos+
                "; Qt_sel_start="+start+
                ", Qt_sel_length="+length+
                "; extx.selectionStart="+extx.selectionStart+
                ", extx.selectionEnd="+extx.selectionEnd+
                "; extx.flags="+extx.flags+
                ", multiline="+(ml?"YES":"NO")
            );
        }
        return extx;
    }

    // Get full text rendered on Qt side: including text already
    // applied into the edit control and composing text.
    static protected String fullQtText()
    {
        String completed_text = QtApplicationBase.qt_android_editor_text();
        String composing_text = QtApplicationBase.qt_android_composing_text();
        int cursorPos = QtApplicationBase.qt_android_cursor_position();
        try {
            String left = completed_text.substring(0, cursorPos);
            String right = (cursorPos >= completed_text.length())?
                "":
                completed_text.substring(cursorPos);
            return left + composing_text + right;
        } catch (Exception e) { // Should not happen, but...
            Log.e(tag, "fullQtText exception: "+e+", completed text=\""+
                completed_text+"\", cursorPos="+cursorPos);
            return completed_text + composing_text; // Weird fallback
        }
    }

    // Get cursor position in full Qt text (see fullQtText()).
    static protected int fullQtTextCursor()
    {
        String composing_text = QtApplicationBase.qt_android_composing_text();
        int cursorPos = QtApplicationBase.qt_android_cursor_position();
        // Assuming that in Qt composing text always goes right after
        // the Qt cursor. So we have to add composing text length
        // to its position.
        return cursorPos + composing_text.length();
    }

    // Own function - resets IME using Qt state (only in fullscreen mode)
    protected void sendTextFromQtToAndroid()
    {
        sendTextFromQtToAndroid(false);
    }

    // Own function - resets IME using Qt state
    protected void sendTextFromQtToAndroid(final boolean sendInNonFullscreen)
    {
        if( mFullscreen || sendInNonFullscreen )
        {
            Log.d(tag, "sendTextFromQtToAndroid(): mFullscreen="+
                mFullscreen+", sendInNonFullscreen="+sendInNonFullscreen);
            InputMethodManager imm = (InputMethodManager)mView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.restartInput(mView);
        }
    }

    // "Have the editor perform an action it has said it can do."
    // E.g.: pressing Enter on soft keyboard comes here with actionCode==0.
    @Override
    public boolean performEditorAction(int actionCode)
    {
        /*
          TODO:
          Add support for these actionCode's:

          EditorInfo.IME_ACTION_DONE // 6
          EditorInfo.IME_ACTION_GO // 2
          EditorInfo.IME_ACTION_NEXT // 5
          EditorInfo.IME_ACTION_NONE // 1
          EditorInfo.IME_ACTION_PREVIOUS // 7, API 11
          EditorInfo.IME_ACTION_SEARCH // 3
          EditorInfo.IME_ACTION_SEND // 4
          EditorInfo.IME_ACTION_UNSPECIFIED // 0 - normally "Enter" goes here

          So far, any actions work as "Enter".
        */

        Log.d(tag, "performEditorAction ("+actionCode+")");
        // Send "Enter" press
        QtApplicationBase.qt_android_input_apply_text();
        QtApplicationBase.qt_android_input_key_event(0x42, 0);
        // QtApplicationBase.qt_android_input_apply_text(); - done by "Enter"
        // In fullscreen, any action needs hiding keyboard first
        if( mFullscreen ) {
            QtApplicationBase.qt_android_java_set_sip(false);
        } else {
            // Restart input now, because any hanging text should get applied,
            // and other stuff may happen on Qt side.
            // (In fullscreen mode that's not necessary as SIP closing
            // already does the job.)
            sendTextFromQtToAndroid(true);
        }
        // "true on success, false if the input connection is no longer valid"
        return true;
    }

    // "Clear the given meta key pressed states in the given input connection."
    // "states: The states to be cleared, may be one or more bits as per KeyEvent.getMetaState()."
    // "Default implementation uses
    // MetaKeyKeyListener.clearMetaKeyState(long, int)
    // to clear the state."
    // TODO: implement this function.
    @Override
    public boolean clearMetaKeyStates(int states)
    {
        Log.d(tag, "clearMetaKeyStates ("+states+")");
        return true;
    }

    // "Send a key event to the process that is currently attached
    // through this input connection. The event will be dispatched
    // like a normal key event..."
    @Override
    public boolean sendKeyEvent(KeyEvent event)
    {
        Log.d(tag, "sendKeyEvent("+event+")");
        // Ignore ups, only downs matter
        if (event.getAction() == KeyEvent.ACTION_UP)
            return true;
        QtApplicationBase.qt_android_input_key_event(event.getKeyCode(), event.getMetaState());
        sendTextFromQtToAndroid();
        return true;
    }

    // "Set composing text around the current cursor position
    // with the given text, and set the new cursor position.
    // Any composing text set previously will be removed automatically."
    @Override
    public boolean setComposingText(CharSequence text, int newCursorPosition)
    {
        Log.d(tag, "setComposingText (\""+text+"\", cursorPosition="+newCursorPosition+")");
        QtApplicationBase.qt_android_input_composing_text(text.toString(), newCursorPosition);
        if( mFullscreen ) {
            Log.d(tag, "....Apply text because we're in fullscreen ***************** ");
            QtApplicationBase.qt_android_input_apply_text();
            sendTextFromQtToAndroid();
        }
        return true;
    }

    // "Get n characters of text before the current cursor position."
    @Override
    public CharSequence getTextBeforeCursor(int length, int flags)
    {
        try {
            // This could be done more effectively with direct
            // calls to qt_...() functions because fullQtText()
            // reconstructs the text using cursor position and
            // here we do it again.
            String text = fullQtText();
            int cursor = fullQtTextCursor();
            String seq = (cursor >= text.length())?
                text:
                text.substring(0, cursor);
            if (seq.length() > length)
                seq = seq.substring(seq.length() - length);
            Log.d(tag, "getTextBeforeCursor(length="+length+", flags="+flags+"): text=\""+
                   text+"\", cursor="+cursor+", beforeCursor=\""+seq+"\"");
            return seq;
        } catch (Exception e) {
            Log.e(tag, "getTextBeforeCursor() exception: "+e);
            return "";
        }
    }

    // "Get n characters of text after the current cursor position."
    @Override
    public CharSequence getTextAfterCursor(int length, int flags)
    {
        try {
            // This could be done more effectively with direct
            // calls to qt_...() functions because fullQtText()
            // reconstructs the text using cursor position and
            // here we do it again.
            String text = fullQtText();
            int cursor = fullQtTextCursor();
            String seq = (cursor >= text.length())?
                "":
                text.substring(cursor);
            if (seq.length() > length)
                seq = seq.substring(0, length);
            Log.d(tag, "getTextAfterCursor(length="+length+", flags="+flags+"): text=\""+
                 text+"\", cursor="+cursor+", afterCursor=\""+seq+"\"");
            return seq;
        } catch (Exception e) {
            Log.e(tag, "getTextAfterCursor() exception: "+e);
            return "";
        }
    }

    // "Commit text to the text box and set the new cursor position.
    // Any composing text set previously will be removed automatically."
    @Override
    public boolean commitText(CharSequence text, int newCursorPosition)
    {
        Log.d(tag, "commitText(\""+text+"\", "+newCursorPosition+")");
        String tx = text.toString();
        QtApplicationBase.qt_android_input_commit_text(tx, newCursorPosition);
        if( mFullscreen ) {
            Log.d(tag, "....Apply text because we're in fullscreen ***************** ");
            QtApplicationBase.qt_android_input_apply_text();
            sendTextFromQtToAndroid();
        }
        return true;
    }

    // "Commit a completion the user has selected from the
    // possible ones previously reported to
    // InputMethodSession.displayCompletions(). This will
    // result in the same behavior as if the user had selected
    // the completion from the actual UI."
    // TODO: implement this function? (not really necessary as
    // we usually don't select completions from the app)
    @Override
    public boolean commitCompletion(CompletionInfo text)
    {
        Log.d(tag, "commitCompletion("+text+")");
        return true;
    }

    // "Delete leftLength characters of text before the current
    // cursor position, and delete rightLength characters of text
    // after the current cursor position, excluding composing text."
    @Override
    public boolean deleteSurroundingText(int leftLength, int rightLength)
    {
        Log.d(tag, "deleteSurroundingText("+leftLength+", "+rightLength+")");
        QtApplicationBase.qt_android_delete_surrounding_text(leftLength, rightLength);
        return true;
    }

    // "Tell the editor that you are starting a batch of editor operations.
    // The editor will try to avoid sending you updates about its state
    // until endBatchEdit() is called."
    @Override
    public boolean beginBatchEdit ()
    {
        Log.d(tag, "beginBatchEdit()");
        return true;
    }

    // "Tell the editor that you are done with a batch edit
    // previously initiated with beginBatchEdit()."
    // - WTF???
    @Override
    public boolean endBatchEdit()
    {
        Log.d(tag, "endBatchEdit()");
        return true;
    }

    // "Have the text editor finish whatever composing text is currently active."
    @Override
    public boolean finishComposingText()
    {        
        Log.d(tag, "finishComposingText()");
        QtApplicationBase.qt_android_input_apply_text();
        // (NB: Don't do sendTextFromQtToAndroid() here - it causes eternal loop.)
        return true;
    }

    // Static functions of InputConnection, not called here:
    // public static int getComposingSpanEnd(Spannable text);
    // public static int getComposingSpanStart(Spannable text);

    // "Retrieve the current capitalization mode in effect
    // at the current cursor position in the text."
    @Override
    public int getCursorCapsMode(int reqModes)
    {
        Log.d(tag, "getCursorCapsMode("+reqModes+")");
        // Possible return values:
        // TextUtils.CAP_MODE_CHARACTERS
        // TextUtils.CAP_MODE_WORDS
        // TextUtils.CAP_MODE_SENTENCES
        // TODO? Implement this somehow (need to pull
        // the desired mode from Qt's text edit).
        return 0;
    }

    /*
    // API Level 9
    // "Mark a certain region of text as composing text.
    // Any composing text set previously will be removed
    // automatically. The default style for composing text is used."
    @Override
    public boolean setComposingRegion(int start, int end)
    {
        Log.d(tag, "setComposingRegion("+start+", "+end+")");
        return true;
    }*/

    // Static functions of InputConnection, not called here:
    // public static void setComposingSpans(Spannable text);

    // "Set the selection of the text editor. To set the cursor
    // position, start and end should have the same value."
    @Override
    public boolean setSelection(int start, int end)
    {
        Log.d(tag, "setSelection("+start+", "+end+")");
        String composing_text = QtApplicationBase.qt_android_composing_text();
        if (composing_text.length()>0) {
            if (start != end) {
                // TODO! What can we do about this situation?!
                // This may cause various incredible results...
                Log.d(tag, "setSelection("+start+", "+end+
                    "): setting selection during composing "+
                    "is not implemented!");
            } else {
                // We're just moving cursor and we have some composing text.
                // In Qt, cursor position is always right before composing text.
                // In Android, cursor position can be anything in the
                // visible string (which includes the composing text).
                // We can't actually fix this situation but here's
                // a workaround which at least works for Swype.
                int cursorPos = QtApplicationBase.qt_android_cursor_position();

                if (start > cursorPos + composing_text.length()) {
                    Log.d(tag, "setSelection("+start+", "+end+
                        "): correcting cursor position (after composing text)");
                    start -= composing_text.length();
                    end = start;
                } else if (start > cursorPos ) {
                    Log.d(tag, "setSelection("+start+", "+end+
                        "): cursor position cannot be set within composing text :(");
                    start = cursorPos;
                    end = start;
                }
            }
        }
        QtApplicationBase.qt_android_set_selection(start, end-start);
        return true;
    }

    // Returned by getEditable()
    class MyEditable implements Editable
    {
        static private final String edttag = "QtWindowInputConnection.MyEditable/QAKB";

        InputFilter[] mFilters; // TODO: implement application of the filters

        class MySpan
        {
            public int start;
            public int end;
            public int flags;

            public MySpan( int s, int e, int f )
            {
                start = s;
                end = e;
                flags = 0;
            }

            public MySpan()
            {
                start = -1;
                end = -1;
                flags = 0;
            }
        }

        private Map<Object, MySpan> mSpans = new WeakHashMap<Object, MySpan>();

        public MyEditable()
        {
            super();
        }

        private String qtText()
        {
            return fullQtText();
        }

        private void composingSpanRemoved()
        {
            QtApplicationBase.qt_android_input_apply_text();
        }

        // "Convenience for replace(length(), length(), text, 0, text.length())"
        @Override
        public Editable append(CharSequence text)
        {
            Log.d(edttag, "append");
            return replace(length(), length(), text, 0, text.length());
        }

        // "Convenience for replace(length(), length(), text, start, end)"
        @Override
        public Editable append(CharSequence text, int start, int end)
        {
            Log.d(edttag, "append");
            return replace(length(), length(), text, start, end);
        }

        // "Convenience for append(String.valueOf(text))."
        @Override
        public Editable append(char text)
        {
            Log.d(edttag, "append: "+text);
            return append(String.valueOf(text));
        }

        // "Convenience for replace(0, length(), "", 0, 0)"
        @Override
        public void clear()
        {
            Log.d(edttag, "clear");
            replace(0, length(), "", 0, 0);
        }

        // "Removes all spans from the Editable,
        // as if by calling removeSpan(Object) on each of them."
        @Override
        public void clearSpans()
        {
            Log.d(edttag, "clearSpans");
            mSpans.clear();
            composingSpanRemoved();
        }

        // "Convenience for replace(st, en, "", 0, 0)"
        @Override
        public Editable delete(int st, int en)
        {
            Log.d(edttag, "delete "+st+".."+en);
            return replace(st, en, "", 0, 0);
        }

        // "Returns the array of input filters that are currently
        // applied to changes to this Editable."
        @Override
        public InputFilter[] getFilters()
        {
            Log.d(edttag, "getFilter");
            return mFilters;
        }

        // "Convenience for replace(where, where, text, start, end)"
        @Override
        public Editable insert(int where, CharSequence text, int start, int end)
        {
            Log.d(edttag, "insert: "+where+", \""+text+"\", "+start+".."+end);
            return replace(where, where, text, start, end);
        }

        // "Convenience for replace(where, where, text, 0, text.length())"
        @Override
        public Editable insert(int where, CharSequence text)
        {
            Log.d(edttag, "insert: "+where+", \""+text+"\"");
            return replace(where, where, text, 0, text.length());
        }

        // "Replaces the specified range (st...en) of text in this
        // Editable with a copy of the slice start...end from source.
        // The destination slice may be empty, in which case
        // the operation is an insertion, or the source slice
        // may be empty, in which case the operation is a deletion.
        //
        // Before the change is committed, each filter that was set
        // with setFilters(InputFilter[]) is given the opportunity
        // to modify the source text.
        //
        // If source is Spanned, the spans from it are preserved
        // into the Editable. Existing spans within the Editable
        // that entirely cover the replaced range are retained,
        // but any that were strictly within the range that was
        // replaced are removed. As a special case, the cursor
        // position is preserved even when the entire range where
        // it is located is replaced."
        //
        // TODO: support spans from source
        @Override
        public Editable replace(int st, int en, CharSequence source, int start, int end)
        {
            Log.d(edttag, "replace: "+st+".."+en+", \""+source+"\", "+
                start+".."+end+" WARNING: INCOMPLETE & UNTESTED FUNCTION!");
            try {
                // Forming the new text
                String text = qtText();
                CharSequence source_fragment = source.subSequence(start, end);
                String old_left = text.substring(0, st);
                String old_right = (en>=text.length()-1)?"": text.substring(en+1);
                CharSequence result = old_left + source_fragment + old_right;

                // Trashing old contents and setting new contents.
                // This could be done more effectively by adding a special
                // function to the Qt side...

                // Set cursor position at end of the span
                QtApplicationBase.qt_android_set_selection(text.length(), 0);

                // Remove text under the span
                deleteSurroundingText(text.length(), 0);

                // Set the old text under the span as a composing text
                commitText(result, 1);

            } catch(Exception e) {
                Log.d(edttag, "replace: "+st+".."+en+", \""+source+"\", "+
                    start+".."+end+" Exception: "+e);
            }
            return this; // "Returns: a reference to this object."
        }

        // "Convenience for replace(st, en, text, 0, text.length())"
        @Override
        public Editable replace(int st, int en, CharSequence text)
        {
            Log.d(edttag, "replace: "+st+".."+en+", \""+text+"\"");
            return replace(st, en, text, 0, text.length());
        }

        // "Sets the series of filters that will be called in
        // succession whenever the text of this Editable is changed,
        // each of which has the opportunity to limit or transform
        // the text that is being inserted."
        @Override
        public void setFilters(InputFilter[] filters)
        {
            Log.d(edttag, "setFilters");
            mFilters = filters;
            // TODO: run the filters immediately
        }

        // "Exactly like String.getChars(): copy chars start through
        // end - 1 from this CharSequence into dest beginning at offset destoff."
        @Override
        public void getChars(int start, int end, char[] dest, int destoff)
        {
            Log.d(edttag, "getChars: "+start+".."+end+", \""+dest+"\", "+destoff);
            CharSequence seq = subSequence(start, end);
            for( int i = 0; i < seq.length(); i++ )
                dest[i+destoff] = seq.charAt(i);
        }

        // "Remove the specified object from the range of text
        // to which it was attached, if any."
        @Override
        public void removeSpan(Object what)
        {
            if( mSpans.containsKey(what) ) {
                Log.d(edttag, "removeSpan (OK) "+what.toString());
                try {
                    String objname = what.getClass().getName();
                    Log.d(edttag, "removeSpan: ...class name: "+objname);
                    if (objname.equals(c_composingSpanClass)) {
                        composingSpanRemoved();
                    }
                } catch(Exception e) {
                    Log.e(edttag, "setSpan exception: "+e);
                }
                mSpans.remove(what);
            } else {
                Log.d(edttag, "removeSpan - no such span; span count="+mSpans.size()+", tag: "+what.toString());
            }
        }

        // "Attach the specified markup object to the range
        // start...end of the text, or move the object to that range
        // if it was already attached elsewhere."
        @Override
        public void setSpan(Object what, int start, int end, int flags)
        {
            Log.d(edttag, "setSpan: "+what.toString()+"; "+start+".."+end+", "+flags+
                    " **********************************************");
            mSpans.put( what, new MySpan( start, end, flags ) );
            try {
                String objname = what.getClass().getName();
                Log.d(edttag, "setSpan: ...class name: "+objname);
                //
                // God help us all! One more hack.
                // When ComposingText span is set, we have to make the text
                // under the span a "composing text".
                // This happens with Swype.
                //
                if (objname.equals(c_composingSpanClass)) {
                    Log.d(edttag, "setSpan: ....composing text span detected!");
                    // Extracting the text below this span
                    CharSequence text_under_span = subSequence(start, end);

                    // Set cursor position at end of the span
                    // (it will be used by deleteSurroundingText()).
                    QtApplicationBase.qt_android_set_selection(end, 0);

                    // Remove text under the span
                    deleteSurroundingText(end-start, 0);

                    // Set the old text under the span as a composing text
                    setComposingText(text_under_span, 1);
                }
            } catch(Exception e) {
                Log.e(edttag, "setSpan exception: "+e);
            }
        }

        // "Return the end of the range of text to which the specified
        // markup object is attached, or -1 if the object is not attached."
        @Override
        public int getSpanEnd(Object tag)
        {
            if( mSpans.containsKey(tag) )
            {
                int e = mSpans.get(tag).end;
                Log.d(edttag, "getSpanEnd: "+e+" - "+tag.toString()+" ("+mSpans.size()+" spans)");
                return e;
            }
            else
            {
                Log.d(edttag, "getSpanEnd - no such span; span count="+mSpans.size()+", tag: "+tag.toString());
                return -1;
            }
        }

        // "Return the flags that were specified when
        // setSpan(Object, int, int, int) was used to attach
        // the specified markup object, or 0 if the specified object
        // has not been attached."
        @Override
        public int getSpanFlags(Object tag)
        {
            if( mSpans.containsKey(tag) )
            {
                int f = mSpans.get(tag).flags;
                Log.d(edttag, "getSpanFlags: "+f+" - "+tag.toString()+" ("+mSpans.size()+" spans)");
                return f;
            }
            else
            {
                Log.d(edttag, "getSpanFlags - no such span; span count="+mSpans.size()+", tag: "+tag.toString());
                return 0;
            }
        }

        // "Return the beginning of the range of text to which
        // the specified markup object is attached, or -1
        // if the object is not attached."
        @Override
        public int getSpanStart(Object tag)
        {
            if( mSpans.containsKey(tag) ){
                int s = mSpans.get(tag).start;
                Log.d(edttag, "getSpanStart: start is "+s+" - "+tag.toString()+" ("+mSpans.size()+" spans)");
                return s;
            }
            else
            {
                Log.d(edttag, "getSpanStart - no such span; span count="+mSpans.size()+", tag: "+tag.toString());
                return -1;
            }
        }

        // "Return an array of the markup objects attached
        // to the specified slice of this CharSequence and whose type
        // is the specified type or a subclass of it."
        // TODO: verify this function
        @Override
        public <T> T[] getSpans(int start, int end, Class<T> type)
        {
            try
            {
                Log.d(edttag, "getSpans: start="+start+", end="+end+", type="+type.toString());
                int count = 0;
                // TODO: rewrite this function with checked type casts
                @SuppressWarnings("unchecked")
                T[] tmpspans = (T[])Array.newInstance(type, mSpans.size());
                for (Map.Entry<Object, MySpan> entry : mSpans.entrySet()) {
                    // Check the slice (TODO: is it correct?)
                    if (entry.getValue().start <= end &&
                        entry.getValue().end >= start)
                    {
                        // Check the type. instanceof doesn't work with generics
                        // so I use this WTF trick to check class inheritance:
                        if (type.isAssignableFrom(entry.getKey().getClass())) {
                            @SuppressWarnings("unchecked")
                            T s = (T)entry.getKey(); // Is it correct?
                            // Assertion check
                            if (s == null) {
                                Log.e(edttag, "getSpans: ...type error!!");
                                return null;
                            }
                            tmpspans[count++] = s;
                            Log.d(edttag, "getSpans: ....span ["+entry.getValue().start+
                                ", "+entry.getValue().end+"] matches ["+
                                start+", "+end+"] and the type");
                        }
                    } else {
                        Log.d(edttag, "getSpans: ....span ["+entry.getValue().start+
                            ", "+entry.getValue().end+"] does not overlap with ["+
                            start+", "+end+"] - skipping");
                    }
                }
                @SuppressWarnings("unchecked")
                T[] spans = (T[])Array.newInstance(type, count);
                for(int i = 0; i < count; i++)
                    spans[i] = tmpspans[i];
                Log.d(edttag, "getSpans: ....got "+count+" spans of "+mSpans.size());
                return spans;
            }
            catch (Exception e)
            {
                Log.e(edttag, "getSpans: exception: "+e);
                return null;
            }
        }

        // "Return the first offset greater than or equal to start
        // where a markup object of class type begins or ends,
        // or limit if there are no starts or ends greater than
        // or equal to start but less than limit."
        // TODO: implement
        @Override
        public int nextSpanTransition(int start, int limit, Class type)
        {
            Log.d(edttag, "nextSpanTransition: "+start+".."+limit);
            return 0;
        }

        // "Returns the character at the specified index,
        // with the first character having index zero."
        @Override
        public char charAt(int index)
        {
            Log.d(edttag, "charAt: "+index);
            return QtApplicationBase.qt_android_editor_text().charAt(index);
        }

        // "Returns the number of characters in this sequence."
        @Override
        public int length()
        {
            Log.d(edttag, "length");
            return qtText().length();
        }

        // "Returns a CharSequence from the start index (inclusive)
        // to the end index (exclusive) of this sequence."
        @Override
        public CharSequence subSequence(int start, int end)
        {
            Log.d(edttag, "subSequence: "+start+".."+end);
            String text = qtText();
            return text.subSequence(start, end);
        }

        // "Returns a string with the same characters in the same order
        // as in this sequence."
        @Override
        public String toString()
        {
            Log.d(edttag, "toString");
            return qtText();
        }
    }

    private Editable mEditable = new MyEditable();

    @Override
    public Editable getEditable()
    {
        Log.d(tag, "getEditable()");
        return mEditable;
    }


} // private class QtWindowInputConnection


