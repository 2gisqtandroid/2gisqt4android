/*
  A prototype for Qt/Android application class which implements the basic
  functionality.

  Author: Alexander A. Saytgalin, <a.saytgalin@2gis.ru>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.core;

import android.app.Activity;
import android.text.ClipboardManager;
import android.util.Log;
import android.content.Context;


public class QtClipboard implements Runnable
{
    private abstract class Action
    {
        private QtClipboard mainClipboard;
        private ClipboardManager clipboardManager;

        public Action(QtClipboard mainClipboard)
        {
            this.mainClipboard = mainClipboard;
        }

        protected QtClipboard getMainClipboard()
        {
            return mainClipboard;
        }

        protected void setClipboardManager(ClipboardManager clipboardManager)
        {
            this.clipboardManager = clipboardManager;
        }

        protected ClipboardManager getClipboardManager()
        {
            return this.clipboardManager;
        }

        public abstract void run();
    }

    private class TextGetter extends Action
    {
        public TextGetter(QtClipboard cb)
        {
            super(cb);
        }

        @Override
        public void run()
        {
            try
            {
                CharSequence text = getClipboardManager().getText();
                if (text != null)
                    getMainClipboard().text = text.toString();
                else
                    getMainClipboard().text = "";
            } catch (Exception e)
            {
                Log.e(QtTAG, "QtClipboard: exception in TextGetter: "+e);
            }
        }
    }

    private class TextSetter extends Action {
        public TextSetter(QtClipboard cb) {
            super(cb);
        }

        @Override
        public void run() {
            try {
               getClipboardManager().setText(getMainClipboard().text);
            } catch (Exception e)
            {
                Log.e(QtTAG, "QtClipboard: exception in TextSetter: "+e);
            }
        }
    }

    private final String QtTAG = "QtClipboard";
    private Activity activity = null;
    private boolean isDone;
    private String text;
    private Action curentAction;

    public QtClipboard(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        try {
            if (null == curentAction)
                return;

            ClipboardManager clipboardManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            synchronized(this) {
                curentAction.setClipboardManager(clipboardManager);
                curentAction.run();

                this.isDone = true;
                this.notifyAll();
            }
        } catch (Exception e) {
            Log.e(QtTAG, "run(): Exception:", e);
        }
    }

    public String getText() {
        try {
            synchronized(this) {
                this.text = "";
                this.isDone = false;
                this.curentAction = new TextGetter(this);
            }

            activity.runOnUiThread(this);

            synchronized(this) {
                if(!this.isDone)
                    this.wait();

                return text;
            }
        } catch (Exception e) {
            Log.e(QtTAG, "getText(): Exception: " + e);
            return "";
        }
    }

    public void setText(final String text) {
        try {
            synchronized(this) {
                this.text = text;
                this.isDone = false;
                this.curentAction = new TextSetter(this);
            }

            activity.runOnUiThread(this);
        } catch (Exception e) {
            Log.e(QtTAG, "setText(\""+text+"\"): Exception: " + e);
        }
    }
}
