/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.lite;

import java.lang.Thread;
import android.util.Log;
import android.graphics.Bitmap;
import android.view.View;
import android.util.DisplayMetrics;

import org.qt.core.QtMutex;
import org.qt.core.QtApplicationBase;
import org.qt.core.QtActivityBase;
import org.qt.core.QtLibraryLoader;

public class QtApplication extends QtApplicationBase
{
    // Execution state
    private enum State {
        Stopped,
        Loading,
        Loaded,
        Running
        /*, Minimized*/
    };

    private static State mState = State.Stopped;
    private static boolean mQtAppRun = false;
    private Thread mMainThread = null;

    // these become invalid on screen updates
    private QtActivity mActivity = null;
    private QtScreen mScreen = null;

    private String tag = "UNSET_TAG";
    private static String tag_s = "UNSED_TAG_S";

    // created just once 
    public QtApplication(QtActivity activity)
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtActivity.mTag, "QtApplication()");

            mActivity = activity;
            setImplementationInstance(this);

            // what libraries will we load?
            mState = State.Stopped;
            //mQtLibrariesToLoad = defaultSharedQtLibraries;
            //mNextLibraryToLoad = 0;

            tag = QtActivity.mTag+" QtApplication";
            tag_s = QtActivity.mTag + " QtApplication (static)";
        }
    }

    @Override
    public QtActivityBase getActivity()
    {
        return mActivity;
    }

    // Change activity. This should be called by QtActivity only, when it is re-created.
    // NOTE: mScreen is changed via screenChanged() in completely another way
    public void setActivity(QtActivity a)
    {
        synchronized( QtMutex.instance ) {
            if( mActivity != a )
                mActivity = a;
        }
    }

    // Main view, also one used to open SIP
    @Override
    public View getView()
    {
        return mScreen; 
    }

    private synchronized void advanceLoading()
    {
        try {

            // Crash test
            // qt_android_java_crash_message("Whoops", "bo-bo", "Die", false);
//            mActivity.showCrashMessageUIT("Whoops", "bo-bo", "Die");
//            return;

            synchronized( QtMutex.instance ) {
                Log.d(QtTAG, "advanceLoading, state=="+mState);
                if( mState == State.Loaded ) {
                    Log.d(QtTAG, "........Already loaded.");
                    return;
                }
                QtActivity a = mActivity;
                if( a==null ) {
                    Log.e(QtTAG, "Activity is NULL!");
                } else {
                    a.startRuntime();
                    // TODO: display loading progress
                    QtLibraryLoader.loadQtApplication(a);

                    // Note: If the state will not be set to Loaded, then manageScreenState()
                    // would call this function to continue loading.
                    mState = State.Loaded; 

                }
                manageScreenState();
            }

        } catch (Exception e) {
            Log.e(QtTAG, "Exception in advanceLoading: "+e);
        }
    }

    private void die()
    {
        if( mActivity != null ) {
            Log.e(QtTAG, "die(): telling activity to exit.");
            mActivity.setHardExit(true);
            mActivity.finish();
        } else {
            Log.e(QtTAG, "die(): no known activity - killing VM.");
            System.exit(1);
        }
    }

    // Called from manageScreenState() to start execution of the application
    private void runMainThread()
    {
        synchronized( QtMutex.instance ) {
            if( !QtLibraryLoader.isApplicationLoaded() || !QtActivityBase.isRuntimeStarted() ) {
                Log.e(QtTAG, "Trying to run main thread when application is not loaded yet!");
                die();
                return;
            }

            // This problem happens occasionally on older/slower Androids when screen
            // is rotated during application shutdown: application starts again during
            // de-initialization and everything is screwed up.
            if (mQtAppRun) {
                Log.e(QtActivity.mTag, "Application has already been started in this VM.");
                die();
                return;
            }

            // Main thread is the main() thread.
            // It is created from Java because this way it will have a proper class loader
            // and won't force user to do preload tricks or whatever.
            mMainThread = new Thread("Qt main()")
            {
                public void run()
                {
                    Log.d(QtActivity.mTag, "mMainThread = new Thread().run()");
                    qt_android_start_qt_application_this_thread(this); // TODO - WTF is jniProxyObject?
                    mainThreadDidStop(); // The previous line exited => Qt application ended work
                }
            };
            mState = State.Running;
            mMainThread.start();
            mQtAppRun = true;
        }
    }

    // Called when native thread is about to stop
    // WARNING - this function is called from Qt thread (not Java GUI thread!)
    public void mainThreadDidStop()
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtActivity.mTag, "mainThreadDidStop");
            mState = State.Stopped;
            mMainThread = null;

            // close activity, if any
            if (mActivity != null) {
                Log.d(QtActivity.mTag, "........finishing activity...");
                mActivity.setHardExit(true); // Always using hard exit in this plugin
                mActivity.finish();
            }else{
                Log.d(QtActivity.mTag, "........activity doesn't exist - stopping VM...");
                System.exit(0);
            }

            // Is this really necessary?
            mActivity = null;
            mScreen = null;
        }
    }

    static float m_xDpi = 120, m_yDpi = 120, m_densityDpi = 120;

    private void sendScreenChanged(int w, int h)
    {
        synchronized( QtMutex.instance ) {
            if( mActivity != null ){
                try{
                    DisplayMetrics metrics = new DisplayMetrics();
                    mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    m_xDpi = metrics.xdpi;
                    m_yDpi = metrics.ydpi;
                    m_densityDpi = metrics.densityDpi;
                }catch( Exception e ){
                    Log.e(tag, "Exception in sendScreenChanged: "+e );
                }
            }
            if( mScreen != null && QtActivityBase.isRuntimeStarted() )
                screenChanged(mScreen.width(), mScreen.height(), m_xDpi, m_yDpi, m_densityDpi);
        }
    }

    private void manageScreenState()
    {
        synchronized( QtMutex.instance ) {
            Log.d(tag, "manageScreenState()");
            try {
                switch(mState)
                {
                case Stopped:
                    Log.d(tag, ".......app is stopped");
                    if (mScreen != null) {
                        mState = State.Loading;
                        advanceLoading(); // Load native libraries
                    }
                    break;
                case Loading:
                    Log.d(tag, ".......app is loading");
                    // do nothing, loading will continue as it is
                    break;
                case Loaded:
                    Log.d(tag, ".......app is loaded");
                    if (mScreen != null) {
                        Log.d(tag, ".......setting screen size: "+mScreen.width()+", "+mScreen.height());
                        sendScreenChanged(mScreen.width(), mScreen.height());
                        Log.d(tag, ".......starting main thread...");
                        runMainThread();
                    }
                    break;
                case Running:
                    Log.d(tag, ".......app is running");
                    if (mScreen != null) {
                        Log.d(tag, ".......setting screen size: "+mScreen.width()+", "+mScreen.height());
                        sendScreenChanged(mScreen.width(), mScreen.height());
                    } else {
                        sendScreenChanged(0, 0); // means screen lost
                    }
                    break;
                default:
                    Log.e(tag, ".......UNKNOWN APP STATE");
                }
            } catch ( Exception e ) {
                Log.e(QtTAG, "Exception in manageScreenState: "+e);
            }
        }
    }

    /*******************************************************************************
     * Surface-drawing-related functions that notify us of a screen state
     ******************************************************************************/
// screen was created or changed (usually only created)
    public void screenChanged(QtScreen screen)
    {
        synchronized( QtMutex.instance ) {
            Log.e(tag, "screenChanged");

            // FIXME debug
            if (mScreen != null && screen != mScreen)
                Log.w(tag, "screenChanged: Got a new screen while posessing the old one. This shouldn't happen.");

            mScreen = screen;
            manageScreenState();
        }
    }

// screen was lost (activity destroyed). 
// this means either user switched to another app, or screen was rotated. elaborate
    public void screenDestroyed(QtScreen screen)
    {
        synchronized( QtMutex.instance ) {
            Log.e(tag, "screenDestroyed");

            // FIXME debug
            if (screen != mScreen)
            {
                Log.w(tag, "Unknown screen was destroyed. This shouldn't happen.");
                return;
            }

            mScreen = null;
            manageScreenState();
        }
    }

    /*******************************************************************************
     * Surface-drawing-related functions that get called from Qt native code
     ******************************************************************************/

    /*******************************************************************************
     * Create Android bitmap for Qt to draw a window on it
     * Note, that as this is a java allocation, you can't get past some memory limits
     * set by manufacturer. So, for a 20Mb limit you can only allocate N full-screen images ...
     * @param w -- width
     * @param h -- height
     * @param depth -- bits per pixel (valid values are 32 and 16) 
     * @return android.graphics.Bitmap;
     */
    public static Bitmap surfaceCreate(int w, int h, int depth)
    {
        // FIXME: remember all surfaces/windows and draw them properly on resize
        Log.d(tag_s, "surfaceCreate("+w+", "+h+", "+depth+")");
        Bitmap.Config fmt;
        switch(depth)
        {
            case 16:
                fmt = Bitmap.Config.RGB_565;
                break;
            case 32:
                fmt = Bitmap.Config.ARGB_8888;
                break;
            default:
                Log.e(tag_s, "Invalid pixel bit depth: "+depth);
                return null;
        }
        return Bitmap.createBitmap(w, h, fmt);
    }

    /*******************************************************************************
     * Blit our surface to physical screen  
     * @param bmp   surface to draw
     * @param px    at screen position X 
     * @param py    at screen position Y
     * @param ax    from surface rect at ax
     * @param ay    from surface rect at ax
     * @param w     with width
     * @param h     with height
     */
    public static void surfaceBlit(final Bitmap bmp, final int px, final int py, final int ax, final int ay, final int w, final int h)
    {
        synchronized( QtMutex.instance ) {
            //Log.d(tag_s, "blitSurface("+px+", "+py+", "+ax+", "+ay+", "+w+", "+h+")");
            QtApplication inst = (QtApplication)getImplementationInstance();
            if( inst != null && bmp !=null )
                if ( inst.mScreen != null)
                    inst.mScreen.blitRect(bmp, px, py, ax, ay, w, h);
        }
    }

    //
    // Native functions
    //

    public native void nativeMain();

    // screen methods
    public static native void screenChanged(int width, int height, float xdpi, float ydpi, float densityDpi);
    public static native void repaint();
}
