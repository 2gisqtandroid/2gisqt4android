/*
  Lite - a QPA windowing plugin for Qt/Android.

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.lite;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.util.Log;

import org.qt.core.QtApplicationBase;
import org.qt.core.QtActivityBase;
import org.qt.core.QtLibraryLoader;
import org.qt.core.QtMutex;

public class QtActivity extends QtActivityBase
{
    public static String mTag = "Qt Java";
    private QtScreen mScreen = null;
    private QtApplication mApplication = null;

    private String tag()
    {
        try
        {
            if (mApplication != null) {
                return mTag+" "+mApplication+" QtActivity";
            } else {
                Log.w(mTag, "mApplication is null in tag()");
            }
        } catch (Exception e) {
            Log.e(mTag, "Exception in tag(): "+e);
        }
        return mTag;
    }

    public QtActivity()
    {
        super();
        setPlugin(getSelectedPluginName());
    }

    public QtActivity(String name)
    {
        super();
        setApplicationName(name);
        setPlugin(getSelectedPluginName());
    }

    static public String getSelectedPluginName()
    {
        int api = QtApplicationBase.qt_android_java_get_api_level();
        if( api < 5 )
            return "QwpLiteApi4";
        if( api < 8 )
            return "QwpLiteApi5";
        return "QwpLiteApi8";
    }

    public void repaintQt()
    {
        try {
            if( QtLibraryLoader.isApplicationLoaded() && isRuntimeStarted() )
                mApplication.repaint();
        } catch( Exception e ) {
            Log.e(QtApplicationBase.QtTAG, "Exception in call to Qt repaint: "+e);
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        synchronized( QtMutex.instance ) {
            mApplication = (QtApplication)QtApplication.getImplementationInstance();
            if (mApplication == null){
                Log.d(tag(), "Creating QtApplication instance...");
                mApplication = new QtApplication(this);
            }

            super.onCreate(savedInstanceState);

            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setBackgroundDrawable(null);
            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

            mScreen = new QtScreen(this, mApplication);
            setContentView(mScreen);

            mApplication.setActivity(this);

            registerForContextMenu(mScreen);

            repaintQt();
        }
    }

    @Override
    protected void onStart()
    {
        Log.d(tag(), "Activity.onStart()");
        super.onStart();
        repaintQt();
    }

    @Override
    protected void onRestart()
    {
        Log.d(tag(), "Activity.onRestart()");
        super.onRestart();
        repaintQt();
    }

    @Override
    protected void onResume()
    {
        Log.d(tag(), "Activity.onResume()");
        super.onResume();
        // FIXME errorcheck
        //mApplication.resume();
        repaintQt();
    }

    @Override
    protected void onPause()
    {
        Log.d(tag(), "Activity.onPause()");
        super.onPause();
        //mApplication.pause();
    }

    @Override
    protected void onStop()
    {
        Log.d(tag(), "Activity.onStop()");
        super.onStop();
        //mApplication.minimize();
    }

    @Override
    protected void onDestroy()
    {
        Log.d(tag(), "Activity.onDestroy()");
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        Log.d(tag(), "onSaveInstanceState "+outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        Log.d(tag(), "onRestoreInstanceState "+savedInstanceState);
        repaintQt();
    }
}
