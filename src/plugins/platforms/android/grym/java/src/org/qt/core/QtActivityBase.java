/*
  A prototype for Qt/Android Activity which implements basic functionality.
  Author: Sergey A. Galin, <sergey.galin@gmail.com>
  Contributors: Ivan 'w23' Avdeev, <marflon@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

package org.qt.core;

import java.util.Set;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.Locale;
import java.io.File;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.method.MetaKeyKeyListener;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.KeyEvent;
import android.view.KeyCharacterMap;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.InputType;
import org.qt.core.QtApplicationBase;
import org.qt.core.QtKeepaliveService;
import org.qt.core.QtLibraryLoader;

public class QtActivityBase extends Activity
{
    // Exit VM on activity finish?
    // Most Qt apps need this, so let it be ON by default.
    // In fact, Qt by itself has many static stuff which causes crashes
    // after "soft restart". Application may have this flag disabled
    // only if it has been carefully tested to restart successfully
    // without it.
    private static boolean m_hardExit = true;
    private static boolean m_runtimeStarted = false;
    private static boolean m_crashState = false;
    private boolean m_onCreateOrLater = false;
    private boolean m_keepaliveService = false;
    boolean mIsPaused = false;

    // Keyboard support
    private long mMetaState = 0;
    private int mLastChar = 0;
    // Used for Android 1.6 bug workaround: when context menu is closed via Back key,
    // the key up is sent to application and then gets processed as Escape by Qt app.
    private boolean mSkipKeyUp = false;

    private static QtApplicationBase m_appBase = null;

    // Is native application shutting down?
    private static boolean m_shuttingDown = false;

    public QtActivityBase()
    {
        super();
        m_shuttingDown = false;
        mIsPaused = false;
    }

    public QtActivityBase(boolean keepaliveService)
    {
        super();
        m_shuttingDown = false;
        m_keepaliveService = keepaliveService;
        mIsPaused = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        synchronized( QtMutex.instance ) {
            mIsPaused = false;

            if( m_shuttingDown || m_crashState ) {
                Log.e(QtApplicationBase.QtTAG, "ERROR: QtActivityBase.onCreate when application is shutting down!");
                setHardExit(true);
                finish();
            }

            m_appBase = QtApplicationBase.getImplementationInstance();
            if( m_appBase == null )
                Log.e(QtApplicationBase.QtTAG, "WARNING: QtApplicationBase instance does not exists yet in QtActivityBase.onCreate()!");

            m_onCreateOrLater = true;
            super.onCreate(savedInstanceState);
            // takeKeyEvents(true); - this looks sane, but crashes, and stupid docs...
            updateKeepaliveService();
            QtInputCommon.resetInput();
            if( QtLibraryLoader.isQAndroidCoreLoaded() )
                QtApplicationBase.qt_android_send_application_activated();
        }
    }

    @Override
    public Object onRetainNonConfigurationInstance()
    {
        Log.d(QtApplicationBase.QtTAG, "QtActivityBase.onRetainNonConfigurationInstance()");
        m_shuttingDown = false;
        super.onRetainNonConfigurationInstance();
        return true;
    }

    @Override
    public void finish()
    {
        Log.d(QtApplicationBase.QtTAG, "QtAcivityBase.finish()");
        m_shuttingDown = true;
        try {
            // Stop Qt application, it it's still running
            if( QtLibraryLoader.isApplicationLoaded() ){
                QtApplicationBase.qt_android_exit_qtmain();
            }
            // Stop keepalive service, if it's running
            setKeepaliveService(false);
        } catch( Exception e ) {
            Log.e(QtApplicationBase.QtTAG, "Exception in QtActivityBase.finish: "+e);
        }
        super.finish();
    }

    public static boolean isShuttingDown()
    {
        return m_shuttingDown;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (isShuttingDown() && isHardExit()){
            Log.d(QtApplicationBase.QtTAG, "QtActivityBase.onDestroy(): Quitting VM...");
            System.exit(0);
        }
    }

    // Some backward compatibility / convenience wrappers
    public static void setApplicationName(String app){ QtLibraryLoader.setApplicationName(app); }
    public static void addLibrary(String lib){ QtLibraryLoader.addLibrary(lib); }
    public static void setPlugin(String plugin){ QtLibraryLoader.setPlugin(plugin); }

    // Returns the same as Environment.getExternalFilesDir(), which is not available on API levels < 8.
    public File getExternalFilesDir()
    {
        return new File(Environment.getExternalStorageDirectory().getPath()+
                "/Android/data/"+getApplication().getPackageName()+"/files");
    }

    // Pass Android system directories to Qt application
    private void setDirectories()
    {
        if( QtLibraryLoader.isQAndroidCoreLoaded() ){
            QtApplicationBase.qt_android_set_internal_files_path(getApplication().getFilesDir().getPath());
            QtApplicationBase.qt_android_set_external_files_path(this.getExternalFilesDir().getPath());
            QtApplicationBase.qt_android_set_external_storage_path(Environment.getExternalStorageDirectory().getPath());
            QtApplicationBase.qt_android_set_cache_path(Environment.getDownloadCacheDirectory().getPath());
            QtApplicationBase.qt_android_set_apk_lib_path(QtLibraryLoader.apkLibraryDir(getApplication()));
        }else{
            Log.e(QtApplicationBase.QtTAG, "ERROR: setDirectories() called while QAndroidCore is not loaded!");
        }
    }

    // Initialize display metrics in QAndroidCore.
    // Please note that QDesktopWidget must be updated elsewhere at the moment!
    private void setScreenMetrics()
    {
        if( QtLibraryLoader.isQAndroidCoreLoaded() ){
            try {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int w = metrics.widthPixels;
                int h = metrics.heightPixels;
                float xd = metrics.xdpi;
                float yd = metrics.ydpi;
                float dd = metrics.densityDpi;

                Log.d(QtApplicationBase.QtTAG, "Android display metrics: "+w+"x"+h+
                    ", "+xd+"x"+yd+" DPI (logical="+dd+")");

                QtApplicationBase.qt_android_set_screen_metrics(w, h, xd, yd, dd);
            } catch ( Exception e ) {
                Log.e(QtApplicationBase.QtTAG, "Exception in setScreenMetrics(): "+e);
            }
        }else{
            Log.e(QtApplicationBase.QtTAG, "ERROR: setScreenMetrics() called while QAndroidCore is not loaded!");
        }
    }

    private static void setLocale()
    {
        if( QtLibraryLoader.isQAndroidCoreLoaded() ){
            try {
                Locale lc = Locale.getDefault();
                // Forming a Linux-style locale string, like: ru_RU.UTF-8
                String localestr = lc.toString()+".UTF-8";
                Log.d(QtApplicationBase.QtTAG, "setLocale: "+localestr);
                QtApplicationBase.qt_android_set_locale(localestr);
            } catch ( Exception e ) {
                Log.e(QtApplicationBase.QtTAG, "Exception in setLocale(): "+e);
            }
        }else{
            Log.e(QtApplicationBase.QtTAG, "ERROR: setLocale() called while QAndroidCore is not loaded!");
        }
    }

    // Initialize all variables in QtAndroiCore
    private void setAllAndroidCoreVars()
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "setAllAndroidCoreVars...");
            if( !QtLibraryLoader.isQAndroidCoreLoaded() ){
                Log.e(QtApplicationBase.QtTAG, "ERROR: setAllAndroidCoreVars: QAndroidCore is not loaded!");
            }
            setDirectories();
            setScreenMetrics();
            setLocale();
            QtApplicationBase.qt_android_send_application_activated();
        }
    }

    // Override this function in your Activity to do any custom initializations
    // needed after run-time is loaded and set up.
    protected void onRuntimeStarted()
    {
    }

    // Load & initialize run-time
    public void startRuntime()
    {
        synchronized( QtMutex.instance ) {
            Log.d(QtApplicationBase.QtTAG, "startRuntime...");
            if( !m_runtimeStarted ) {
                QtLibraryLoader.loadAllRuntime(this);
                setAllAndroidCoreVars();
                m_runtimeStarted = true;
                onRuntimeStarted();
            } else {
                Log.i(QtApplicationBase.QtTAG, "WARNING: startRuntime called second time!");
            }
        }

        QtApplicationBase.doTest(); // SGEXP
    }

    public static boolean isRuntimeStarted()
    {
        return m_runtimeStarted;
    }

    //
    // Shut down VM on clean exit?
    // By default, we attempt to shut down VM only if application crashed.
    //

    public static boolean isHardExit()
    {
        return m_hardExit;
    }

    public static void setHardExit(boolean hard)
    {
        m_hardExit = hard;
    }

    //
    // Keepalive service
    // Don't forget to add the service to your AndroidMainfest.xml file:
    // <service android:enabled="true" android:name="org.qt.common.QtKeepaliveService" />
    //
    public void setKeepaliveService(boolean keepaliveService)
    {
        if( m_keepaliveService != keepaliveService || keepaliveService ) {
            m_keepaliveService = keepaliveService;
            if( m_onCreateOrLater ){ // (...or onCreate will call updateKeepaliveService() after setting the flag.)
                try {
                    if( m_keepaliveService ){
                        Log.d(QtApplicationBase.QtTAG, "setKeepaliveService: starting the service...");
                        Intent svc = new Intent(this, QtKeepaliveService.class);
                        startService(svc);
                    }else{
                        Log.d(QtApplicationBase.QtTAG, "setKeepaliveService: stopping the service...");
                        Intent svc = new Intent(this, QtKeepaliveService.class);
                        stopService(svc);
                    }
                }catch( Exception e ){
                    Log.e(QtApplicationBase.QtTAG, "setKeepaliveService: exception: "+e);
                }
            }else{
                Log.d(QtApplicationBase.QtTAG, "setKeepaliveService: not changing service state yet...");
            }
        }
    }

    public void updateKeepaliveService()
    {
        setKeepaliveService(m_keepaliveService);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Log.d(QtApplicationBase.QtTAG, "[QAKB] Activity - DOWN keycode: "+keyCode+
            " unicode char: "+event.getUnicodeChar()+
            " repeat count: "+event.getRepeatCount());

        mSkipKeyUp = false;

        mMetaState = MetaKeyKeyListener.handleKeyDown(mMetaState, keyCode, event);
        int unicodeChar = event.getUnicodeChar(MetaKeyKeyListener.getMetaState(mMetaState));
        int lastc = unicodeChar;
        mMetaState = MetaKeyKeyListener.adjustMetaAfterKeypress(mMetaState);

        // Check for accented character input
        if ((unicodeChar & KeyCharacterMap.COMBINING_ACCENT) != 0) // Accent character!
            unicodeChar = KeyEvent.getDeadChar(mLastChar, unicodeChar & KeyCharacterMap.COMBINING_ACCENT_MASK);
        mLastChar = lastc;

        if( QtLibraryLoader.isApplicationLoaded() )
            QtApplicationBase.qt_android_key_down(keyCode, unicodeChar, event.getMetaState(), event.getRepeatCount());

        // Note: there's also a function Acitivity.onBackPressed(), which may be useful later
        // We are not processing BACK here; Qt application should check for Qt.Key_Esc and
        // go back in frames or exit or minimize itself via an exported function.
        // if ( keyCode == KeyEvent.KEYCODE_BACK )
        //    return super.onKeyDown(keyCode, event);

        return true;
    }

    // This function does nothing except that it suppresses a rare IllegalStateException
    // with a call stack which doesn't include a single sourcecode line of the Qt plugin.
    // Unfortunately, it has to be commented out for API level 4.
    /*
    @Override - API Level 5.
    public void onBackPressed ()
    {
        try
        {
            // This may throw:
            // java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
            // Google Play doesn't give any clues which devices and OS versions do that.
            super.onBackPressed();
        }
        catch (IllegalStateException e)
        {
            Log.e(QtApplicationBase.QtTAG, "onBackPressed: IllegalStateException:" + e);
        }
        catch (Exception e)
        {
            Log.e(QtApplicationBase.QtTAG, "onBackPressed: exception:" + e);
        }
    }
    */

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        Log.d(QtApplicationBase.QtTAG, "[QAKB] Activity - UP keycode: "+keyCode+
            " unicode char: "+event.getUnicodeChar()+
            " repeat count: "+event.getRepeatCount());
        mMetaState = MetaKeyKeyListener.handleKeyUp(mMetaState, keyCode, event);
        if (mSkipKeyUp) {
            Log.d(QtApplicationBase.QtTAG, "[QAKB] Activity - skipping key up for: "+keyCode);
            mSkipKeyUp = false;
            return true;
        }
        if( QtLibraryLoader.isApplicationLoaded() )
             QtApplicationBase.qt_android_key_up(keyCode, event.getUnicodeChar(), event.getMetaState(), event.getRepeatCount());
        // We are not processing BACK here; Qt application should check for Qt.Key_Esc and
        // go back in frames or minimize itself via an exported function.
        // if (keyCode == KeyEvent.KEYCODE_BACK)
        //    return super.onKeyUp(keyCode, event);
        return true;
    }

    // NOTE: This is necessary to support non-latin characters.
    // Although we use Input Connection method for soft keyboard, this hack
    // is still necessary for hardware keyboards, or when using simplified SIP mode.
    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        Log.d(QtApplicationBase.QtTAG, "Activity - dispatchKeyEvent: action="+event.getAction()+
            ", flags="+event.getFlags()+
            ", keyCode="+event.getKeyCode()+", characters=\""+event.getCharacters()+"\"");

        // TODO: Can the event contain a string, not a single character?
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE &&
            event.getCharacters() != null &&
            event.getCharacters().length() == 1 &&
            event.getKeyCode() == 0 &&
            QtLibraryLoader.isApplicationLoaded() )
        {
            Log.d(QtApplicationBase.QtTAG, "dispatchKeyEvent at MULTIPLE with one character: "+event.getCharacters());
            if( QtLibraryLoader.isApplicationLoaded() )
                QtApplicationBase.qt_android_key(0, event.getCharacters().charAt(0), event.getMetaState());
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onPause()
    {
        synchronized( QtMutex.instance ) {
            if( QtLibraryLoader.isApplicationLoaded() ){
                QtApplicationBase.qt_android_send_application_deactivated();
                QtApplicationBase.qt_android_java_set_sip(false);
            }
            mIsPaused = true;
        }
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        synchronized( QtMutex.instance ) {
            mSkipKeyUp = false;
            mIsPaused = false;
            if( QtLibraryLoader.isApplicationLoaded() ){
                setScreenMetrics(); // Resuming often happens when screen is rotated
                QtInputCommon.resetInput(); // Resetting touches
                QtApplicationBase.qt_android_send_application_activated();
            }
        }
        super.onResume();
    }

    public boolean isPaused()
    {
        return mIsPaused;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        /*

http://developer.android.com/guide/topics/manifest/activity-element.html#config
http://developer.android.com/reference/android/app/Activity.html#onConfigurationChanged(android.content.res.Configuration)

A flag indicating whether the hard keyboard has been hidden. This will be set on a device with a mechanism to hide the keyboard from the user, when that mechanism is closed. One of: HARDKEYBOARDHIDDEN_NO, HARDKEYBOARDHIDDEN_YES.
public int keyboard
Since: API Level 1

The kind of keyboard attached to the device. One of: KEYBOARD_NOKEYS, KEYBOARD_QWERTY, KEYBOARD_12KEY.
public int keyboardHidden
Since: API Level 1

A flag indicating whether any keyboard is available. Unlike hardKeyboardHidden, this also takes into account a soft keyboard, so if the hard keyboard is hidden but there is soft keyboard available, it will be set to NO. Value is one of: KEYBOARDHIDDEN_NO, KEYBOARDHIDDEN_YES.

public int orientation
Since: API Level 1

Overall orientation of the screen. May be one of ORIENTATION_LANDSCAPE, ORIENTATION_PORTRAIT, or ORIENTATION_SQUARE.

public int touchscreen
Since: API Level 1

The kind of touch screen attached to the device. One of: TOUCHSCREEN_NOTOUCH, TOUCHSCREEN_STYLUS, TOUCHSCREEN_FINGER.
     */
/*
        if( QtLibraryLoader.isQAndroidCoreLoaded() ){
            try {
                setLocale();
            } catch ( Exception e ) {
                Log.e(QtApplicationBase.QtTAG, "Exception in onConfigurationChanged(): "+e);
            }
        }
*/
        super.onConfigurationChanged(newConfig);
    }

    // Show crash message and exit.
    // This function must be called from Java UI thread.
    // Looper.loop() can be called afterwards to pause execution until
    // user respond to the error message.
    public void showCrashMessageUIT(final String title, final String explanation, final String buttonText)
    {
        m_crashState = true;
        new AlertDialog.Builder(QtActivityBase.this)
            .setTitle(title)
            .setMessage(explanation)
            .setPositiveButton(buttonText,
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        finish();
                    }
                })
            .setCancelable(false)
            .show();
    }

    // Warning: "halt" should not be set to true when calling from Java UI thread!
    public void showCrashMessage(final String title, final String explanation, final String buttonText, final boolean halt)
    {
        m_crashState = true;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                QtActivityBase.this.showCrashMessageUIT(title, explanation, buttonText);
            }
        });
        if( halt ) {
            // This is a bit ugly, but I can't find a better solution right now.
            // Assuming that we're not in Java UI thread, so we can safely
            // pause it, waiting for finish.
            Log.i(QtApplicationBase.QtTAG, "showCrashMessage: halting thread");
            try{
                // Totally wait for 10 minutes max, that's 60 * 1000 * 10 = 600000 ms
                for( int i = 0; i < 10000; i++ ) {
                    if( m_shuttingDown ) // Set by finish()
                        break;
                    Thread.sleep(60);
                }
            }catch( Exception ex ){};
            Log.i(QtApplicationBase.QtTAG, "showCrashMessage: waiting timed out or exit button clicked by the user.");
            // If user didn't click exit button yet, just finish it now
            if( !m_shuttingDown )
                finish();
        } // if( halt )
    }

    // Called by system when it knows that user wants to create context menu.
    // It is told that from QtApplicationBase.qt_android_java_context_menu_show() ;)
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
    {
        Log.d(QtApplicationBase.QtTAG, "onCreateContextMenu("+v.toString()+", "+menuInfo);
        // Let it be done in QtApplicationBase!
        QtApplicationBase.qt_android_context_menu_create(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        Log.d(QtApplicationBase.QtTAG, "onContextItemSelected("+item.toString()+")");
        return QtApplicationBase.qt_android_java_context_menu_call(item.getItemId());
    }

    @Override
    public void onContextMenuClosed(Menu menu)
    {
        Log.d(QtApplicationBase.QtTAG, "onContextMenuClosed()");
        QtApplicationBase.qt_android_java_context_menu_closed();
        // Android 1.6 bug workaround
        if (QtApplicationBase.qt_android_java_get_api_level() <= 4)
            mSkipKeyUp = true;
    }
}

