/*
  A keepalive service which does nothing but reduces chances that Qt
  application will be unloaded while being in background.

  Author: Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

// Don't forget to add the service to your AndroidMainfest.xml file:
// <service android:enabled="true" android:name="org.qt.core.QtKeepaliveService" />

package org.qt.core;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import org.qt.core.QtApplicationBase;

public class QtKeepaliveService extends Service
{
    // Keeping QtApplicationBase from unloading by holding a reference to it;
    // other classes should be held by the QtApplicationBase or its descendant.
    private Object m_appClass = null;
    private QtApplicationBase m_app = null;

    public QtKeepaliveService()
    {
        super();
        Log.d( QtApplicationBase.QtTAG, "QtKeepaliveService() ##############################################################" );
        m_appClass = QtApplicationBase.class;
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        Log.d( QtApplicationBase.QtTAG, "onBind() ##############################################################" );
        return null;
    }

    @Override
    public void onCreate()
    {
        Log.d( QtApplicationBase.QtTAG, "onCreate() ##############################################################" );
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d( QtApplicationBase.QtTAG, "onStartCommand() ##############################################################" );
        m_app = QtApplicationBase.getImplementationInstance();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        Log.d( QtApplicationBase.QtTAG, "onDestroy() ##############################################################" );
        super.onDestroy();
    }

    @Override
    public void onStart(Intent intent, int startid)
    {
        Log.d( QtApplicationBase.QtTAG, "onStart() ##############################################################" );        
    }
}
