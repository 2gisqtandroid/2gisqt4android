TEMPLATE = subdirs

SUBDIRS += core lite qandroidentrypoint

INSTALLS += android_java_grym
android_java_grym.path = $$[QT_INSTALL_PREFIX]/src/android/java_grym
android_java_grym.files = $$QT_SOURCE_TREE/src/plugins/platforms/android/grym/java/*

OTHER_FILES += \
    README.txt \
    java/AndroidManifest.xml \
    java/build.properties \
    java/res/values/strings.xml \
    java/src/org/qt/lite/QtActivity.java \
    java/src/org/qt/lite/QtApplication.java \
    java/src/org/qt/lite/QtScreen.java \
    java/src/org/qt/util/LibraryLoadListener.java \
    java/src/org/qt/util/StreamUtil.java \
    java/src/org/qt/util/AssetUtil.java \
    java/src/org/qt/core/QtMutex.java \
    java/src/org/qt/core/QtInputCommonMultitouch.java \
    java/src/org/qt/core/QtLibraryLoader.java \
    java/src/org/qt/core/QtWindowInputConnection.java \
    java/src/org/qt/core/QtKeepaliveService.java \
    java/src/org/qt/core/QtInputCommon.java \
    java/src/org/qt/core/QtApplicationBase.java \
    java/src/org/qt/core/QtActivityBase.java

