/*
  Qt/Android Integration Core Library

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef ANDROIDINPUTCONTEXT_H
#define ANDROIDINPUTCONTEXT_H

#include <QInputContext>
#include <QMutex>
#include <QPointer>
#include <QSemaphore>
#include "qandroidcoreexports.h"

class Q_ANDROID_EXPORT QAndroidInputContext : public QInputContext
{
    Q_OBJECT
public:
    explicit QAndroidInputContext(QObject *parent = 0);
    virtual ~QAndroidInputContext();

    // This does not create instace, only returns pointer
    // to existing one or NULL.
    static QAndroidInputContext* instance() { return s_instance.data(); }

    virtual QString identifierName();
    virtual bool isComposing() const;
    virtual QString language();
    virtual void mouseHandler( int x, QMouseEvent * event );
    virtual void reset();
    virtual void setFocusWidget( QWidget * widget );
    virtual void widgetDestroyed ( QWidget * widget );
    virtual void update();
    virtual bool filterEvent ( const QEvent * event );

    // Get text being edited, to display it in full-screen editor.
    // This is currently implemented through a completely evil hack.
    // Also, QLineEdit and QTextEdit descendants only are supported.
    QString editorText();
    int cursorPosition();
    QString composingText() const;

    // Get current selection; any parameter can be NULL.
    // Returns false if text editing control is not selected, or
    // doesn't support selection (yet).
    bool getSelection(int* outStart, int* outLength, QString* outText, bool* multilineEditor);

    // Find true editor widget
    QWidget* editorWidget();

public slots:
    void setSelection(int start, int length);
    void deleteSurroundingText(int leftLength, int rightLength);
    void inputKeyEvent(int keyCode, int metaState);
    void inputComposingText(const QString& text, int newCursorPosition);
    void inputCommitText(const QString& text, int newCursorPosition, bool open_semaphore);
    void commitCurrentText(bool open_semaphore); // Apply current composing text
    void resetCurrentText(bool open_semaphore); // Reset current composing text

private:
    static QPointer<QAndroidInputContext> s_instance;

    mutable QMutex mComposingTextMutex;
    QString mComposingText;

    // Functions for accessing text being edited.
    bool isAppRunning();
    QWidget* graphicsViewEditorWidget(QWidget* graphview); // Helper for editorWidget()
    bool isEditorWidget(QWidget* widget);
};

Q_DECL_EXPORT bool qt_android_reset_input_context();
Q_DECL_EXPORT void qt_android_commit_and_hide_software_keyboard();
Q_DECL_EXPORT void qt_android_keyboard_touched();

#endif // ANDROIDINPUTCONTEXT_H
