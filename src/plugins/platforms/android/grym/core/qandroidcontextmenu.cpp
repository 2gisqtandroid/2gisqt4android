/*
  Qt/Android Integration Core Library

  Author:
    Alexander A. Saytgalin <s.saytgalin@2gis.ru>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2012, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtCore/QDebug>
#include <QMetaObject>
#include "qandroidcontextmenu.h"
#include "qandroidcorejava.h"
#include "qandroidutils.h"

namespace QtAndroid
{

QAndroidContextMenu QAndroidContextMenu::instance;


QAndroidContextMenu::QAndroidContextMenu()
{
}

QAndroidContextMenu::~QAndroidContextMenu()
{
}

void QAndroidContextMenu::SetCaption(const QString & new_caption)
{
    caption = new_caption;
}

QAndroidContextMenu::ItemId QAndroidContextMenu::AddItem(const QString &caption, QObject *obj, const char *on_click)
{
    menu.append(Item(caption, obj, on_click));
    return menu.size() - 1;
}

void QAndroidContextMenu::SetOnCloseHandler(QObject *obj, const char *on_close)
{
    onclose_obj = obj;
    onclose_slot = on_close;
}

// Convenience wrapper for Clicked(Item&)
bool QAndroidContextMenu::Clicked(const ItemId &id)
{
    qDebug()<<"Context menu: item"<<static_cast<long>(id)<<"clicked";
    if (menu.size() <= id || id < 0)
    {
        qWarning()<<"Context menu: invalid item id. Menu size is:"<<menu.size();
        return false;
    }
    return Clicked(menu[id]);
}

bool QAndroidContextMenu::Clicked(Item &item)
{
    qDebug()<<"QAndroidContextMenu::Clicked";
    if (item.obj.isNull() || item.on_click.isEmpty())
    {
        qWarning()<<"Context menu: menu item's object or slot is not set.";
        return false;
    }
    bool ret = QMetaObject::invokeMethod(item.obj.data(), item.on_click.data(), Qt::QueuedConnection);
    return ret;
}

bool QAndroidContextMenu::Closed()
{
    qDebug()<<"QAndroidContextMenu::Closed()";
    if (onclose_obj.isNull() || onclose_slot.isEmpty())
    {
        qDebug()<<"Context menu: on close object or slot is not set.";
        return false;
    }
    bool ret = QMetaObject::invokeMethod(onclose_obj.data(), onclose_slot.data(), Qt::QueuedConnection);
    return ret;
}

void QAndroidContextMenu::Clear()
{
    caption = "";
    menu.clear();
    onclose_obj = 0;
    onclose_slot.clear();
}

// static
bool QAndroidContextMenu::ContextMenuClicked(long item_id)
{
    qt_android_java_context_menu_clear();
    QtAndroid::QAndroidContextMenu *context_menu = QtAndroid::QAndroidContextMenu::GetInstance();
    bool ret = context_menu->Clicked(static_cast<QtAndroid::QAndroidContextMenu::ItemId>(item_id));
    return ret;
}

// static
bool QAndroidContextMenu::ContextMenuClosed()
{
    qt_android_java_context_menu_clear();
    QtAndroid::QAndroidContextMenu *context_menu = QtAndroid::QAndroidContextMenu::GetInstance();
    bool ret = context_menu->Closed();
    return ret;
}

} // namespace QtAndroid


Q_ANDROID_EXPORT void qt_android_context_menu_create(const QString &caption)
{
    qt_android_context_menu_create(caption, 0, 0);
}

Q_ANDROID_EXPORT void qt_android_context_menu_create(const QString &caption, QObject *obj, const char *on_cancel)
{
    QtAndroid::QAndroidContextMenu *context_menu = QtAndroid::QAndroidContextMenu::GetInstance();
    context_menu->Clear();
    context_menu->SetCaption(caption);
    if (obj && on_cancel)
        context_menu->SetOnCloseHandler(obj, on_cancel);
    qt_android_java_context_menu_create(caption);
}

Q_ANDROID_EXPORT long qt_android_context_menu_add(const QString &caption, QObject *obj, const char *on_click)
{
    QtAndroid::QAndroidContextMenu *context_menu = QtAndroid::QAndroidContextMenu::GetInstance();
    long item_id = static_cast<long>(context_menu->AddItem(caption, obj, on_click));
    qt_android_java_context_menu_add(caption, item_id);
    return item_id;
}

Q_ANDROID_EXPORT void qt_android_context_menu_exec()
{
    qt_android_java_context_menu_show(true);
}

Q_ANDROID_EXPORT void qt_android_context_menu_cancel()
{
    qt_android_java_context_menu_show(false);
    qt_android_java_context_menu_clear();
    QtAndroid::QAndroidContextMenu *context_menu = QtAndroid::QAndroidContextMenu::GetInstance();
    context_menu->Clear();
}

