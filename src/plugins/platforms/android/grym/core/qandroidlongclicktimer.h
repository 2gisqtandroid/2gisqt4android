/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QANDROIDLONGCLICKTIMER_H
#define QANDROIDLONGCLICKTIMER_H

#include <QtCore/QTimer>
#include "qandroidcoreexports.h"

#if defined(QT_NO_CONTEXTMENU) || defined(QT_NO_MENU)
    #define QT_ANDROID_NO_LONGCLICK
#endif

namespace QtAndroid {

void longClickStarted(int x, int y);
void longClickMoved(int x, int y);
void longClickReleased(int x, int y);
void longClickReleased();

// This class is here (in .h file) only to be processed by moc
class QAndroidLongClickTimer: protected QTimer
{
    Q_OBJECT
private:
    QAndroidLongClickTimer();
    QAndroidLongClickTimer(const QAndroidLongClickTimer&); // noncopyable ;)
    void clickStarted(int x, int y);
    void clickMoved(int x, int y);
    void clickReleased(int x, int y);
    void clickReleased();

    friend void ::qt_android_set_long_click_enabled(bool enable);
    friend void QtAndroid::longClickStarted(int x, int y);
    friend void QtAndroid::longClickMoved(int x, int y);
    friend void QtAndroid::longClickReleased(int x, int y);
    friend void QtAndroid::longClickReleased();
private slots:
    void longClick();
private:
    int m_x, m_y;
};

} // namespace QtAndroid

#endif
