/*
  Qt/Android Integration Core Library

  Author: Alexander A. Saytgalin <a.saytgalin@2gis.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "qandroidstorages.h"
#include <android/log.h>
#include <qdebug.h>
#include <QStringList>

namespace QtAndroid {

bool MountEntry::IsExternalStorage() const
{
    const char * const dev_prefix = "/dev/";
    return 0 == dev.indexOf(dev_prefix);
}

} // namespace QtAndroid

QTextStream& operator>> (QTextStream &stream, QtAndroid::MountEntry &mount_entry)
{
    stream >> mount_entry.dev;

    QString mount_point;
    stream >> mount_point;
    mount_entry.MountPoint(mount_point);

    stream >> mount_entry.fs;
    stream >> mount_entry.options;
    stream >> mount_entry.dump;
    stream >> mount_entry.pass;

    return stream;
}

QTextStream& operator>> (QTextStream &stream, QtAndroid::VoldEntry &vold_entry)
{
    const char * const media_type_name = "media_type";
    const char * const mount_point_name = "mount_point";
    const char comment = '#';
    const char * const open_br = "{";
    const char * const close_br = "}";

    // Read caption
    {
        QString line;
        // Skip empty lines and comments.
        do {
            line = stream.readLine().trimmed();
        } while(!stream.atEnd() && (line.isEmpty() || comment == line[0]));

        if (stream.atEnd() || QTextStream::Ok != stream.status())
        {
            return stream;
        }

        QTextStream stream_line(&line);
        stream_line >> vold_entry.caption;
        QString open;
        stream_line >> open;
        if (open_br != open.trimmed())
        {
            stream.setStatus(QTextStream::ReadCorruptData);
            return stream;
        }
    }

    // Read data
    for(;;)
    {
        QString line;
        // Skip empty lines and comments.
        do {
            line = stream.readLine().trimmed();
        } while(!stream.atEnd() && (line.isEmpty() || comment == line[0]));

        if (QTextStream::Ok != stream.status())
        {
            return stream;
        }

        if (close_br == line)
        {
            return stream;
        }

        QTextStream stream_line(&line);
        QString var;
        QString value;

        stream_line >> var >> value;

        var = var.trimmed();
        value = value.trimmed();

        if (media_type_name == var)
        {
            vold_entry.media_type = value;
        }
        else if (mount_point_name == var)
        {
            vold_entry.MountPoint(value);
        }
    }

    return stream;
}


// Function gives a list of external storages.
Q_ANDROID_EXPORT const QStringList& qt_android_get_external_storages()
{
    static bool need_update(true);
    static QStringList ret;

    // The file'/etc/vold.conf' ('/etc/vold.fstab') doesn't change. Need to read once.
    if (!need_update)
        return ret;

    // Will try to read mount tables from one of these files:
    const char * const vold_conf_file_name = "/etc/vold.conf";
    const char * const vold_fstab_file_name = "/etc/vold.fstab";
    const char * const mount_file_name = "/proc/mounts";

    QtAndroid::DevEntryLoader loader;
    // Load devices from the file '/etc/vold.conf'. File format is 'VoldEntry'.
    QSet<QString> devs = loader.Load<QtAndroid::VoldEntry>(vold_conf_file_name);

    if (devs.isEmpty())
    {
        // Some devices don't contain the file '/etc/vold.conf' (it containts the file '/etc/vold.fstab'?).
        devs = loader.Load<QtAndroid::VoldEntry>(vold_fstab_file_name);
    }

    if (!devs.isEmpty())
    {
        // The file'/etc/vold.conf' ('/etc/vold.fstab') doesn't change.
        need_update = false;
    }
    else
    {
        // Load devices from the universal file '/proc/mounts'. File format is 'MountEntry'.
        devs = loader.Load<QtAndroid::MountEntry>(mount_file_name);
    }

    // Add the primary storage directory.
    const QString &external_storage(qt_android_get_external_storage_path());
    devs << external_storage;

    // Blacklist
    const char * const blacklist[] = {
        "/", "/acct", "/cache", "/config", "/data", "/dev", "/etc", "/init", "/mnt",
        "/pds", "/proc", "/root", "/sbin", "/sys", "/system", "/vendor", 0
    };
    for (const char * const * p = blacklist; *p; ++p)
    {
        devs.remove(*p);
    }

    ret = devs.toList();

    return ret;
}
