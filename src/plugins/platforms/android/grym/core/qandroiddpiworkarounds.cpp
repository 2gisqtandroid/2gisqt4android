/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtCore/qdebug.h>
#include "qandroidcorejava.h"
#include "qandroiddpiworkarounds.h"

namespace QtAndroid {

static const qreal cInsaneLowDpi = 10.0f, cInsaneHighDpi = 10000.0f;

// Most of devices with wrong DPI are Chinese tablets and have 100-140 dpi.
const qreal cDefaultDpi = 120.0f;

Q_ANDROID_EXPORT qreal fixDpi(qreal dpi)
{
    if( dpi < cInsaneLowDpi || dpi > cInsaneHighDpi ){
        qDebug()<<QString("Received ridiculous DPI setting: %1, changing to: %2 dpi")
                  .arg(dpi)
                  .arg(cDefaultDpi);
        return cDefaultDpi;
    }
    return dpi;
}

// Choose which DPI to use: physical or logical
Q_ANDROID_EXPORT qreal chooseDpi(qreal physical, qreal logical)
{
   if( logical < cInsaneLowDpi )
   {
       qWarning()<<"WARNING: device reports low logical DPI:"<<logical;
       return physical;
   }
   if( physical < cInsaneLowDpi )
   {
       qWarning()<<"WARNING: device reports low physical DPI:"<<physical;
       return logical;
   }
   qreal ratio = (physical > logical)? physical / logical: logical / physical;
   // Why 1.6? This is just a weird magic number, no reason. Nothing to look for, move along!
   if( ratio > 1.6f )
   {
       qWarning()<<"WARNING: device's logical and physical DPI "
                   "are too different, choosing logcal; physical ="
                 <<physical<<", logical ="<<logical;
       return logical;
   }
   return physical;
}

struct Q_ANDROID_EXPORT QDpiFixupTable
{
   char manufacturer[16];
   char device[16];
   char model[16];
   int width, height;
   qreal reported_dpi_x, reported_dpi_y;
   qreal real_dpi_x, real_dpi_y;

   // Compare data provided by OS with fixup table entry;
   // returns true if current device matches the entry.
   // Device name and model are retrieved using
   // qt_android_get_version_data().
   // NOTE: Must be w > h (swap values if necessary, and don't
   // forget to swap xd and yd as well).
   bool requiresFixup(int w, int h, qreal xd, qreal yd) const;
};

bool QDpiFixupTable::requiresFixup(int w, int h, qreal xd, qreal yd) const
{
   if( (w == width || width == 0) &&
       (h == height || height == 0) &&
       (reported_dpi_x == 0 || qAbs(xd - reported_dpi_x) < 1.0f) &&
       (reported_dpi_y == 0 || qAbs(yd - reported_dpi_y) < 1.0f) )
   {
       static bool gotData = false;
       static QString f, d, m;
       if( !gotData ) {
           const QMap<QString, QString>* map = qt_android_get_version_data();
           if( !map ) {
              qCritical()<<"Failed to obtain device identification!";
              return false;
           }
           f = (*map)[QLatin1String("MANUFACTURER")];
           d = (*map)[QLatin1String("DEVICE")];
           m = (*map)[QLatin1String("MODEL")];
           gotData = true;

#if 0
           qDebug()<<"******************** MANUFACTURER:"<<f;
           qDebug()<<"******************** DEVICE:"<<d;
           qDebug()<<"******************** MODEL:"<<m;
           // qDebug()<<"******************** PRODUCT:"<<(*map)[QLatin1String("PRODUCT")];
           // qDebug()<<"******************** BRAND:"<<(*map)[QLatin1String("BRAND")];
#endif
       }

       if( (manufacturer[0] == 0 || f == QLatin1String(manufacturer)) &&
           (device[0] == 0 || d == QLatin1String(device)) &&
           (model[0] == 0 || m == QLatin1String(model)) )
       {
           qDebug()<<"WARNING: Need to fix wrong DPI for"<<f<<d<<m;
           return true;
       }
       return false;
   }
   return false;
}

static QDpiFixupTable sDpiFixup[] = {
    { "motorola", "", "Milestone", 854, 480, 96.0f, 96.0f, 271.0f, 271.0f }
};

static size_t sDpiFixupSize = sizeof(sDpiFixup) / sizeof(QDpiFixupTable);

// Fix DPI settings using device name / vendor / returned settings.
Q_ANDROID_EXPORT void fixDpi(int w, int h, qreal* xdpi, qreal* ydpi, qreal logicalDpi)
{
    qDebug()<<"Checking DPI's for:"<<w<<h<<(*xdpi)<<(*ydpi)<<"Table size:"<<sDpiFixupSize
            <<"Logical DPI:"<<logicalDpi;
    // Fix for rotated screen
    bool rotate = w < h;
    if( rotate )
    {
        int t = w;
        w = h;
        h = t;
        qreal t2 = *xdpi;
        *xdpi = *ydpi;
        *ydpi = t2;
    }
    bool certainDpiFound = false;
    for( size_t i = 0; i < sDpiFixupSize; i++ )
       if( sDpiFixup[i].requiresFixup(w, h, *xdpi, *ydpi) ){
          *xdpi = sDpiFixup[i].real_dpi_x;
          *ydpi = sDpiFixup[i].real_dpi_y;
          certainDpiFound = true;
          break;
       }
    if( !certainDpiFound ) {
        *xdpi = fixDpi(chooseDpi(*xdpi, logicalDpi));
        *ydpi = fixDpi(chooseDpi(*ydpi, logicalDpi));
    }
    // Undo screen rotation, if necessary
    if( rotate )
    {
        qreal t = *xdpi;
        *xdpi = *ydpi;
        *ydpi = t;
    }
}

} // namespace QtAndroid
