/*
  Qt/Android Integration Core Library

  Author: Alexander A. Saytgalin <s.saytgalin@2gis.ru>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <QtCore/QDebug>
#include "qandroidclipboard.h"
#include <qandroidcorejava.h>

#if !defined(QT_NO_CLIPBOARD)

namespace QtAndroid
{

QAndroidClipboard::QAndroidClipboard()
    : QPlatformClipboard()
{
}

QAndroidClipboard::~QAndroidClipboard()
{
}

QMimeData* QAndroidClipboard::mimeData(QClipboard::Mode mode)
{
    if (mode == QClipboard::Clipboard) {
        QString text = qt_android_from_clipboard();
        if (text.isEmpty()) {
            return 0;
        }
        mClipboard.setText(text);
        return &mClipboard;
    }
    return 0;
}

void QAndroidClipboard::setMimeData(QMimeData* data, QClipboard::Mode mode)
{
    if (mode == QClipboard::Clipboard)
    {
        QString text;
        if (data && data->hasText())
        {
            text = data->text();
        }

        qt_android_to_clipboard(text);
    }
    else
    {
        qWarning("QClipboard::setMimeData: unsupported mode '%d'", mode);
    }
}

bool QAndroidClipboard::supportsMode(QClipboard::Mode mode) const
{
    if (mode == QClipboard::Clipboard)
        return true;
    return false;
}

} // namespace QtAndroid

#endif
