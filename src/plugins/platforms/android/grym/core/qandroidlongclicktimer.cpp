/*
  Qt/Android Integration Core Library

  Long click implementation.
  NOTE: We don't use native Android's long click event because it has
  some issues, like it is triggered when finger was moving during long click.
  I.e. this file implements long click support purely on C++/Qt side.

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtCore/qdebug.h>
#include <QtGui/QApplication>
#include <QtGui/QContextMenuEvent>
#include <QtGui/QWidget>
#include <QtCore/QEvent>
#include <QtCore/QMutex>
#include "qandroidlongclicktimer.h"
#include "qandroidutils.h"

namespace QtAndroid {

static const int longClickShiftThreshold = 10;
static const int badCoordinate = -1000000000;

#ifndef QT_ANDROID_NO_LONGCLICK
static QScopedPointer<QAndroidLongClickTimer> longClickTimer;
#endif

QAndroidLongClickTimer::QAndroidLongClickTimer()
    : m_x(badCoordinate)
    , m_y(badCoordinate)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    setInterval(1000);
    setSingleShot(true);
    connect(this, SIGNAL(timeout()), this, SLOT(longClick()));
#endif
}

QAndroidLongClickTimer::QAndroidLongClickTimer(const QAndroidLongClickTimer&)
    : QTimer()
{
}

void QAndroidLongClickTimer::longClick()
{
#ifndef QT_ANDROID_NO_LONGCLICK
    QMutexLocker locker(&QtAndroid::jniMutex);
    if( m_x == badCoordinate )
        return;
    if( !QApplication::instance() )
        return;
    QWidget* widget = QApplication::widgetAt( m_x, m_y );
    if( widget ){
        if( !widget->isVisible() )
            return;
        QPoint global_pos = QPoint( m_x, m_y );
        QPoint pos = widget->mapFromGlobal( global_pos );
        QContextMenuEvent * ctxmevent = new QContextMenuEvent(
                QContextMenuEvent::Mouse,
                pos, global_pos );
        QApplication::instance()->postEvent( widget, ctxmevent );
        // qDebug()<<"LONG CLICK POSTED EVENT TO"
        //        <<widget->metaObject()->className()<<pos.x()<<pos.y();
    }
    m_x = badCoordinate;
#endif
}

void QAndroidLongClickTimer::clickStarted(int x, int y)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    stop();
    m_x = x;
    m_y = y;
    start();
#else
    Q_UNUSED(x);
    Q_UNUSED(y);
#endif
}

void QAndroidLongClickTimer::clickMoved(int x, int y)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    if( m_x != badCoordinate )
        if( qAbs(m_x - x) > longClickShiftThreshold || qAbs(m_y - y) > longClickShiftThreshold ){
            // qDebug()<<"LONG CLICK RELEASING BECAUSE OF MOVE TO"<<x<<y";
            stop();
            m_x = badCoordinate;
        }
#else
    Q_UNUSED(x);
    Q_UNUSED(y);
#endif
}

void QAndroidLongClickTimer::clickReleased(int x, int y)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    // qDebug()<<"LONG CLICK RELEASED AT"<<x<<y;
    stop();
    m_x = badCoordinate;
#endif
    Q_UNUSED(x);
    Q_UNUSED(y);
}

void QAndroidLongClickTimer::clickReleased()
{
#ifndef QT_ANDROID_NO_LONGCLICK
    // qDebug()<<"LONG CLICK RELEASED (ABORT)";
    stop();
    m_x = badCoordinate;
#endif
}

void longClickStarted(int x, int y)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    if( !longClickTimer.isNull() ){
        QMutexLocker locker(&QtAndroid::jniMutex);
        longClickTimer->clickStarted(x, y);
    }
#else
    Q_UNUSED(x);
    Q_UNUSED(y);
#endif
}

void longClickMoved(int x, int y)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    if( !longClickTimer.isNull() ){
        QMutexLocker locker(&QtAndroid::jniMutex);
        longClickTimer->clickMoved(x, y);
    }
#else
    Q_UNUSED(x);
    Q_UNUSED(y);
#endif
}

void longClickReleased(int x, int y)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    if( !longClickTimer.isNull() ){
        QMutexLocker locker(&QtAndroid::jniMutex);
        longClickTimer->clickReleased(x, y);
    }
#else
    Q_UNUSED(x);
    Q_UNUSED(y);
#endif
}

void longClickReleased()
{
#ifndef QT_ANDROID_NO_LONGCLICK
    if( !longClickTimer.isNull() ){
        QMutexLocker locker(&QtAndroid::jniMutex);
        longClickTimer->clickReleased();
    }
#endif
}

} // namespace QtAndroid

Q_ANDROID_EXPORT void qt_android_set_long_click_enabled(bool enable)
{
#ifndef QT_ANDROID_NO_LONGCLICK
    QMutexLocker locker(&QtAndroid::jniMutex);
    if( enable ){
        if( QtAndroid::longClickTimer.isNull() )
            QtAndroid::longClickTimer.reset(new QtAndroid::QAndroidLongClickTimer());
    }else{
        if( !QtAndroid::longClickTimer.isNull() )
            QtAndroid::longClickTimer.reset(NULL);
    }
#else
    Q_UNUSED(enable);
#endif
}

Q_ANDROID_EXPORT bool qt_android_long_click_enabled()
{
#ifndef QT_ANDROID_NO_LONGCLICK
    QMutexLocker locker(&QtAndroid::jniMutex);
    return !QtAndroid::longClickTimer.isNull();
#else
    return false;
#endif
}
