/*
  Qt/Android Integration Core Library

  Authors:
    Ivan 'w23' Avdeev, <marflon@gmail.com>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QDebug>
#include <QPointer>
#include <QTextFormat>
#include <QWindowSystemInterface>

#include <QApplication>
#include <QLineEdit>
#include <QTextEdit>
#include <QTextCursor>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsProxyWidget>

#include "qandroidutils.h"
#include "qandroidinputcontext.h"
#include "qandroidkeyboard.h"

QPointer<QAndroidInputContext> QAndroidInputContext::s_instance;

QAndroidInputContext::QAndroidInputContext(QObject *parent) :
    QInputContext(parent),
    mComposingTextMutex(QMutex::Recursive)
{
    //qDebug()<<QtAndroid::keyboard_log_tag << "QAndroidInputContext::QAndroidInputContext";
    s_instance = this;
}

QAndroidInputContext::~QAndroidInputContext()
{
    //qDebug()<<QtAndroid::keyboard_log_tag << "~QAndroidInputContext";
}

QString QAndroidInputContext::identifierName()
{
    return "QAndroidInputContext";
}

bool QAndroidInputContext::isComposing() const
{
    //qDebug()<<QtAndroid::keyboard_log_tag << "QAndroidInputContext::isComposing()";
    QMutexLocker locker(&mComposingTextMutex); // Overkill?
    bool ret = !mComposingText.isEmpty();
    return ret;
}

// Qt documentation:
// "This function must be implemented in any subclasses to return a
// language code (e.g. "zh_CN", "zh_TW", "zh_HK", "ja", "ko", ...)
// of the input context. If the input context can handle multiple
// languages, return the currently used one. The name has to be
// consistent with QInputContextPlugin::language()."
QString QAndroidInputContext::language()
{
    return QString();
}

void QAndroidInputContext::mouseHandler( int x, QMouseEvent *event)
{
    // Default implementation resets input on each click.
    // Here, we should not do that.
    Q_UNUSED(x);
    Q_UNUSED(event);
}

void QAndroidInputContext::reset()
{
    // Note: QInputContext::reset() is pure virtual.

    // Qt documentation:
    // "You must not send any QInputMethodEvent except
    // empty InputMethodEnd event using QInputContext::reset()
    // at reimplemented reset(). It will break input state consistency."

    // TODO: this function may leave "hanging" composing text
    // on Qt side visible to user (although not used anywhere;
    // it will be cleaned out by any further input to the
    // same field).
    {
        QMutexLocker locker(&mComposingTextMutex);
        if (!mComposingText.isEmpty())
        {
            if (QtAndroid::keyboard_verbose_logs)
                qDebug()<<QtAndroid::keyboard_log_tag<<"QAndroidInputContext::reset() (clearing composing text)";
            mComposingText.clear(); // Also clears "composing" state
        }
        else
        {
            if (QtAndroid::keyboard_verbose_logs)
                qDebug()<<QtAndroid::keyboard_log_tag<<"QAndroidInputContext::reset()";
        }
    }
    qt_android_reset_imm();
    update();
}

void QAndroidInputContext::setFocusWidget( QWidget * widget )
{
    // We could do something here. But no idea what.
    // if (focusWidget() && widget != focusWidget())...
    QInputContext::setFocusWidget(widget);
}

void QAndroidInputContext::widgetDestroyed ( QWidget * widget )
{
    // TODO - implement something?
    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<"QAndroidInputContext::widgetDestroyed()";
    QInputContext::widgetDestroyed(widget);
}

void QAndroidInputContext::update()
{
    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<"QAndroidInputContext::update()";
    QInputContext::update();
}

bool QAndroidInputContext::filterEvent( const QEvent * event )
{
    // Auto SIP implementation is here!
    // Note: the events are sent automatically from Qt unless
    // auto opening and closing of SIP is disabled (in Qt).
    switch (event->type())
    {
        case QEvent::RequestSoftwareInputPanel: // Sent by Qt auto SIP mechanism
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"- Open SIP";
            qt_android_show_software_keyboard(true);
            return true;
        case QEvent::CloseSoftwareInputPanel: // Sent by Qt auto SIP mechanism
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"- Close SIP";
            qt_android_show_software_keyboard(false);
            return true;
        default:
            return QInputContext::filterEvent(event);
    }
}

void QAndroidInputContext::inputKeyEvent(int keyCode, int metaState)
{
    // keyCode is Android system keycode!
    qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<">>>>"<<keyCode;
    // Android key code => Qt key code
    int qt_key = QtAndroid::mapAndroidKey(keyCode);
    if (qt_key){
        QString text; // Text generated by the key
        // TODO: figure out - maybe Enter should be added to the text
        // for multi-line text inputs as well?
        // (|| key==13)
        if( qt_key >=32 // Non-control characters
            || qt_key==9 ) // Tab
            text += QChar(qt_key);

        // Applying keyboard modifiers
        Qt::KeyboardModifiers modifiers = QtAndroid::mapAndroidModifiers(metaState);
        if (!text.isEmpty()) {
            if (!(modifiers & Qt::ShiftModifier))
                text = text.toLower();
            else
                text = text.toUpper();
        }

        qDebug()<<QtAndroid::keyboard_log_tag
                <<"QAndroidInputContext::inputKeyEvent: Android keycode"
                <<keyCode<<"=>"<<qt_key
                <<"text:"<<text;

        // TODO: investigate this code fragment:
        // 1. Is it really necessary?
        // 2. If it's necessary, shouldn't it do the same filtering
        // as qtandroidkeyboard.cpp ::keyEvent()?
        // If Enter has been pressed, reset input context to apply non-commited text
        if (keyCode == 0x42 || // Return, decimal 66
            keyCode == 0x17 || // DPAD_CENTER
            !text.isEmpty() )
        {
            commitCurrentText(false); // Apply composing text
        }

        QWindowSystemInterface::handleKeyEvent( focusWidget(), QEvent::KeyPress, qt_key, modifiers, text );
        QWindowSystemInterface::handleKeyEvent( focusWidget(), QEvent::KeyRelease, qt_key, modifiers, text );

        // Make sure all key events are processed before returning,
        // so Android full-screen text editor would reflect actual text state.
        QCoreApplication::processEvents();

    } else {
        qDebug()<<QtAndroid::keyboard_log_tag<<"QAndroidInputContext::inputKeyEvent: unhandled Android keycode"<<keyCode;
    }

    qDebug()<<QtAndroid::keyboard_log_tag<<"<<<<"<<__FUNCTION__;

    QtAndroid::keyboard_semaphore.open();
}

// Set composing text, i.e. something which is still being composed
// and may be modified later, for example, by providing another
// autocompletion suggestion.
// TODO: support newCursorPosition
void QAndroidInputContext::inputComposingText(const QString& text, int newCursorPosition)
{
    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<">>>>"<<text<<"newCursorPosition:"<<newCursorPosition;
    QList<QInputMethodEvent::Attribute> attrs;
    attrs << QInputMethodEvent::Attribute(
                 QInputMethodEvent::TextFormat,
                 0,
                 text.length(),
                 standardFormat(QInputContext::PreeditFormat));
    QInputMethodEvent e(text, attrs); // Passing "text" as preedit text
    sendEvent(e);
    // Note that 'text' can be a reference to mComposingText!
    {
        QMutexLocker locker(&mComposingTextMutex);
        mComposingText = text;
    }
    QCoreApplication::processEvents();
    QtAndroid::keyboard_semaphore.open();
}

// Set new value of the composing text and finish it, embedding it
// into the field being edited.
// TODO: support newCursorPosition
void QAndroidInputContext::inputCommitText(const QString& text, int newCursorPosition, bool open_semaphore)
{
    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__
               <<(QString("text: \"")+text+"\"");
    QInputMethodEvent e;
    e.setCommitString(text);
    sendEvent(e);
    // Note that 'text' can be a reference to mComposingText!
    {
        QMutexLocker locker(&mComposingTextMutex);
        mComposingText.clear();
    }
    QCoreApplication::processEvents();
    if (open_semaphore)
        QtAndroid::keyboard_semaphore.open();
}

void QAndroidInputContext::commitCurrentText(bool open_semaphore)
{
    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__
               <<(QString("text: \"")+mComposingText+"\"")
               <<"Semaphore:"<<open_semaphore;
    if (!mComposingText.isEmpty())
        inputCommitText(mComposingText, 1, false);
    if (open_semaphore)
        QtAndroid::keyboard_semaphore.open();
}

void QAndroidInputContext::resetCurrentText(bool open_semaphore)
{
    reset();
    if (open_semaphore)
        QtAndroid::keyboard_semaphore.open();
}

QString QAndroidInputContext::editorText()
{
    if (!isAppRunning()) {
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Application is not running.";
        return QString();
    }

    QWidget* widget = editorWidget();

    if( !widget ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"No focus widget.";
        return QString();
    }

    QLineEdit* le = dynamic_cast<QLineEdit*>(widget);
    if( le ){
        QString text = le->text();
                       //>displayText();
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Returning QLineEdit text:"<<text;
        return text;
    }

#ifndef QT_NO_TEXTEDIT
    QTextEdit* te = dynamic_cast<QTextEdit*>(widget);
    if( te ){
        QString text = te->toPlainText();
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Returning QTextEdit text:"<<text;
        return text;
    }
#endif

    qDebug()<<QtAndroid::keyboard_log_tag
            <<__FUNCTION__<<"Unknown type of text editor control:"
            <<widget->metaObject()->className()
            <<widget->objectName();
    return QString();
}

QString QAndroidInputContext::composingText() const
{
    QMutexLocker locker(&mComposingTextMutex);
    QString result = mComposingText;
    return result;
}

int QAndroidInputContext::cursorPosition()
{
    if( !isAppRunning() ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Application is not running.";
        return 0;
    }

    QWidget* widget = editorWidget();
    if( !widget ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"No focus widget.";
        return 0;
    }

    QLineEdit* le = dynamic_cast<QLineEdit*>(widget);
    if( le ){
        int pos = le->cursorPosition();
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Returning QLineEdit pos:"<<pos;
        return pos;
    }

#ifndef QT_NO_TEXTEDIT
    QTextEdit* te = dynamic_cast<QTextEdit*>(widget);
    if( te )
    {
        QTextCursor cursor = te->textCursor();
        int pos = cursor.position();
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Returning QTextEdit pos:"<<pos;
        return pos;
    }
#endif

    qDebug()<<QtAndroid::keyboard_log_tag
            <<__FUNCTION__<<"Unknown type of text editor control!"
            <<widget->metaObject()->className()
            <<widget->objectName();
    return 0;
}

// Helper for QAndroidInputContext::deleteSurroundingText(int leftLength, int rightLength)
static QString doDeleteSurroundingText(const QString& text, int cursor, int leftLength, int rightLength)
{
    QString tx = text;
    int begin_remove = cursor - leftLength;
    if( begin_remove < 0 ) {
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Negative start index!";
        leftLength = cursor;
        begin_remove = 0;
    }
    // length_remove needs no fixing, QString::remove() is smart enough
    int length_remove = leftLength + rightLength;
    return tx.remove(begin_remove, length_remove);
}

void QAndroidInputContext::deleteSurroundingText(int leftLength, int rightLength)
{
    if( !isAppRunning() ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Application is not running.";
        QtAndroid::keyboard_semaphore.open();
        return;
    }

    QWidget* widget = editorWidget();
    if( !widget ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"No focus widget.";
        QtAndroid::keyboard_semaphore.open();
        return;
    }

    QLineEdit* le = dynamic_cast<QLineEdit*>(widget);
    if( le ){
        int pos = le->cursorPosition();
        QString before = le->text();
        QString after = doDeleteSurroundingText(before, pos, leftLength, rightLength);
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__
                <<"Cutting from QLineEdit:"<<leftLength<<rightLength
                <<"Before:"<<before<<"After:"<<after;
        le->setText(after);
        le->setCursorPosition(qMax(pos-leftLength, 0));
        QtAndroid::keyboard_semaphore.open();
        return;
    }

#ifndef QT_NO_TEXTEDIT
    QTextEdit* te = dynamic_cast<QTextEdit*>(widget);
    if( te )
    {
        // This is totally untested code!
        QTextCursor cursor = te->textCursor();
        int pos = cursor.position();
        QString before = te->toPlainText();
        QString after = doDeleteSurroundingText(before, pos, leftLength, rightLength);
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Cutting from QTextEdit:"<<leftLength<<rightLength;
        te->setText(after);
        cursor.setPosition(qMax(pos-leftLength, 0));
        te->setTextCursor(cursor);
        QtAndroid::keyboard_semaphore.open();
        return;
    }
#endif

    qDebug()<<QtAndroid::keyboard_log_tag
            <<__FUNCTION__<<"Unknown type of text editor control!"
            <<widget->metaObject()->className()
            <<widget->objectName();
    QtAndroid::keyboard_semaphore.open();
}


bool QAndroidInputContext::getSelection(int* outStart, int* outLength, QString* outText, bool* multilineEditor)
{
    if( !isAppRunning() ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Application is not running.";
        return false;
    }

    QWidget* widget = editorWidget();
    if( !widget ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"No focus widget.";
        return false;
    }

    QLineEdit* le = dynamic_cast<QLineEdit*>(widget);
    if( le ){
        if( outStart )
            *outStart = le->selectionStart(); // -1 if no selection
        if( outLength || outText ) {
            QString text = le->selectedText();
            if( outLength )
                *outLength = text.length();
            if( outText )
                *outText = text;
        }
        if (multilineEditor)
            *multilineEditor = false;
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Returning current selection.";
        return true;
    }

    // TODO: implement selections for multi-line text edits!

    if (multilineEditor)
        *multilineEditor = true;

    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Not implemented for current widget.";
    return false;
}

void QAndroidInputContext::setSelection(int start, int length)
{
    if( !isAppRunning() ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Application is not running.";
        QtAndroid::keyboard_semaphore.open();
        return;
    }

    QWidget* widget = editorWidget();
    if( !widget ){
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"No focus widget.";
        QtAndroid::keyboard_semaphore.open();
        return;
    }

    QLineEdit* le = dynamic_cast<QLineEdit*>(widget);
    if( le ){
        if( length==0 ) {
            if (QtAndroid::keyboard_verbose_logs)
                qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Set cursor position:"<<start;
            le->deselect();
            le->setCursorPosition(start);
        } else {
            le->setCursorPosition(start+length);
            le->setSelection(start, length);
            if (QtAndroid::keyboard_verbose_logs)
                qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Set selection:"<<start<<length;
        }
        QtAndroid::keyboard_semaphore.open();
        return;
    }

    // TODO: implement selections for multi-line text edits!

    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Not implemented for current widget.";

    QtAndroid::keyboard_semaphore.open();
}

#if 0
    #define TEXTWIDGETSEARCH_VERBOSE(x) x
#else
    #define TEXTWIDGETSEARCH_VERBOSE(x)
#endif

// Check that Qt application is currently working and active
// (i.e. fully suitable for text input)
bool QAndroidInputContext::isAppRunning()
{
    TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Checking app...");
    QApplication* app = (QApplication*)QApplication::instance();
    if( !app )
    {
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"NULL app instance.");
        return false;
    }
    TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"App found, checking...");
    if( !app->activeWindow() )
    {
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"No active window.");
        return false;
    }
    if( !app->activeWindow()->isVisible() )
    {
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Active window is hidden.");
        return false;
    }
    TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"App running.");
    return true;
}

// Find currently active editor widget.
QWidget* QAndroidInputContext::editorWidget()
{
    // QInputContext function. You might thought
    // it does the job, but... look further
    QWidget* widget = focusWidget();

    //
    // Check the widget "officially" focused by QInputContext first
    //
    if( !widget ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"focusWidget is NULL");
        return NULL;
    }
    TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag
                    <<__FUNCTION__<<"Focus widget:"
                    <<widget->metaObject()->className()
                    <<widget->objectName());
    if( isEditorWidget(widget) )
        return widget;

    //
    // If focused widget is a graphics view, search it for
    // text edit control which floats in a graphics scene
    //
    if( dynamic_cast<QGraphicsView*>(widget) ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"focusWidget is QGraphicsView");
        return graphicsViewEditorWidget(widget);
    }

    //
    // Check focused widget's focus proxy (is it really necessary?)
    // Not sure: maybe we should just back off here?
    //
    QWidget* fpwidget = widget->focusProxy();
    if( !widget ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"focusProxy is NULL");
        return NULL;
    }
    if( isEditorWidget(fpwidget) )
        return widget;
    //
    // If focus proxy widget is a graphics view, search it for
    // text edit control which floats in a graphics scene
    //
    if( dynamic_cast<QGraphicsView*>(widget) ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"focusProxy is QGraphicsView");
        return graphicsViewEditorWidget(widget);
    }

    // TODO: what happens with QML UI?

    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Failed to find text edit control.";
    return NULL;
}

// Helper for editorWidget(): find currently active editor widget
// inside of a graphics view.
QWidget* QAndroidInputContext::graphicsViewEditorWidget(QWidget* graphview)
{
    QGraphicsView* gv = dynamic_cast<QGraphicsView*>(graphview);
    if( !gv ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Not a graphics view!");
        return NULL;
    }
    QGraphicsScene* sc = gv->scene();
    if( !sc ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"View has no scene.");
        return NULL;
    }
    QGraphicsItem* fi = sc->focusItem();
    if( !fi ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Scene has no focus item.");
        return NULL;
    }
    QGraphicsItem* gi = fi->focusItem();
    if( gi ){
        QGraphicsProxyWidget* gpw = dynamic_cast<QGraphicsProxyWidget*>(gi);
        if( gpw && isEditorWidget(gpw->widget()) )
            return gpw->widget();
    }
    QGraphicsItem* gp = fi->focusProxy();
    if( gp ){
        QGraphicsProxyWidget* gpw = dynamic_cast<QGraphicsProxyWidget*>(gp);
        if( gpw && isEditorWidget(gpw->widget()) )
            return gpw->widget();
    }
    return NULL;
}

// Test if 'widget' is suitable for using with text input connection.
// Believe it or not, but Qt does not have a common interface for
// all text editors! So we have to handle each type separately using
// switches. Very ugly.
bool QAndroidInputContext::isEditorWidget(QWidget* widget)
{
    if( dynamic_cast<QLineEdit*>(widget) ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Widget is QLineEdit");
        return true;
    }
#ifndef QT_NO_TEXTEDIT
    if( dynamic_cast<QTextEdit*>(widget) ){
        TEXTWIDGETSEARCH_VERBOSE(if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Widget is QTextEdit");
        return true;
    }
#endif
    return false;
}

// TODO: this function is a hack which clears current
// composing text. It should be taken out when
// QAndroidInputContext::reset() learn to do that.
// This function could be called before programmatically
// clearning a text input field which might be currently
// connected to IMM.
Q_DECL_EXPORT bool qt_android_reset_input_context()
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__;
    QAndroidInputContext * ic = QAndroidInputContext::instance();
    bool ret = false;
    if (ic) {
        if (ic->isComposing()) {
            ret = true;
            QtAndroid::keyboard_semaphore.close();
            const QString empty("");
            QMetaObject::invokeMethod(ic, "inputCommitText",
                                      Qt::AutoConnection,
                                      Q_ARG(const QString&,empty),
                                      Q_ARG(int,1),
                                      Q_ARG(bool,true));
            QtAndroid::keyboard_semaphore.wait();
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(ic, "resetCurrentText", Qt::AutoConnection, Q_ARG(bool,true));
            QtAndroid::keyboard_semaphore.wait();
        }
    }
    return ret;
}

// Untested!
// TODO: move somehere else / put in header / etc.
Q_DECL_EXPORT void qt_android_commit_and_hide_software_keyboard()
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    if (QtAndroid::keyboard_verbose_logs)
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__;
    QAndroidInputContext * ic = QAndroidInputContext::instance();
    if (ic) {
        if (ic->isComposing()) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(ic, "commitCurrentText", Qt::AutoConnection, Q_ARG(bool,true));
            QtAndroid::keyboard_semaphore.wait();
        }
    }
    qt_android_show_software_keyboard(false);
}

// This function must be called when user touches screen
Q_DECL_EXPORT void qt_android_keyboard_touched()
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    QAndroidInputContext * ic = QAndroidInputContext::instance();
    if (ic) {
        if (ic->isComposing() && ic->editorWidget()) {
            if (QtAndroid::keyboard_verbose_logs)
                qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__;
            // Commit current text & wait for processing
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(ic, "commitCurrentText", Qt::AutoConnection, Q_ARG(bool,true));
            QtAndroid::keyboard_semaphore.wait();
            // Make sure SIP understood what happened
            qt_android_reset_imm();
        }
    }
}

