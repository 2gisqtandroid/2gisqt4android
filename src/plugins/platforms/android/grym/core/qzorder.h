/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  RICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QZORDER_H
#define QZORDER_H

// Comment out for experimental multi-TLW Z-Order support
// #define QT_ANDROID_CORE_NOZORDER

#include <QtCore/QSharedPointer>
#include <QtCore/QWeakPointer>
#include <QtGui/QWidget>
#include <QtGui/QWidgetList>
#include "qandroidcoreexports.h"

#ifndef QT_ANDROID_CORE_NOZORDER

namespace QtAndroid {

// An interface which implements Z-order.
class Q_ANDROID_EXPORT QZOrder: public QObject
{
    Q_OBJECT
public:
    virtual ~QZOrder();

    // Default implementation does nothing, just ignores Z-Order.
    // This is also set as default instance.
    virtual void zSort(QWidgetList* tlwlist);
    static QWeakPointer<QZOrder> instance(){ return s_Instance; }

    // Plugins may set another implementation.
    static void setInstance(QSharedPointer<QZOrder> zo);
    static void sort(QWidgetList* tlwlist);
private:
    static QWeakPointer<QZOrder> s_Instance;
};

// Z-order AI - determines Z-order of widgets from their types
// and QApplication's pointers to currently active widgets.
// This is enough to support very minimal mutli-TLW functionality,
// like main window + dialog, hints, popup menus.
class Q_ANDROID_EXPORT QZOrderAi: public QZOrder
{
    Q_OBJECT
public:
    virtual ~QZOrderAi();
    virtual void zSort(QWidgetList* tlwlist);
};

} // namespace QtAndroid

#endif

#endif
