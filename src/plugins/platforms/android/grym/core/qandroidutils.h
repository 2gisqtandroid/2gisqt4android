/*
  Qt/Android Integration Core Library

  Authors:
    Sergey A. Galin <sergey.galin@gmail.com>
    Ivan 'w23' Avdeev, <marflon@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QANDROIDUTILS_H
#define QANDROIDUTILS_H

#include <jni.h>
#include <QImage>
#include <QMutex>
#include "qandroidcoreexports.h"

#define JNI_CHECK_EXCEPTION(env)       \
    {                                  \
        if( env->ExceptionCheck() ) {  \
            env->ExceptionDescribe();  \
            env->ExceptionClear();     \
        }                              \
    }


namespace QtAndroid {

Q_ANDROID_EXPORT extern QMutex jniMutex; // Recursive

Q_ANDROID_EXPORT QString jstringToQString(JNIEnv *env, jstring str);

// Don't forget to do DeleteLocalRef() on returned jstring.
Q_ANDROID_EXPORT jstring QStringToJstring(JNIEnv *env, const QString& str);

Q_ANDROID_EXPORT void Log(QtMsgType type, const char *msg);

Q_ANDROID_EXPORT QImage::Format qAndroidBitmapFormat_to_QImageFormat(uint32_t abf);

} // namespace QtAndroid

// Used by libQtAndroidMain to tell if main() is currently working, or finished.
Q_ANDROID_EXPORT void qt_android_set_in_main(bool inMain);
Q_ANDROID_EXPORT bool qt_android_is_in_main();

// Setting Android filesystem directories. These paths are available
// later through corresponding exported functions and from environment
// variables: QT_ANDROID_STORAGE, TMP/TEMP/QT_ANDROID_EXTERNAL_FILES,
// HOME/EXTERNAL_STORAGE, QT_ANDROID_CACHE.

Q_DECL_EXPORT void qt_android_set_internal_files_path(JNIEnv* env, jclass, jstring string);
Q_DECL_EXPORT void qt_android_set_external_files_path(JNIEnv* env, jclass, jstring string);
Q_DECL_EXPORT void qt_android_set_external_storage_path(JNIEnv* env, jclass, jstring string);
Q_DECL_EXPORT void qt_android_set_cache_path(JNIEnv*, jclass, jstring string);
Q_DECL_EXPORT void qt_android_set_apk_lib_path(JNIEnv*, jclass, jstring string);
Q_DECL_EXPORT void qt_android_set_font_path(JNIEnv*, jclass, jstring string);
Q_DECL_EXPORT void qt_android_set_font_mask(JNIEnv*, jclass, jstring string);

// Set Linux locale variable. It can be read in your app only using: getenv("LANG").
Q_DECL_EXPORT void qt_android_set_locale(JNIEnv*, jclass, jstring string);

// If native main() function did not finish yet, send Quit event to QApplication
// (if its instance exists) and allow few seconds to finish. The parameters are not used.
Q_DECL_EXPORT void qt_android_exit_qtmain(JNIEnv* env = 0, jclass clazz = 0);

// Send events for application & its window activation/deactivation to Qt
Q_DECL_EXPORT void qt_android_send_application_activated(JNIEnv*, jobject);
Q_DECL_EXPORT void qt_android_send_application_deactivated(JNIEnv*, jobject);

// Set screen metrics - this function is called before main(), before creation
// of QApplication and way before QDesktopWidget.
// Some plugins, e.g. mw and mw_grym, don't have valid data in QDesktopWidget
// until app's main window is created and shown.
Q_DECL_EXPORT void qt_android_set_screen_metrics(JNIEnv*, jclass, jint widthPixels, jint heightPixels,
                                                 jfloat xdpi, jfloat ydpi, jfloat densityDpi);

// Context menu methods which are called from Java to C++.
// This method called when item of context menu selected.
Q_DECL_EXPORT jboolean qt_android_java_context_menu_call(JNIEnv*, jclass, jint item_id);
Q_DECL_EXPORT jboolean qt_android_java_context_menu_closed(JNIEnv*, jclass);

//Q_DECL_EXPORT void qt_android_java_context_menu_build(JNIEnv *env, jclass, /*android.view.ContextMenu*/jobject menu);


#define QANDROIDUTILS_JNI_EXPORTS \
    {"qt_android_set_internal_files_path", "(Ljava/lang/String;)V", (void*)qt_android_set_internal_files_path}, \
    {"qt_android_set_external_files_path", "(Ljava/lang/String;)V", (void *)qt_android_set_external_files_path}, \
    {"qt_android_set_external_storage_path", "(Ljava/lang/String;)V", (void *)qt_android_set_external_storage_path}, \
    {"qt_android_set_cache_path", "(Ljava/lang/String;)V", (void *)qt_android_set_cache_path}, \
    {"qt_android_set_apk_lib_path", "(Ljava/lang/String;)V", (void *)qt_android_set_apk_lib_path}, \
    {"qt_android_set_font_path", "(Ljava/lang/String;)V", (void *)qt_android_set_font_path}, \
    {"qt_android_set_font_mask", "(Ljava/lang/String;)V", (void *)qt_android_set_font_mask}, \
    {"qt_android_set_locale", "(Ljava/lang/String;)V", (void *)qt_android_set_locale}, \
    {"qt_android_exit_qtmain", "()V", (void*)qt_android_exit_qtmain}, \
    {"qt_android_send_application_activated", "()V", (void *)qt_android_send_application_activated}, \
    {"qt_android_send_application_deactivated", "()V", (void *)qt_android_send_application_deactivated}, \
    {"qt_android_set_screen_metrics", "(IIFFF)V", (void *)qt_android_set_screen_metrics}, \
    {"qt_android_java_context_menu_call", "(I)Z", (void *)qt_android_java_context_menu_call}, \
    {"qt_android_java_context_menu_closed", "()Z", (void *)qt_android_java_context_menu_closed}

//    {"qt_android_java_context_menu_build", "(Landroid/view/ContextMenu;)V", (void *)qt_android_java_context_menu_build}


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// EXPERIMENTAL - THE WORKAROUND FUNCTIONS BELOW WHICH WILL BE TAKEN OUT IN FUTURE
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// TLW filter control

class QWidget;

Q_ANDROID_EXPORT bool qt_android_get_filter_tlw_widgets();
Q_ANDROID_EXPORT void qt_android_set_filter_tlw_widgets(bool filter);
Q_ANDROID_EXPORT bool qt_android_get_filter_tlw_widget_input();
Q_ANDROID_EXPORT void qt_android_set_filter_tlw_widget_input(bool filter);
Q_ANDROID_EXPORT void qt_android_set_tlw_standard_names(bool wl);
Q_ANDROID_EXPORT void qt_android_add_tlw_class(const QString& name);
Q_ANDROID_EXPORT void qt_android_add_tlw_object(const QString& name);
Q_ANDROID_EXPORT bool qt_android_is_tlw_widget( const QWidget* w );

#endif
