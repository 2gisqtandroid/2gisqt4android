/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <qdebug.h>
#include <QApplication>
#include <QWidget>
#include <QDesktopWidget>
#include <QEvent>
#include <QList>
#include <QPointer>
#include <QTouchEvent>
#include <QWindowSystemInterface>
#include "qandroidpointingdevices.h"
#include "qandroidlongclicktimer.h"
#include "qandroidutils.h"
#include "qandroidinputcontext.h"
#include "qzorder.h"

#define TOUCH_MUTEX

static QList<QWindowSystemInterface::TouchPoint> m_touchPoints;

// Pointers to the active widgets. They are usually the same but handled
// differently (the separation between mouse and touch is done on Java side).
static QPointer<QWidget> m_touchWidget, m_mouseWidget;

// FIXME: thread-safety? What if the widget, or QApplication
// is being removed right when searching for a window?
static QWidget* findTouchWindow(int x, int y)
{
    QWidgetList wl = QApplication::topLevelWidgets();
#ifndef QT_ANDROID_CORE_NOZORDER
    QtAndroid::QZOrder::sort(&wl);
#endif
    bool notlwfilter = !qt_android_get_filter_tlw_widget_input();
    for( int i = wl.size()-1; i>=0; i-- ) {
        QWidget* w = wl.at(i);
        if( w->isWindow() &&
            w->isVisible() &&
            w->geometry().contains(x, y) &&
            (notlwfilter || qt_android_is_tlw_widget(w)) )
        {
            /*qDebug()<<"findTouchWindow found a window:"
                    <<w->metaObject()->className()
                    <<"****************************************";*/
            return w;
        }
    }
    // Fallback! This should not happen during normal program work, but it happens
    // during initialization and deinitialization.
    if(wl.empty()) // QApplication::topLevelAt() will crash if there are no widgets
        return 0;
    QWidget * stdtlw = QApplication::topLevelAt(x, y);
    return stdtlw;
}


Q_DECL_EXPORT void qt_android_mouse_down(JNIEnv*, jobject, jint x, jint y, jlong time)
{
#if defined(TOUCH_MUTEX)
    QMutexLocker locker(&QtAndroid::jniMutex);
#endif

    if( !qt_android_is_in_main() || !QApplication::instance() ) {
        m_mouseWidget = 0;
        return;
    }

    m_mouseWidget = findTouchWindow(x, y);
    if( !m_mouseWidget )
        return;

    // OK, we have a touch to some widget

    qt_android_keyboard_touched(); // Drop any hanging input

    // qDebug()<<"MOUSE WIDGET:"<<m_mouseWidget.data()->metaObject()->className();

    // FIXME: This call is not good, as moused top-level
    // widget can be deleted by other thread right during the operation!
    QPoint p = m_mouseWidget->mapFromGlobal(QPoint(x, y));

    QWindowSystemInterface::handleMouseEvent(
            m_mouseWidget.data(),
            (ulong)time,
            p,
            p,
            Qt::MouseButtons(Qt::LeftButton));

#ifndef QT_ANDROID_NO_LONGCLICK
    QtAndroid::longClickStarted(x, y);
#endif
}

Q_DECL_EXPORT void qt_android_mouse_up(JNIEnv*, jobject, jint x, jint y, jlong time)
{
#if defined(TOUCH_MUTEX)
    QMutexLocker locker(&QtAndroid::jniMutex);
#endif

#ifndef QT_ANDROID_NO_LONGCLICK
    QtAndroid::longClickReleased(x, y);
#endif

    if( !qt_android_is_in_main() || !QApplication::instance() ) {
        m_mouseWidget = 0;
        return;
    }

    if( !m_mouseWidget )
        return;

    QPoint p = m_mouseWidget->mapFromGlobal(QPoint(x, y));

    QWindowSystemInterface::handleMouseEvent(
            m_mouseWidget.data(),
            (ulong)time,
            p,
            p,
            Qt::MouseButtons(Qt::NoButton)); // Qt::LeftButton you send should not

    m_mouseWidget = 0;
}

Q_DECL_EXPORT void qt_android_mouse_move(JNIEnv* env, jobject jo, jint x, jint y, jlong time)
{
#if defined(TOUCH_MUTEX)
    QMutexLocker locker(&QtAndroid::jniMutex);
#endif

#ifndef QT_ANDROID_NO_LONGCLICK
    QtAndroid::longClickMoved(x, y);
#endif

    if( !qt_android_is_in_main() || !QApplication::instance() ) {
        m_mouseWidget = 0;
        return;
    }

    if( !m_mouseWidget )
        return;

    // Check if mouse stays on the same top-level widget
    QPointer<QWidget> tlw( findTouchWindow(x, y) );
    if( tlw.data() != m_mouseWidget.data() ) {
       qt_android_mouse_up(env, jo, x, y, time);
       return;
    }

    // Send mouse move event
    QPoint p = m_mouseWidget->mapFromGlobal(QPoint(x, y));

    QWindowSystemInterface::handleMouseEvent(
            m_mouseWidget.data(),
            (ulong)time,
            p,
            p,
            Qt::MouseButtons(Qt::LeftButton));
}

//
// TOUCH EVENTS
//
// Note: touch events are, in fact, arrays of active points;
// to pass an array from Java to C++ we fist call "begin"
// function, then "add" for each array element, then "end".
//

static bool s_touch_to_send = false;

Q_DECL_EXPORT void qt_android_touch_begin(JNIEnv*, jobject)
{
#if defined(TOUCH_MUTEX)
    QMutexLocker locker(&QtAndroid::jniMutex);
#endif
    m_touchPoints.clear();
    m_touchWidget = 0;
    s_touch_to_send = true;
}

Q_DECL_EXPORT void qt_android_touch_add(JNIEnv*, jobject, jint id, jint action, jboolean primary, jint x, jint y, jfloat size, jfloat pressure)
{
#if defined(TOUCH_MUTEX)
    QMutexLocker locker(&QtAndroid::jniMutex);
#endif
    if( !s_touch_to_send || !qt_android_is_in_main() || !QApplication::instance() ) {
        m_touchWidget = 0;
        return;
    }

    if( !m_touchWidget )
    {
        m_touchWidget = findTouchWindow(x, y);
        if( !m_touchWidget )
        {
            // Lots of the stuff below uses m_touchWidget - run away!
            s_touch_to_send = false;
            return;
        }
    }

    QPointF p(m_touchWidget->mapFromGlobal(QPoint(x, y)));

    Qt::TouchPointState state = Qt::TouchPointStationary;
    switch(action)
    {
    case 0:
        state=Qt::TouchPointPressed;
        break;
    case 1:
        state=Qt::TouchPointMoved;
        break;
    case 2:
        state=Qt::TouchPointStationary;
        break;
    case 3:
        state=Qt::TouchPointReleased;
        break;
    }

    QWindowSystemInterface::TouchPoint touchPoint;
    QDesktopWidget* desktop = QApplication::desktop();
    if( !desktop )
    {
        qDebug()<<"qt_android_touch_add: no desktop widget!";
        return;
    }
    qreal dw = desktop->width(), dh = desktop->height(), dws = dw * size, dhs = dh * size;
    touchPoint.id = id;
    touchPoint.isPrimary = primary;
    touchPoint.state = state;
    touchPoint.pressure = pressure;
    touchPoint.normalPosition = QPointF( p.x() / dw, p.y() / dh );
    touchPoint.area=QRectF( p.x()-dws*0.5, p.y()-dhs*0.5, dws, dhs ); // Area covered by the finger
    m_touchPoints.push_back(touchPoint);
}

Q_DECL_EXPORT void qt_android_touch_end(JNIEnv*, jobject, jint action, jlong time)
{
#if defined(TOUCH_MUTEX)
    QMutexLocker locker(&QtAndroid::jniMutex);
#endif
    if( !s_touch_to_send || !m_touchWidget )
    {
        return;
    }
    if( !qt_android_is_in_main() || !QApplication::instance() ) {
        m_touchWidget = 0;
        return;
    }

    QEvent::Type eventType;
    switch (action)
    {
        case 0:
            eventType = QEvent::TouchBegin;
            // Drop any hanging input.
            // It is also done in qt_android_mouse_down() but this place may
            // occur earlier so let's rather be on the safe side and do it
            // from here as well.
            qt_android_keyboard_touched();
            break;
        case 1:
            eventType = QEvent::TouchUpdate;
            break;
        case 2:
            eventType = QEvent::TouchEnd;
            break;
        default:
            qWarning()<<"Bad touch event action value:"<<action;
            return;
    }

    QWindowSystemInterface::handleTouchEvent(
            m_touchWidget.data(),
            (ulong)time,
            eventType,
            QTouchEvent::TouchScreen,
            m_touchPoints);

    // Prevent sending more touch events until new touch is started
    m_touchWidget = 0;
}
