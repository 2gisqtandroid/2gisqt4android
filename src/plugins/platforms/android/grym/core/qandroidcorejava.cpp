/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <android/log.h>
#include <QString>
#include <QStringList>
#include <qdebug.h>
#include "qandroidcorejava.h"
#include "qandroidutils.h"
#include "qandroidkeyboard.h"
#include "qandroidpointingdevices.h"

#define ATTACH_THREAD(RET) \
    JNIEnv* env; \
    if (m_javaVM->AttachCurrentThread(&env, NULL)<0) { \
        qCritical()<<__FUNCTION__<<"- AttachCurrentThread failed!"; \
        return RET; \
    }

#define CHECK_ID(id, RET) \
    if( !m_applicationObjectBaseClass || !id ) { \
        qCritical()<<__FUNCTION__<<"- Environment is not set!"; \
        return RET; \
    }

#define ATTACH_AND_CHECK(id, RET) \
    ATTACH_THREAD(RET); \
    CHECK_ID(id, RET);

#define EMPTY_MACRO

#if 0
    #define PRINT_FUNCTION qDebug()<<__FUNCTION__
#else
    #define PRINT_FUNCTION
#endif

#define VOID_FUNCTION(id) \
    PRINT_FUNCTION; \
    ATTACH_AND_CHECK(id, EMPTY_MACRO); \
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, id);

#define VOID_RETBOOL_FUNCTION(id) \
    PRINT_FUNCTION; \
    ATTACH_AND_CHECK(id, false); \
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, id); \
    return true;

#define GET_INT_FUNCTION(id) \
    PRINT_FUNCTION; \
    ATTACH_AND_CHECK(id, 0); \
    return (int)env->CallStaticIntMethod(m_applicationObjectBaseClass, id); \

#define GET_STRING_FUNCTION(id) \
    PRINT_FUNCTION; \
    ATTACH_AND_CHECK(id, QString()); \
    jstring jpn = (jstring)env->CallStaticObjectMethod(m_applicationObjectBaseClass, id); \
    QString ret = QtAndroid::jstringToQString(env, jpn); \
    env->DeleteLocalRef(jpn); \
    return ret;

#define GET_BOOL_FUNCTION(id, defret) \
    PRINT_FUNCTION; \
    ATTACH_AND_CHECK(id, defret); \
    jboolean ret = env->CallStaticBooleanMethod(m_applicationObjectBaseClass, id); \
    return ret? true: false;


static const char * const cQtApplicationBaseClassPathName = "org/qt/core/QtApplicationBase";

static JavaVM * m_javaVM = 0;
static JNIEnv * m_javaEnvironment = 0;
static jobject m_jniProxyObject = 0;
static jclass m_applicationObjectBaseClass = 0;

// Soft keyboard methods
// static jmethodID m_keyboardStatusMethodID=0;
static jmethodID m_showSoftwareKeyboardMethodID = 0;
static jmethodID m_resetIMMMethodID = 0;
static jmethodID m_getAdvancedSipMethodID = 0;
static jmethodID m_setAdvancedSipMethodID = 0;
static jmethodID m_getDisableFullscreenSipMethodID = 0;
static jmethodID m_setDisableFullscreenSipMethodID = 0;

// Misc methods
static jmethodID m_systemExitMethodID = 0;
static jmethodID m_exitToMainMenuMethodID = 0;
static jmethodID m_getCurrentPluginID = 0;
static jmethodID m_getVersionDataID = 0;
static jmethodID m_getApiLevelID = 0;
static jmethodID m_lowOnMemoryID = 0;
static jmethodID m_crashMessageID = 0;

// APK/Market methods
static jmethodID m_packageNameID = 0;
static jmethodID m_allowNonMarketAppsID = 0;
static jmethodID m_openAppSettingsID = 0;
static jmethodID m_openMarketID = 0;
static jmethodID m_openMarketSelfID = 0;

// Clipboard
static jmethodID m_toClipboardID = 0;
static jmethodID m_fromClipboardID = 0;

// Context menu
static jmethodID m_contextMenuCreateID = 0;
static jmethodID m_contextMenuClearID = 0;
static jmethodID m_contextMenuAddID = 0;
static jmethodID m_contextMenuShowID = 0;

// Toast notifications
static jmethodID m_toastID = 0;

// Show select Input Method
static jmethodID m_inputMethodSelect = 0;

// Misc settings methods
static jmethodID m_openLocationSourceSettingsID = 0;

int qt_android_register_core_java_methods(JavaVM *javaVM, JNIEnv* env)
{
     m_javaVM = javaVM;

    qDebug()<<"Grym Android Core library built"<<__DATE__<<__TIME__;
    qDebug()<<"Registering native methods for class"<<cQtApplicationBaseClassPathName;
    jclass clazz = env->FindClass(cQtApplicationBaseClassPathName);
    if (clazz == NULL)
    {
        qCritical()<<"Native registration unable to find class"<<cQtApplicationBaseClassPathName;
        return JNI_FALSE;
    }

    static const JNINativeMethod coreMethods[] =
    {
        QANDROIDUTILS_JNI_EXPORTS,
        QANDROIDKEYBOARD_JNI_EXPORTS,
        QANDROIDPOINTINGDEVICES_JNI_EXPORTS
    };

    if (env->RegisterNatives(clazz, coreMethods, sizeof(coreMethods)/sizeof(coreMethods[0])) < 0)
    {
        qCritical()<<"RegisterNatives failed for"<<cQtApplicationBaseClassPathName;
        return JNI_FALSE;
    }

    m_applicationObjectBaseClass = static_cast<jclass>(env->NewGlobalRef(clazz));

    // Soft keyboard methods
    m_showSoftwareKeyboardMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_set_sip", "(Z)V");
    m_resetIMMMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_reset_sip", "()V");
    m_getAdvancedSipMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_get_advanced_sip", "()Z");
    m_setAdvancedSipMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_set_advanced_sip", "(Z)V");
    m_getDisableFullscreenSipMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_get_disable_fullscreen_sip", "()Z");
    m_setDisableFullscreenSipMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_set_disable_fullscreen_sip", "(Z)V");


    // Misc methods
    m_systemExitMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_abort_vm", "()V");
    m_exitToMainMenuMethodID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_exit_to_main_menu", "()V");
    m_getCurrentPluginID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_get_current_plugin", "()Ljava/lang/String;");
    m_getVersionDataID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_get_version_data", "()Ljava/lang/String;");
    m_getApiLevelID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_get_api_level", "()I");
    m_lowOnMemoryID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_low_on_memory", "()Z");
    m_crashMessageID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_crash_message", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V");

    // APK/Market methods
    m_packageNameID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_package_name", "()Ljava/lang/String;");
    m_allowNonMarketAppsID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_allow_non_market_apps_installation", "()Z");
    m_openAppSettingsID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_open_app_settings", "()Z");
    m_openMarketID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_open_market", "(Ljava/lang/String;)Z");
    m_openMarketSelfID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_open_market_self", "()Z");

    // Clipboard
    m_toClipboardID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_to_clipboard", "(Ljava/lang/String;)V");
    m_fromClipboardID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_from_clipboard", "()Ljava/lang/String;");

    // Context menu
    m_contextMenuCreateID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_context_menu_create", "(Ljava/lang/String;)V");
    m_contextMenuClearID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_context_menu_clear", "()V");
    m_contextMenuAddID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_context_menu_add", "(Ljava/lang/String;I)V");
    m_contextMenuShowID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_context_menu_show", "(Z)V");

    // Toast notifications
    m_toastID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_toast", "(Ljava/lang/String;Z)V");

    // Show select Input Method
    m_inputMethodSelect = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_input_method_select", "()V");

    // Misc settings
    m_openLocationSourceSettingsID = env->GetStaticMethodID(m_applicationObjectBaseClass, "qt_android_java_open_location_source_settings", "()Z");

    if( !m_showSoftwareKeyboardMethodID ||
        !m_resetIMMMethodID ||
        !m_getAdvancedSipMethodID ||
        !m_setAdvancedSipMethodID ||
        !m_getDisableFullscreenSipMethodID ||
        !m_setDisableFullscreenSipMethodID ||
        !m_systemExitMethodID ||
        !m_exitToMainMenuMethodID ||
        !m_getCurrentPluginID ||
        !m_getVersionDataID ||
        !m_getApiLevelID ||
        !m_lowOnMemoryID ||
        !m_crashMessageID ||
        !m_packageNameID ||
        !m_allowNonMarketAppsID ||
        !m_openAppSettingsID ||
        !m_openMarketID ||
        !m_openMarketSelfID ||
        !m_toClipboardID ||
        !m_fromClipboardID ||
        !m_contextMenuCreateID ||
        !m_contextMenuClearID ||
        !m_contextMenuAddID ||
        !m_contextMenuShowID ||
        !m_toastID ||
        !m_inputMethodSelect ||
        !m_openLocationSourceSettingsID
      )
    {
        qCritical()<<"WARNING: QAndroidCore failed to link to all Java methods!";
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

static volatile bool s_last_show_software_keyboard = false;

Q_ANDROID_EXPORT void qt_android_show_software_keyboard(bool show)
{
    s_last_show_software_keyboard = show;
    ATTACH_AND_CHECK(m_showSoftwareKeyboardMethodID, EMPTY_MACRO);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_showSoftwareKeyboardMethodID, (jboolean)show);
}

// Get last value passed to qt_android_show_software_keyboard()
Q_DECL_EXPORT bool qt_android_last_show_software_keyboard()
{
    return s_last_show_software_keyboard;
}

Q_ANDROID_EXPORT void qt_android_reset_imm()
{
    VOID_FUNCTION(m_resetIMMMethodID);
}

Q_ANDROID_EXPORT void qt_android_set_advanced_sip(bool advanced)
{
    ATTACH_AND_CHECK(m_setAdvancedSipMethodID, EMPTY_MACRO);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_setAdvancedSipMethodID, (jboolean)advanced);
}

Q_ANDROID_EXPORT bool qt_android_get_advanced_sip()
{
    GET_BOOL_FUNCTION(m_getAdvancedSipMethodID, false);
}

Q_ANDROID_EXPORT bool qt_android_get_disable_fullscreen_sip()
{
    GET_BOOL_FUNCTION(m_getDisableFullscreenSipMethodID, true);
}

Q_ANDROID_EXPORT void qt_android_java_set_disable_fullscreen_sip(bool disable)
{
    ATTACH_AND_CHECK(m_setDisableFullscreenSipMethodID, EMPTY_MACRO);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_setDisableFullscreenSipMethodID, (jboolean)disable);
}

Q_ANDROID_EXPORT bool qt_android_system_exit()
{
    VOID_RETBOOL_FUNCTION(m_systemExitMethodID);
}

Q_ANDROID_EXPORT bool qt_android_exit_to_main_menu()
{
    VOID_RETBOOL_FUNCTION(m_exitToMainMenuMethodID);
}

Q_ANDROID_EXPORT QString qt_android_get_current_plugin()
{
    GET_STRING_FUNCTION(m_getCurrentPluginID);
}

Q_ANDROID_EXPORT QString qt_android_get_version_data_string()
{
    GET_STRING_FUNCTION(m_getVersionDataID);
}

Q_ANDROID_EXPORT const QMap<QString, QString>* qt_android_get_version_data()
{
    PRINT_FUNCTION;
    static QMap<QString, QString> ret;
    if( !ret.size() ){
        QString str = qt_android_get_version_data_string();
        QStringList lines = str.split(QChar('\n'), QString::SkipEmptyParts);
        foreach( QString line, lines ){
            QString key = line.section(QChar('='), 0, 0);
            QString value = line.right(line.length() - (key.length()+1));
            ret[key] = value;
            // PRINT_FUNCTION<<key<<"="<<value;
        }
    }
    return &ret;
}

inline int qt_android_get_api_level_impl()
{
    GET_INT_FUNCTION(m_getApiLevelID);
}

Q_ANDROID_EXPORT int qt_android_get_api_level()
{
    static int al = 0;
    if( al < 3 )
        al = qt_android_get_api_level_impl();
    return al;
}

Q_ANDROID_EXPORT bool qt_android_low_on_memory()
{
    GET_BOOL_FUNCTION(m_lowOnMemoryID, false);
}


union UnionJNIEnvToVoid
{
    JNIEnv* nativeEnvironment;
    void* venv;
};


Q_DECL_EXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void*)
{
    qInstallMsgHandler(QtAndroid::Log);
    qDebug()<<"QAndroidCore initialization...";

    UnionJNIEnvToVoid uenv;
    uenv.venv = 0;
    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK)
    {
        qCritical()<<"........GetEnv failed!";
        return -1;
    }
    m_javaEnvironment = uenv.nativeEnvironment;
    m_javaVM = vm;
    if (!qt_android_register_core_java_methods(m_javaVM, m_javaEnvironment))
    {
        qCritical()<<".......registerNatives failed!";
        return -1;
    }
    return JNI_VERSION_1_4;
}

Q_DECL_EXPORT void JNICALL JNI_OnUnload(JavaVM*, void*)
{
    qDebug()<<"androidjnimain JNI_OnUnload begin";
    if( m_javaEnvironment ){
        if( m_applicationObjectBaseClass ){
            m_javaEnvironment->DeleteGlobalRef(m_applicationObjectBaseClass);
            m_applicationObjectBaseClass = 0;
        }
    }
    qDebug()<<"androidjnimain JNI_OnUnload end";
}

Q_ANDROID_EXPORT void qt_android_crash_message(const QString& title, const QString& message, const QString& buttonMessage, bool halt)
{
    JNIEnv* env;
    if (m_javaVM->AttachCurrentThread(&env, NULL)<0) {
        qCritical()<<__FUNCTION__<<"- AttachCurrentThread failed!";
        if( !qt_android_system_exit() )
            abort();
    }
    if( !m_applicationObjectBaseClass || !m_crashMessageID ) {
        qCritical()<<__FUNCTION__<<"- Environment is not set!";
        if( !qt_android_system_exit() )
            abort();
    }

    jstring title_local = QtAndroid::QStringToJstring(env, title);
    jstring message_local = QtAndroid::QStringToJstring(env, message);
    jstring buttonMessage_local = QtAndroid::QStringToJstring(env, buttonMessage);

    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_crashMessageID,
        title_local,
        message_local,
        buttonMessage_local,
        jboolean(halt) );

    env->DeleteLocalRef(title_local);
    env->DeleteLocalRef(message_local);
    env->DeleteLocalRef(buttonMessage_local);
}

Q_ANDROID_EXPORT QString qt_android_package_name()
{
    GET_STRING_FUNCTION(m_packageNameID);
}

Q_ANDROID_EXPORT bool qt_android_allow_non_market_apps_installation()
{
    GET_BOOL_FUNCTION(m_allowNonMarketAppsID, false);
}

Q_ANDROID_EXPORT bool qt_android_open_app_settings()
{
    GET_BOOL_FUNCTION(m_openAppSettingsID, false);
}

Q_ANDROID_EXPORT bool qt_android_open_market(const QString& packageName)
{
    ATTACH_AND_CHECK(m_openMarketID, false);

    jstring packageName_local = QtAndroid::QStringToJstring(env, packageName);
    bool res = (bool)env->CallStaticBooleanMethod(m_applicationObjectBaseClass, m_openMarketID, packageName_local);
    env->DeleteLocalRef(packageName_local);

    return res;
}

Q_ANDROID_EXPORT bool qt_android_open_market_self()
{
    GET_BOOL_FUNCTION(m_openMarketSelfID, false);
}

Q_ANDROID_EXPORT void qt_android_to_clipboard(const QString &text)
{
    ATTACH_AND_CHECK(m_toClipboardID, EMPTY_MACRO);

    jstring text_local = QtAndroid::QStringToJstring(env, text);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_toClipboardID, text_local);
    env->DeleteLocalRef(text_local);
}

Q_ANDROID_EXPORT QString qt_android_from_clipboard()
{
    GET_STRING_FUNCTION(m_fromClipboardID);
}

Q_ANDROID_EXPORT void qt_android_java_context_menu_create(const QString &caption)
{
    ATTACH_AND_CHECK(m_contextMenuCreateID, EMPTY_MACRO);

    jstring caption_local = QtAndroid::QStringToJstring(env, caption);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_contextMenuCreateID, caption_local);
    env->DeleteLocalRef(caption_local);
}

Q_ANDROID_EXPORT void qt_android_java_context_menu_clear()
{
    VOID_FUNCTION(m_contextMenuClearID);
}

Q_ANDROID_EXPORT void qt_android_java_context_menu_add(const QString &text, int item_id)
{
    ATTACH_AND_CHECK(m_contextMenuAddID, EMPTY_MACRO);

    jstring text_local = QtAndroid::QStringToJstring(env, text);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_contextMenuAddID, text_local, (jint)item_id);
    env->DeleteLocalRef(text_local);
}

Q_ANDROID_EXPORT void qt_android_java_context_menu_show(bool show)
{
    ATTACH_AND_CHECK(m_contextMenuShowID, EMPTY_MACRO);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_contextMenuShowID, (jboolean)show);
}

Q_ANDROID_EXPORT void qt_android_toast(const QString &message, bool long_duration)
{
    ATTACH_AND_CHECK(m_toastID, EMPTY_MACRO);

    jstring message_local = QtAndroid::QStringToJstring(env, message);
    env->CallStaticVoidMethod(m_applicationObjectBaseClass, m_toastID, message_local, (jboolean)long_duration);
    env->DeleteLocalRef(message_local);
}

Q_ANDROID_EXPORT void qt_android_input_method_select()
{
    VOID_FUNCTION(m_inputMethodSelect);
}

Q_ANDROID_EXPORT bool qt_android_java_open_location_source_settings()
{
    GET_BOOL_FUNCTION(m_openLocationSourceSettingsID, false);
}

Q_ANDROID_EXPORT JavaVM *qt_android_get_java_vm()
{
    return m_javaVM;
}

Q_ANDROID_EXPORT jobject qt_android_get_jni_proxy_object()
{
   return m_jniProxyObject;
}

Q_ANDROID_EXPORT void qt_android_set_jni_proxy_object(jobject value)
{
   m_jniProxyObject = value;
}
