/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QANDROIDCOMMONJAVA_H
#define QANDROIDCOMMONJAVA_H

#include <jni.h>
#include <QMap>
#include <QString>
#include "qandroidcoreexports.h"


// Returns JNI_TRUE or JNI_FALSE
int qt_android_register_common_java_methods(JavaVM *javaVM, JNIEnv* env);

// Access to Java environment. The JNI proxy must be set for Qt GUI thread from
// libQtAndroidMain. The object must be a global ref which is created and deleted
// elsewhere.
Q_ANDROID_EXPORT JavaVM* qt_android_get_java_vm();
Q_ANDROID_EXPORT jobject qt_android_get_jni_proxy_object();
Q_ANDROID_EXPORT void qt_android_set_jni_proxy_object(jobject value);

Q_ANDROID_EXPORT QString qt_android_get_current_plugin();
Q_ANDROID_EXPORT QString qt_android_get_version_data_string();
Q_ANDROID_EXPORT int qt_android_get_api_level();
Q_ANDROID_EXPORT const QMap<QString, QString>* qt_android_get_version_data();
Q_ANDROID_EXPORT bool qt_android_low_on_memory();

// Show crash message using Java API and terminate application.
// This is mostly for internal use within integration system, when
// there are problems with binary libraries and/or screen system.
// If "halt" is true, the function will never return execution to
// the calling point; it will wait for user response for 1 minute
// max and then kill application.
// If "halt" is false, the function MAY return to the calling point,
// but the app will still be terminated when user clicks the dialog
// button (correct QApplication termination will happen, if possible).
Q_ANDROID_EXPORT void qt_android_crash_message(const QString& title, const QString& message, const QString& buttonMessage, bool halt);

// Some APK/Market functionality
Q_ANDROID_EXPORT QString qt_android_package_name();
Q_ANDROID_EXPORT bool qt_android_allow_non_market_apps_installation();
Q_ANDROID_EXPORT bool qt_android_open_app_settings();
Q_ANDROID_EXPORT bool qt_android_open_market(const QString& packageName);
Q_ANDROID_EXPORT bool qt_android_open_market_self();
Q_ANDROID_EXPORT bool qt_android_java_open_location_source_settings();

// Clipboard
Q_ANDROID_EXPORT void qt_android_to_clipboard(const QString &text);
Q_ANDROID_EXPORT QString qt_android_from_clipboard();

// Context menu.
// Create new context menu.
Q_ANDROID_EXPORT void qt_android_java_context_menu_create(const QString &caption);
Q_ANDROID_EXPORT void qt_android_java_context_menu_clear();
// Add new menu item.
Q_ANDROID_EXPORT void qt_android_java_context_menu_add(const QString &text, int item_id);
Q_ANDROID_EXPORT void qt_android_java_context_menu_show(bool show);

// Toast notifications.
Q_ANDROID_EXPORT void qt_android_toast(const QString &message, bool long_duration);

// Show select Input Method.
Q_ANDROID_EXPORT void qt_android_input_method_select();

#endif


