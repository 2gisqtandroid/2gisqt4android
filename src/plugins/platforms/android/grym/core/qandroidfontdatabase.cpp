/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include "qandroidutils.h"
#include "qandroidfontdatabase.h"
#include "qandroidcorejava.h"

namespace QtAndroid {

QAndroidPlatformFontDatabase::QAndroidPlatformFontDatabase()
{
}

QAndroidPlatformFontDatabase::~QAndroidPlatformFontDatabase()
{
}

QString QAndroidPlatformFontDatabase::fontDir() const
{
    return qt_android_get_font_path();
}

// TODO - support multiple font directories
void QAndroidPlatformFontDatabase::populateFontDatabase()
{
    QPlatformFontDatabase::populateFontDatabase();

    QString fontPath = fontDir();
    QString fontMask = qt_android_get_font_mask();

    if( fontPath.isEmpty() || !QFile::exists(fontPath) ) {
        qCritical()<<"QFontDatabase: Cannot find font directory:"<<fontPath
                   <<"- falling back to default path!";
        fontPath = "/system/fonts";
    }

    if( fontMask.isEmpty() )
       fontMask = "Droid*.ttf"; // Default mask - use only Droid fonts (safer)

    // Start looking for fonts with directory and mask as requested
    QDir fileList(fontPath, fontMask);

    // Some fallback logic, just in case the specified directory is wrong
    if( !fileList.count() ) {
        fileList = QDir(fontPath, "*.ttf");
        if( !fileList.count() ) {
            fileList = QDir("/system/fonts", "Droid*.ttf");
            if( !fileList.count() ) {
                fileList = QDir("/system/fonts", "*.ttf");
                if( !fileList.count() ) {
                    // The translation are not really loaded at this point,
                    // so using translate() is probably vain...
                    qt_android_crash_message(
                                QCoreApplication::translate("Android",
                                    "Fatal Error"),
                                QCoreApplication::translate("Android",
                                    "Could not find system TTF fonts directory."),
                                QCoreApplication::translate("Android",
                                    "Exit"),
                                true);
                }
            }
        }
        qWarning()<<"Fallback font directory:"<<fileList.absolutePath();
        qWarning()<<"Fallback font filters:"<<fileList.nameFilters().join(", ");
    }

    for( unsigned i = 0; i < fileList.count(); i++ ) {
        QString absPath = fileList.absoluteFilePath( fileList[ int(i) ] );
        const QByteArray file = QFile::encodeName( absPath );
        addTTFile( QByteArray(), file );
    }
}

} // namespace QtAndroid
