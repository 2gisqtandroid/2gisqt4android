/*
  Qt/Android Integration Core Library

  Low-level functions exported by Qt Android plugin.
  The ideology has been snapped from here:
  http://doc.trolltech.com/4.7/exportedfunctions.html

  Author: Sergey A. Galin, <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QANDROIDCOMMONEXPORTS_H
#define QANDROIDCOMMONEXPORTS_H

#include <QtCore/qglobal.h>
#include <QObject>
#include <QString>

#if !defined(Q_ANDROID_EXPORT)
#  if defined(QT_SHARED)
#    define Q_ANDROID_EXPORT Q_DECL_EXPORT
#  else
#    define Q_ANDROID_EXPORT
#  endif
#endif

// Control soft keyboard. This function can be called in applications
// explicitly or by using Qt's auto SIP mechanism.
Q_ANDROID_EXPORT void qt_android_show_software_keyboard(bool show);

// Get last value passed to qt_android_show_software_keyboard()
Q_DECL_EXPORT bool qt_android_last_show_software_keyboard();

// Reset currently attached SIP/IMM
Q_ANDROID_EXPORT void qt_android_reset_imm();

// SIP method: advanced or simplified.
// Advanced method works with voice, phonetic input, predictive input, and etc.
// The advantage of simplified method is that it supports any Qt controls
// and never opens keyboard fullscreen.
Q_ANDROID_EXPORT void qt_android_set_advanced_sip(bool advanced);
Q_ANDROID_EXPORT bool qt_android_get_advanced_sip();

// SIP: prevent opening in fullscreen mode (true by default).
Q_ANDROID_EXPORT bool qt_android_get_disable_fullscreen_sip();
Q_ANDROID_EXPORT void qt_android_java_set_disable_fullscreen_sip(bool disable);

// Internal file storage, typically used for configuration
// files and alike. Do not use much space here (people may start
// annoying if you use over ~1 Mb). It is not directly accessible
// by user and automatically removed when application is uninstalled.
Q_ANDROID_EXPORT const QString& qt_android_get_internal_files_path();

// External storage (SD card) path, typically /mnt/sdcard
// Use it to store documents and any big files.
// This can be seen as an equivalent for home directory.
// Directly accessible by user and can be mounted by another
// computer as a USB disk.
Q_ANDROID_EXPORT const QString& qt_android_get_external_storage_path();

// External files location, a directory on SD card where application
// stores its "big" data files, typically it is:
// /mnt/sdcard/Android/data/<package name>/files/
// It is not supposed for direct access by user (although
// technically he can get there).
Q_ANDROID_EXPORT const QString& qt_android_get_external_files_path();

// Cache directory which must be used for temporary Internet files
// and similar stuff. It is located on device's internal storage.
// The files will be deleted by OS if disk space runs low.
// It is not directly accessible by user and automatically removed
// when application is uninstalled.
Q_ANDROID_EXPORT const QString& qt_android_get_cache_path();

// Directory where libraries put into libs/armeabi (or libs/x86
// and etc.) are unpacked.
Q_ANDROID_EXPORT const QString& qt_android_get_apk_lib_path();

// Directory for fonts and search mask.
Q_ANDROID_EXPORT const QString& qt_android_get_font_path();
Q_ANDROID_EXPORT const QString& qt_android_get_font_mask();

// Long click support (sends QContextMenu messages)
// WARNING: These functions must be called from QApplication thread!
// E.g.: re-implement QApplication and call them in constructors, or
// call them from you main window, etc.
Q_ANDROID_EXPORT void qt_android_set_long_click_enabled(bool enable);
Q_ANDROID_EXPORT bool qt_android_long_click_enabled();

// Check if application (activity) is currently active on phone screen.
// When application UI thread is not paused (see app configuration
// options on Java side), this functions allows to check if
// application is currently on screen interacting with user or hidden.
// Also, when application is activated or deactivated,
// Application(De)Activated and Window(De)Activated events are sent
// to app instance and current/previous top level winow, so you can
// avoid using this function.
Q_ANDROID_EXPORT bool qt_android_is_application_active();

// Calls System.exit(0) in Java, causing whole VM
// to immediately shut down.
Q_ANDROID_EXPORT bool qt_android_system_exit();

// Exit to main menu without shutting down application (minimize)
Q_ANDROID_EXPORT bool qt_android_exit_to_main_menu();

// Screen size, in pixels (hardware).
// Note: this information is always set before main() is started.
// In some plugins, QDesktopWidget may not return correct values
// until application window is displayed.
Q_ANDROID_EXPORT int qt_android_get_screen_width();
Q_ANDROID_EXPORT int qt_android_get_screen_height();

// Display resolution, in DPI. This information is not 100%
// reliable as it requires attention from device manufacturer.
// Note: this information is always set before main() is started.
// In some plugins, QDesktopWidget may not return correct values
// until application window is displayed.
Q_ANDROID_EXPORT qreal qt_android_get_screen_x_dpi();
Q_ANDROID_EXPORT qreal qt_android_get_screen_y_dpi();
Q_ANDROID_EXPORT qreal qt_android_get_screen_density_dpi();

// Function gives a list of external storages.
Q_ANDROID_EXPORT const QStringList& qt_android_get_external_storages();

#endif
