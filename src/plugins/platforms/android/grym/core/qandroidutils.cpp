/*
  Qt/Android Integration Core Library

  Authors:
    Sergey A. Galin <sergey.galin@gmail.com>
    Ivan 'w23' Avdeev, <marflon@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <unistd.h>
#include <stdlib.h>
#include <android/log.h>
#include <android/bitmap.h>
#include <qdebug.h>
#include <QCoreApplication>
#include <QPointer>
#include <QWidget>
#include <QMainWindow>
#include <QDialog>
#include <QSet>
#include <QString>
#include <QStringList>
#include <QApplication>
#include "qandroidutils.h"
#include "qandroiddpiworkarounds.h"
#include "qandroidcorejava.h"
#include "qandroidcontextmenu.h"

static QString m_androidInternalFilesPath;
static QString m_androidExternalFilesPath;
static QString m_androidExternalStoragePath;
static QString m_androidCachePath;
static QString m_androidApkLibPath;
static QString m_androidFontPath(QLatin1String("/system/fonts"));
static QString m_androidFontMask(QLatin1String("Droid*.ttf"));

static volatile bool m_qtAndroidInMain = false;
static volatile bool m_applicationActive = false;

// Used by qt_android_send_application_(de)activated():
static QPointer<QWidget> m_LastActiveWindow;

// Dispay parameters - these values should be accessible through
// QDesktopWidget as well, however, these values are guarateed
// to be set before main() is started.
static int
        m_screenWidthPixels = 0,
        m_screenHeightPixels = 0;

static qreal
        m_screenXDpi = 0.0f,
        m_screenYDpi = 0.0f,
        m_densityDpi = 0.0f;

namespace QtAndroid {

Q_ANDROID_EXPORT QMutex jniMutex(QMutex::Recursive);

Q_ANDROID_EXPORT QString jstringToQString(JNIEnv *env, jstring str)
{
    if (str == NULL)
        return QString();

    int length = env->GetStringLength(str);
    if (length == 0)
        return QString();

    const jchar* str_ptr = env->GetStringChars(str, NULL);
    if (str_ptr == NULL)
        return QString();

    QString ret = QString((const QChar *)str_ptr, length);
    env->ReleaseStringChars(str, str_ptr);
    return ret;
}

Q_ANDROID_EXPORT jstring QStringToJstring(JNIEnv *env, const QString& str)
{
    jstring ret = env->NewString(str.utf16(), str.length());
    if (env->ExceptionCheck())
        env->ExceptionClear();
    return ret;
}

Q_ANDROID_EXPORT void Log(QtMsgType type, const char *msg)
{
     switch (type)
     {
     case QtDebugMsg:
         __android_log_print(ANDROID_LOG_DEBUG, "Qt", "%s", msg);
         break;
     case QtWarningMsg:
         __android_log_print(ANDROID_LOG_WARN, "Qt", "%s", msg);
         break;
     case QtCriticalMsg:
         __android_log_print(ANDROID_LOG_ERROR, "Qtr", "%s", msg);
         break;
     case QtFatalMsg:
         __android_log_print(ANDROID_LOG_FATAL, "Qt", "%s", msg);
         abort();
     }
}

Q_ANDROID_EXPORT QImage::Format qAndroidBitmapFormat_to_QImageFormat(uint32_t abf)
{
    /*
      ANDROID_BITMAP_FORMAT_NONE = 0,
      ANDROID_BITMAP_FORMAT_RGBA_8888 = 1,
      ANDROID_BITMAP_FORMAT_RGB_565 = 4,
      ANDROID_BITMAP_FORMAT_RGBA_4444 = 7,
      ANDROID_BITMAP_FORMAT_A_8 = 8
    */

    switch(abf)
    {
    case ANDROID_BITMAP_FORMAT_RGB_565:
        return QImage::Format_RGB16;

    case ANDROID_BITMAP_FORMAT_RGBA_8888:
        return QImage::Format_ARGB32_Premultiplied;

    case ANDROID_BITMAP_FORMAT_RGBA_4444:
        qWarning()<<"Warning: untested screen format RGBA 4444!";
        return QImage::Format_ARGB4444_Premultiplied;

    case ANDROID_BITMAP_FORMAT_A_8:
        qCritical()<<"Warning: grayscale video mode is not supported yet!";
        return QImage::Format_Invalid;

    default:
        qCritical()<<"ERROR: Invalid Android bitmap format:"<<abf;
        return QImage::Format_Invalid;
    }
}

} // namespace QtAndroid

Q_ANDROID_EXPORT void qt_android_set_in_main(bool inMain)
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    m_qtAndroidInMain = inMain;
}

Q_ANDROID_EXPORT bool qt_android_is_in_main()
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    return m_qtAndroidInMain;
}

Q_ANDROID_EXPORT const QString& qt_android_get_internal_files_path()
{
    return m_androidInternalFilesPath;
}

Q_ANDROID_EXPORT const QString& qt_android_get_external_files_path()
{
    return m_androidExternalFilesPath;
}

Q_ANDROID_EXPORT const QString& qt_android_get_external_storage_path()
{
    return m_androidExternalStoragePath;
}

Q_ANDROID_EXPORT const QString& qt_android_get_cache_path()
{
    return m_androidCachePath;
}

Q_ANDROID_EXPORT const QString& qt_android_apk_lib_path()
{
    return m_androidApkLibPath;
}

Q_ANDROID_EXPORT const QString& qt_android_get_font_path()
{
    return m_androidFontPath;
}

Q_ANDROID_EXPORT const QString& qt_android_get_font_mask()
{
    return m_androidFontMask;
}


Q_DECL_EXPORT void qt_android_set_internal_files_path(JNIEnv* env, jclass, jstring string)
{
    m_androidInternalFilesPath = QtAndroid::jstringToQString(env, string);

    putenv(("QT_ANDROID_STORAGE="+m_androidInternalFilesPath).toUtf8().data());

    qDebug()<<"Set internal files directory:"<<m_androidInternalFilesPath;
}

Q_DECL_EXPORT void qt_android_set_external_files_path(JNIEnv* env, jclass, jstring string)
{
    m_androidExternalFilesPath = QtAndroid::jstringToQString(env, string);

    putenv(("TMPDIR="+m_androidExternalFilesPath).toUtf8().data());
    putenv(("QT_ANDROID_EXTERNAL_FILES="+m_androidInternalFilesPath).toUtf8().data());

    qDebug()<<"Set external files directory:"<<m_androidExternalFilesPath;
}

Q_DECL_EXPORT void qt_android_set_external_storage_path(JNIEnv* env, jclass, jstring string)
{
    m_androidExternalStoragePath = QtAndroid::jstringToQString(env, string);

    // Setting $HOME, this makes some Qt directory functions work correctly.
    putenv(("HOME="+m_androidExternalStoragePath).toUtf8().data());
    putenv(("EXTERNAL_STORAGE="+m_androidExternalStoragePath).toUtf8().data());

    qDebug()<<"Set external storage directory:"<<m_androidExternalStoragePath;
}

Q_DECL_EXPORT void qt_android_set_cache_path(JNIEnv* env, jclass, jstring string)
{
    m_androidCachePath = QtAndroid::jstringToQString(env, string);

    putenv(("QT_ANDROID_CACHE="+m_androidCachePath).toUtf8().data());

    qDebug()<<"Set cache directory:"<<m_androidCachePath;
}

Q_DECL_EXPORT void qt_android_set_apk_lib_path(JNIEnv* env, jclass, jstring string)
{
    m_androidApkLibPath = QtAndroid::jstringToQString(env, string);

    putenv(("QT_ANDROID_APK_LIB="+m_androidApkLibPath).toUtf8().data());

    // Initialize Qt plug-in search paths
    QStringList list;
    list << m_androidApkLibPath;
    list << "/data/local/qt/plugins";
    QCoreApplication::setLibraryPaths( list );

    qDebug()<<"Set apk lib path directory:"<<m_androidApkLibPath;
}

Q_DECL_EXPORT void qt_android_set_font_path(JNIEnv* env, jclass, jstring string)
{
    QString fp = QtAndroid::jstringToQString(env, string);
    m_androidFontPath = fp;
}

Q_DECL_EXPORT void qt_android_set_font_mask(JNIEnv* env, jclass, jstring string)
{
    QString mask = QtAndroid::jstringToQString(env, string);
    m_androidFontMask = mask;
}

Q_DECL_EXPORT void qt_android_set_locale(JNIEnv* env, jclass, jstring string)
{
    QString lc = QtAndroid::jstringToQString(env, string);
    putenv(("LANG="+lc).toUtf8().data());
}

Q_DECL_EXPORT void qt_android_exit_qtmain(JNIEnv*, jclass)
{
    qDebug()<<__FUNCTION__;
    try {
        if ( m_qtAndroidInMain ) {
            qDebug()<<"qt_android_exit_qtmain - main() didn't return yet!";
            // Ask QApplication to exit
            if( QApplication::instance() ){
                qDebug()<<"qt_android_exit_qtmain - Asking Qt application to quit...";
                QCoreApplication::postEvent(
                    QApplication::instance(),
                    new QEvent(QEvent::Quit));
            }else{
                qDebug()<<"qt_android_exit_qtmain - No QApplication instance!";
            }
            // Give main() some time to finish
            for(int i=0; i<50; i++){ // 50x100 ms == 5 seconds
                if( !m_qtAndroidInMain ){
                    qDebug()<<"qt_android_exit_qtmain - main() finished successfully.";
                    return;
                }
                usleep(100*1000);
            }
            qDebug()<<"qt_android_exit_qtmain - main() timed out!";
            return;
        }else{
            qDebug()<<"qt_android_exit_qtmain - main() already exited, that's very good.";
        }
    }catch(...)
    {
        qCritical()<<"qt_android_exit_qtmain - Caught an exception!";
    }
}

Q_DECL_EXPORT void qt_android_send_application_activated(JNIEnv*, jobject)
{
    m_applicationActive = true;
    if (QCoreApplication::instance()) {
        QCoreApplication::postEvent(
                QApplication::instance(),
                new QEvent(QEvent::ApplicationActivate));
        if( m_LastActiveWindow )
            if( QApplication::topLevelWidgets().contains(m_LastActiveWindow) )
                QCoreApplication::postEvent(
                        m_LastActiveWindow,
                        new QEvent(QEvent::WindowActivate));
    }
}

Q_DECL_EXPORT void qt_android_send_application_deactivated(JNIEnv*, jobject)
{
    m_applicationActive = false;
    if (QCoreApplication::instance()) {
        QCoreApplication::postEvent(
                QApplication::instance(),
                new QEvent(QEvent::ApplicationDeactivate));
        m_LastActiveWindow = QApplication::activeWindow();
        if( m_LastActiveWindow )
            QCoreApplication::postEvent(
                    m_LastActiveWindow,
                    new QEvent(QEvent::WindowDeactivate));
    }
}

// See: http://code.google.com/p/android-lighthouse/issues/detail?id=44
Q_ANDROID_EXPORT bool qt_android_is_application_active()
{
    return m_applicationActive;
}

Q_ANDROID_EXPORT int qt_android_get_screen_width()
{
    return m_screenWidthPixels;
}

Q_ANDROID_EXPORT int qt_android_get_screen_height()
{
    return m_screenHeightPixels;
}

Q_ANDROID_EXPORT qreal qt_android_get_screen_x_dpi()
{
    return m_screenXDpi;
}

Q_ANDROID_EXPORT qreal qt_android_get_screen_y_dpi()
{
    return m_screenYDpi;
}

Q_ANDROID_EXPORT qreal qt_android_get_screen_density_dpi()
{
    return m_densityDpi;
}

// This function stashes some screen information. It is always
// called before application started, while setDisplayMetrics()
// may be called later (when QtMainView is created).
Q_DECL_EXPORT void qt_android_set_screen_metrics(JNIEnv*, jclass,
                                          jint widthPixels, jint heightPixels,
                                          jfloat xdpi, jfloat ydpi, jfloat densityDpi)
{
    qreal xd = xdpi, yd = ydpi;
    qDebug()<<
         QString("qt_android_set_screen_metrics: %1x%2, resolution: %3x%4 DPI (logical=%5)")
         .arg(widthPixels)
         .arg(heightPixels)
         .arg(xd)
         .arg(yd)
         .arg(densityDpi);
    m_screenWidthPixels = widthPixels;
    m_screenHeightPixels = heightPixels;
    QtAndroid::fixDpi( widthPixels, heightPixels, &xd, &yd, densityDpi );
    m_screenXDpi = xd;
    m_screenYDpi = yd;
    m_densityDpi = densityDpi;
}

// This method called when item of context menu selected.
Q_DECL_EXPORT jboolean qt_android_java_context_menu_call(JNIEnv*, jclass, jint item_id)
{
    return QtAndroid::QAndroidContextMenu::ContextMenuClicked(static_cast<long>(item_id));
}

// This method called when context menu is closed.
Q_DECL_EXPORT jboolean qt_android_java_context_menu_closed(JNIEnv*, jclass)
{
    return QtAndroid::QAndroidContextMenu::ContextMenuClosed();
}

/*Q_DECL_EXPORT void qt_android_java_context_menu_build(JNIEnv *env, jclass, / *android.view.ContextMenu * /jobject menu)
{
    qDebug() << "qt_android_java_context_menu_create";

    QtAndroid::QAndroidContextMenu *context_menu = QtAndroid::QAndroidContextMenu::GetInstance();

    context_menu->CreateSystemMenu(env, menu);
}*/

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// EXPERIMENTAL - THE WORKAROUND FUNCTIONS BELOW WHICH WILL BE TAKEN OUT IN FUTURE
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// Do not allow creating top-level windows for widgets
// which are not descendats of QMainWindow or QDialog.
// This is more of a hack for now, but it is very effective.
static bool m_androidFilterTlwWidgets = false;
static bool m_androidFilterTlwWidgetInput = false;
static bool m_androidEnableStdObjectNameWhitelist = true;
static QSet<QString> m_tlwClasses, m_tlwObjects;

Q_ANDROID_EXPORT bool qt_android_get_filter_tlw_widgets()
{
    return m_androidFilterTlwWidgets;
}

Q_ANDROID_EXPORT void qt_android_set_filter_tlw_widgets(bool filter)
{
    m_androidFilterTlwWidgets = filter;
}

Q_ANDROID_EXPORT bool qt_android_get_filter_tlw_widget_input()
{
    return m_androidFilterTlwWidgetInput;
}

Q_ANDROID_EXPORT void qt_android_set_filter_tlw_widget_input(bool filter)
{
    m_androidFilterTlwWidgetInput = filter;
}

Q_ANDROID_EXPORT void qt_android_set_tlw_standard_names(bool wl)
{
    m_androidEnableStdObjectNameWhitelist = wl;
}

Q_ANDROID_EXPORT void qt_android_add_tlw_class(const QString& name)
{
    m_tlwClasses += name;
}

Q_ANDROID_EXPORT void qt_android_add_tlw_object(const QString& name)
{
    m_tlwObjects += name;
}

// Check if a widget is of a class which is typically used
// to create top-level windows.
Q_ANDROID_EXPORT bool qt_android_is_tlw_widget( const QWidget* w )
{
    // Note: qobject_cast<> here fails in some cases when it should not,
    // so we got to stick with dynamic_cast<>.
    if(
#ifndef QT_NO_MAINWINDOW
        dynamic_cast<const QMainWindow*>(w)!=NULL ||
#endif
        dynamic_cast<const QDialog*>(w)!=NULL )
    {
        return true;
    }
    if( m_androidEnableStdObjectNameWhitelist ){
        if( w && (
                w->objectName().contains ( "Window", Qt::CaseInsensitive ) ||
                w->objectName().contains ( "Dialog", Qt::CaseInsensitive ) ))
        {
            return true;
        }
    }
    if( m_tlwClasses.contains(w->metaObject()->className()) )
        return true;
    if( m_tlwObjects.contains(w->objectName()) )
        return true;
    return false;
}

