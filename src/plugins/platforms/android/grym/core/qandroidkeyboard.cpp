/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Contributors:
    Bogdan Vatra, <bog_dan_ro@yahoo.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <qdebug.h>
#include <QPointer>
#include <QChar>
#include <QApplication>
#include <QKeyEvent>
#include <QWidget>
#include <QWindowSystemInterface>
#include <QThread>

#include "qandroidutils.h"
#include "qandroidinputcontext.h"
#include "qandroidkeyboard.h"

#define QT_ANDROID_KEYBOARD

static bool s_keyboard_enabled = true;

// Check it we can use keyboard functions at the moment
// (used for Java => Qt calls).
static bool any_keyboard_disable()
{
#if 0
    return
            !qt_android_is_in_main() ||
            !QApplication::instance() ||
            !qt_android_last_show_software_keyboard();
#else

    // Qt application must be running
    if (!qt_android_is_in_main())
        return true;
    // And QApplication must exist
    if (!QApplication::instance())
        return true;
    // Keyboard was not explicitly disabled
    if (!qt_android_is_keyboard_enabled()) {
        return true;
    }
    return false;
#endif
}

static bool soft_keyboard_disable()
{
    if (!qt_android_last_show_software_keyboard())
        return true;
    return any_keyboard_disable();
}

namespace QtAndroid {

KeyboardSemaphore keyboard_semaphore;
bool keyboard_verbose_logs = false;
const char * const keyboard_log_tag = "QAKB";

// See: http://developer.android.com/reference/android/view/KeyEvent.html
int mapAndroidKey(int key)
{
    // A..Z, KEYCODE_A..KEYCODE_Z
    if (key>=0x0000001d && key<=0x00000036) {
        unsigned k = Qt::Key_A + key - 0x0000001d;
        return k;
    }

    // 0..9, KEYCODE_0..KEYCODE_9
    if (key>=0x00000007 && key<=0x00000010) {
        unsigned k = Qt::Key_0 + key - 0x00000007;
        return k;
    }

    // 0..9, KEYCODE_NUMPAD_0..KEYCODE_NUMPAD_9
    if (key>=0x00000090 && key<=0x00000099) {
        unsigned k = Qt::Key_0 + key - 0x00000090;
        return k;
    }

    switch(key)
    {
    // KEYCODE_3D_MODE 0x000000ce

    case 0x00000039: // KEYCODE_ALT_LEFT
    case 0x0000003a: // KEYCODE_ALT_RIGHT
        return Qt::Key_Alt;

    case 0x0000004b: // KEYCODE_APOSTROPHE
        return Qt::Key_Apostrophe;

    // KEYCODE_APP_SWITCH 0x000000bb

    case 0x0000004d: // KEYCODE_AT ('@')
        return Qt::Key_At;

    // KEYCODE_AVR_INPUT 0x000000b6
    // KEYCODE_AVR_POWER 0x000000b5

    case 0x00000004: //KEYCODE_BACK
        // Key_Escape is more portable than Key_Back (in cross-platform
        // apps it is usually already handled exactly as Android's "Back" should),
        // while Key_Back is just a weird "internet keyboard" key which
        // is not handled by most apps at all.
        return Qt::Key_Escape;

    case 0x00000049: // KEYCODE_BACKSLASH
        return Qt::Key_Backslash;

    // KEYCODE_BOOKMARK 0x00000049

    case 0x00000079: // KEYCODE_BREAK
        return Qt::Key_Pause;

    // Game controller buttons:
    // KEYCODE_BUTTON_1 0x000000bc==188..KEYCODE_BUTTON_16 0x000000cb==203
    // KEYCODE_BUTTON_A 96 (0x00000060)..KEYCODE_BUTTON_C 98 (0x00000062)
    // KEYCODE_BUTTON_L1 102 (0x00000066)
    // KEYCODE_BUTTON_L2 104 (0x00000068)
    // KEYCODE_BUTTON_MODE 110 (0x0000006e)
    // KEYCODE_BUTTON_R1 103 (0x00000067)
    // KEYCODE_BUTTON_R2 105 (0x00000069)
    // KEYCODE_BUTTON_SELECT 109 (0x0000006d)
    // KEYCODE_BUTTON_START 108 (0x0000006c)
    // KEYCODE_BUTTON_THUMBL 106 (0x0000006a)
    // KEYCODE_BUTTON_THUMBR 107 (0x0000006b)
    // KEYCODE_BUTTON_X 99 (0x00000063)
    // KEYCODE_BUTTON_Y 100 (0x00000064
    // KEYCODE_BUTTON_Z 101 (0x00000065)

    case 0x000000d2: // KEYCODE_CALCULATOR
        return Qt::Key_Calculator;

    case 0x000000d0: // KEYCODE_CALENDAR
        return Qt::Key_Calendar;

    case 0x00000005: // KEYCODE_CALL
        return Qt::Key_Call;

    case 0x0000001b: // KEYCODE_CAMERA
        return Qt::Key_WebCam;

    case 0x00000073: // KEYCODE_CAPS_LOCK
        return Qt::Key_CapsLock;

    // TV:
    // KEYCODE_CAPTIONS 0x000000af
    // KEYCODE_CHANNEL_DOWN 0x000000a7
    // KEYCODE_CHANNEL_UP 0x000000a6

    case 0x0000001c: // KEYCODE_CLEAR
        return Qt::Key_Clear;

    case 0x00000037: // KEYCODE_COMMA
        return Qt::Key_Comma;

    // KEYCODE_CONTACTS 0x000000cf

    case 0x00000071: // KEYCODE_CTRL_LEFT
    case 0x00000072: // KEYCODE_CTRL_RIGHT
        return Qt::Key_Control;

    case 0x00000043: // KEYCODE_DEL
        return Qt::Key_Backspace; // Suddenly!!!!11

    case 0x00000017: // KEYCODE_DPAD_CENTER
        return Qt::Key_Enter;

    case 0x00000014: // KEYCODE_DPAD_DOWN
        return Qt::Key_Down;

    case 0x00000015: // KEYCODE_DPAD_LEFT
        return Qt::Key_Left;

    case 0x00000016: // KEYCODE_DPAD_RIGHT
        return Qt::Key_Right;

    case 0x00000013: // KEYCODE_DPAD_UP
        return Qt::Key_Up;

    // KEYCODE_DVR 0x000000ad

    case 0x00000006: // KEYCODE_ENDCALL
        return Qt::Key_Hangup;

    case 0x00000042: // KEYCODE_ENTER
        return Qt::Key_Return;

    case 0x00000041: // KEYCODE_ENVELOPE
        return Qt::Key_LaunchMail;

    case 0x00000046: // KEYCODE_EQUALS
        return Qt::Key_Equal;

    case 0x0000006f: // KEYCODE_ESCAPE
        return Qt::Key_Escape;

    case 0x00000040: // KEYCODE_EXPLORER (launch Web browser)
        return Qt::Key_Explorer;

    case 0x00000083: // KEYCODE_F1
        return Qt::Key_F1;
    case 0x00000084: // KEYCODE_F2
        return Qt::Key_F2;
    case 0x00000085: // KEYCODE_F3
        return Qt::Key_F3;
    case 0x00000086: // KEYCODE_F4
        return Qt::Key_F4;
    case 0x00000087: // KEYCODE_F5
        return Qt::Key_F5;
    case 0x00000088: // KEYCODE_F6
        return Qt::Key_F6;
    case 0x00000089: // KEYCODE_F7
        return Qt::Key_F7;
    case 0x0000008a: // KEYCODE_F8
        return Qt::Key_F8;
    case 0x0000008b: // KEYCODE_F9
        return Qt::Key_F9;
    case 0x0000008c: // KEYCODE_F10
        return Qt::Key_F10;
    case 0x0000008d: // KEYCODE_F11
        return Qt::Key_F11;
    case 0x0000008e: // KEYCODE_F12
        return Qt::Key_F12;

    // KEYCODE_FOCUS 0x00000050 - focus the camera

    case 0x0000007d: // KEYCODE_FORWARD
        return Qt::Key_Forward;

    case 0x00000070: // KEYCODE_FORWARD_DEL
        return Qt::Key_Delete;

    // KEYCODE_FUNCTION 0x00000077

    case 0x00000044: // KEYCODE_GRAVE ('`')
        return 0x60;

    // KEYCODE_GUIDE 0x000000ac (TV)

    case 0x0000004f: // KEYCODE_HEADSETHOOK (Used to hang up calls and stop media)
        return Qt::Key_Escape; // ?

    case 0x00000003: // KEYCODE_HOME
        return Qt::Key_Home;

    // KEYCODE_INFO 0x000000a5 (TV)

    case 0x0000007c: // KEYCODE_INSERT
        return Qt::Key_Insert;

    // KEYCODE_LANGUAGE_SWITCH 0x000000cc

    case 0x00000047: // KEYCODE_LEFT_BRACKET
        return Qt::Key_BracketLeft;

    // "Toggles silent or vibrate mode on and off to make
    // the device behave more politely in certain settings
    // such as on a crowded train. On some devices, the
    // key may only operate when long-pressed. "
    // KEYCODE_MANNER_MODE 0x000000cd

    // KEYCODE_MEDIA_CLOSE 0x00000080 "May be used to close a CD tray, for example."

    case 0x0000005a: // KEYCODE_MEDIA_FAST_FORWARD
        return Qt::Key_Forward;

    case 0x00000057: // KEYCODE_MEDIA_NEXT
        return Qt::Key_MediaNext;

    case 0x0000007f: // KEYCODE_MEDIA_PAUSE
        return Qt::Key_MediaPause;

    case 0x0000007e: // KEYCODE_MEDIA_PLAY
        return Qt::Key_MediaPlay;

    case 0x00000055: // KEYCODE_MEDIA_PLAY_PAUSE
        return Qt::Key_MediaTogglePlayPause;

    case 0x00000058: // KEYCODE_MEDIA_PREVIOUS
        return Qt::Key_MediaPrevious;

    case 0x00000082: // KEYCODE_MEDIA_RECORD
        return Qt::Key_MediaRecord;

    case 0x00000059: // KEYCODE_MEDIA_REWIND
        return Qt::Key_AudioRewind;

    case 0x00000056: // KEYCODE_MEDIA_STOP
        return Qt::Key_MediaStop;

    case 0x00000052: // KEYCODE_MENU
        return Qt::Key_TopMenu;

    // KEYCODE_META_LEFT 0x00000075
    // KEYCODE_META_RIGHT 0x00000076

    case 0x00000045: // KEYCODE_MINUS
        return Qt::Key_Minus;

    case 0x0000007b: // KEYCODE_MOVE_END
        return Qt::Key_End;

    case 0x0000007a: // KEYCODE_MOVE_HOME
        return Qt::Key_Home;

    case 0x000000d1: // KEYCODE_MUSIC
        return Qt::Key_Music;

    case 0x0000005b: // EYCODE_MUTE
        return Qt::Key_VolumeMute;

    // KEYCODE_NOTIFICATION 0x00000053

    // "Key code constant: Number modifier key.
    // Used to enter numeric symbols. This key is not Num Lock;
    // it is more like KEYCODE_ALT_LEFT and is interpreted as
    // an ALT key by MetaKeyKeyListener."
    case 0x0000004e: // KEYCODE_NUM
        return Qt::Key_Alt;

    case 0x0000009d: // KEYCODE_NUMPAD_ADD
        return Qt::Key_Plus;

    case 0x0000009f: // KEYCODE_NUMPAD_COMMA
        return Qt::Key_Comma;

    case 0x0000009a: // KEYCODE_NUMPAD_DIVIDE
        return Qt::Key_division;

    case 0x0000009e: // KEYCODE_NUMPAD_DOT
        return Qt::Key_Period;

    case 0x000000a0: // KEYCODE_NUMPAD_ENTER
        return Qt::Key_Enter;

    case 0x000000a1: // KEYCODE_NUMPAD_EQUALS
        return Qt::Key_Equal;

    case 0x000000a2: // KEYCODE_NUMPAD_LEFT_PAREN
        return static_cast<int>('(');

    case 0x0000009b: // KEYCODE_NUMPAD_MULTIPLY
        return Qt::Key_multiply;

    case 0x000000a3: // KEYCODE_NUMPAD_RIGHT_PAREN
        return static_cast<int>(')');

    case 0x0000009c: // KEYCODE_NUMPAD_SUBTRACT
        return Qt::Key_Minus;

    case 0x0000008f: // KEYCODE_NUM_LOCK
        return Qt::Key_NumLock;

    case 0x0000005d: // KEYCODE_PAGE_DOWN
        return Qt::Key_PageDown;

    case 0x0000005c: // KEYCODE_PAGE_UP
        return Qt::Key_PageUp;

    case 0x00000038: // KEYCODE_PERIOD
        return Qt::Key_Period;

    // KEYCODE_PICTSYMBOLS 0x0000005e

    case 0x00000051: // KEYCODE_PLUS
        return Qt::Key_Plus;

    case 0x00000012: // KEYCODE_POUND
        return static_cast<int>('#');

    case 0x0000001a: // KEYCODE_POWER
        return Qt::Key_PowerOff;

    // TV:
    // KEYCODE_PROG_BLUE 0x000000ba
    // KEYCODE_PROG_GREEN 0x000000b8
    // KEYCODE_PROG_RED 0x000000b7
    // KEYCODE_PROG_YELLOW 0x000000b9

    case 0x00000048: // KEYCODE_RIGHT_BRACKET
        return Qt::Key_BracketRight;

    case 0x00000074: // KEYCODE_SCROLL_LOCK
        return Qt::Key_ScrollLock;

    case 0x00000054: // KEYCODE_SEARCH
        return Qt::Key_Search;

    case 0x0000004a: // KEYCODE_SEMICOLON
        return Qt::Key_Semicolon;

    // KEYCODE_SETTINGS 0x000000b0 "Starts the system settings activity."

    case 0x0000003b: // KEYCODE_SHIFT_LEFT
    case 0x0000003c: // KEYCODE_SHIFT_RIGHT
        return Qt::Key_Shift;

    case 0x0000004c: // KEYCODE_SLASH
        return Qt::Key_Slash;

    // "Key code constant: Soft Left key. Usually situated below the
    // display on phones and used as a multi-function feature key
    // for selecting a software defined function shown on the bottom
    // left of the display."
    case 0x00000001: // KEYCODE_SOFT_LEFT
        return Qt::Key_F1;

    // "Key code constant: Soft Right key. Usually situated below the
    // display on phones and used as a multi-function feature key
    // for selecting a software defined function shown on the bottom
    // right of the display."
    case 0x00000002: // KEYCODE_SOFT_RIGHT
        return Qt::Key_F2;

    case 0x0000003e: // KEYCODE_SPACE
        return Qt::Key_Space;

    // Warning - this is also the button in the center of arrow keypad (DPAD)!
    case 0x00000011: // KEYCODE_STAR
        return Qt::Key_Asterisk;

    // TV:
    // KEYCODE_STB_INPUT 0x000000b4
    // KEYCODE_STB_POWER 0x000000b3

    // KEYCODE_SWITCH_CHARSET 0x0000005f "Used to switch character sets (Kanji, Katakana)."

    case 0x0000003f: // KEYCODE_SYM
        return Qt::Key_Meta;

    case 0x00000078: // KEYCODE_SYSRQ
        return Qt::Key_SysReq;

    case 0x0000003d: // KEYCODE_TAB
        return Qt::Key_Tab;

    // KEYCODE_TV 0x000000aa "On TV remotes, switches to viewing live TV."
    // KEYCODE_TV_INPUT 0x000000b2
    // KEYCODE_TV_POWER 0x000000b1

    case 0x00000000: // KEYCODE_UNKNOWN
        return 0; // Return 0 silently

    case 0x00000019: // KEYCODE_VOLUME_DOWN
        return Qt::Key_VolumeDown;

    case 0x000000a4: // KEYCODE_VOLUME_MUTE
        return Qt::Key_VolumeMute;

    case 0x00000018: // KEYCODE_VOLUME_UP
        return Qt::Key_VolumeUp;

    // KEYCODE_WINDOW 0x000000ab (TV)

    case 0x000000a8: // KEYCODE_ZOOM_IN
        return Qt::Key_ZoomIn;

    case 0x000000a9: // KEYCODE_ZOOM_OUT
        return Qt::Key_ZoomOut;

    default:
        qDebug()<<QtAndroid::keyboard_log_tag
               <<QString("mapAndroidKey: no known translation for key code %1")
                  .arg(static_cast<unsigned>(key), 8, 16, QChar('0'));
        return 0;

    }
}

// See: http://developer.android.com/reference/android/view/KeyEvent.html#META_SHIFT_ON
Qt::KeyboardModifiers mapAndroidModifiers(int metaState)
{
    Qt::KeyboardModifiers modifiers = 0;

    if (metaState & 1 || metaState & 0x80) // META_SHIFT_ON, META_SHIFT_RIGHT_ON
        modifiers |= Qt::ShiftModifier;

    // Is it correct?!
    if (metaState & 0x100000) // META_CAPS_LOCK_ON
        modifiers |= Qt::ShiftModifier;

    if (metaState & 2 || metaState & 0x20) // META_ALT_ON, META_ALT_RIGHT_ON
        modifiers |= Qt::AltModifier;

    if (metaState & 0x4000 || metaState & 0x1000) // META_CTRL_RIGHT_ON, META_CTRL_ON
        modifiers |= Qt::ControlModifier;

    if (metaState & 0x10000) // META_META_ON
        modifiers |= Qt::MetaModifier;

    if (QtAndroid::keyboard_verbose_logs) {
        QString mods;
        if (modifiers & Qt::ShiftModifier)
            mods +="SHIFT ";
        if (modifiers & Qt::AltModifier)
            mods +="ALT ";
        if (modifiers & Qt::ControlModifier)
            mods +="CTRL ";
        if (modifiers & Qt::MetaModifier)
            mods +="META ";
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<metaState<<"=>"<<mods;
    }

    return modifiers;
}

// A function which does 99.9% of job for keyDown() & keyUp()
void keyEvent(bool down, jint key, jint unicode, jint modifier, bool repeat)
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    if (any_keyboard_disable()) {
        qDebug()<<QtAndroid::keyboard_log_tag<<
             QString("Ignoring key 0x%1 because keyboard is disabled.")
             .arg(int(key), 4, 16, QChar('0'));
        return;
    }

    int qt_key = QtAndroid::mapAndroidKey(key);
    Qt::KeyboardModifiers modifiers = mapAndroidModifiers(modifier);

    QString text;
    if(unicode)
        text += QChar(static_cast<ushort>(unicode));

    if (!text.isEmpty()) {
        if (!(modifiers & Qt::ShiftModifier))
            text = text.toLower();
        else
            text = text.toUpper();
    }

    QPointer<QWidget> widget = QApplication::focusWidget();
    if (!widget)
        widget = QApplication::activeWindow();

    qDebug()<<QtAndroid::keyboard_log_tag<<
        QString("%1 key: android=0x%2 (%10) qt=>0x%3 unicode: 0x%4 =>'%5' modifiers: 0x%6 => 0x%7 widget: 0x%8 repeated: %9")
        .arg((down)? "keyDown:": "keyUp:")                                   // %1
        .arg(int(key), 4, 16, QChar('0'))                                    // %2
        .arg(int(qt_key), 4, 16, QChar('0'))                                 // %3
        .arg(int(unicode), 4, 16, QChar('0'))                                // %4
        .arg((unicode>=0x20)? text: "(control symbol)")                      // %5
        .arg(int(modifier), 4, 16, QChar('0'))                               // %6
        .arg(int(modifiers), 4, 16, QChar('0'))                              // %7
        .arg((unsigned long)widget.data(), sizeof(QWidget*), 16, QChar('0')) // %8
        .arg(repeat)                                                         // %9
        .arg(key);                                                           // %10

    if (!qt_key && !unicode)
    {
        qDebug()<<QtAndroid::keyboard_log_tag<<"....key ignored";
        return;
    }

    switch (qt_key)
    {
    case Qt::Key_Left:
    case Qt::Key_Right:
    case Qt::Key_Up:
    case Qt::Key_Down:
    case Qt::Key_PageUp:
    case Qt::Key_PageDown:
    case Qt::Key_Home:
    case Qt::Key_End:
        {
            QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
            if (input_context) {
                if (input_context->isComposing()) {
                    qDebug()<<QtAndroid::keyboard_log_tag<<"....key ignored because of active composing text";
                    return;
                }
            }
        }
        break;
    default:
        break;
    }

    // Note: Android does key auto-repeat (automatically repeats key-downs).
    // We only have to set the auto-repeat flag properly.
    QWindowSystemInterface::handleKeyEvent(
        widget,
        (down)?QEvent::KeyPress: QEvent::KeyRelease,
        qt_key,
        modifiers,
        text,
        repeat // bool autorep, please see QKeyEvent::isAutoRepeat()
        // ushort count, please see QKeyEvent::count()
    );

    // This helps a bit on slow devices
    QThread::yieldCurrentThread();
}

} // namespace QtAndroid

Q_DECL_EXPORT bool qt_android_is_keyboard_enabled()
{
    return s_keyboard_enabled;
}

// Disable all JNI=>Qt calls related to keyboard.
// TODO: call this function automatically when QApplication
// is destroyed? Not sure how to it in a good way without
// patching QtCore or QtGui...
Q_DECL_EXPORT void qt_android_set_keyboard_enabled(bool enable)
{
    QMutexLocker locker(&QtAndroid::jniMutex);
    qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__
            <<"Old value:"<<s_keyboard_enabled
            <<"New value:"<<enable;
    if (enable != s_keyboard_enabled) {
        if (s_keyboard_enabled)
            if (qt_android_last_show_software_keyboard())
                qt_android_show_software_keyboard(false);
        s_keyboard_enabled = enable;
    }
}

Q_DECL_EXPORT void qt_android_keyboard_verbose_logs(bool verbose)
{
    QtAndroid::keyboard_verbose_logs = verbose;
}

// Some Android hardware keyboard keys send only key ups.
// Some Qt app, on the other hand, might not expect a key up without key down.
// We use this set to track keys which have been down.
// Used by qt_android_key_down()/qt_android_key_up().
static QSet<jint> s_pressed_keys;

Q_DECL_EXPORT void qt_android_key_down(JNIEnv*, jobject, jint key, jint unicode, jint modifier, jint repeat_count)
{
#if defined(QT_ANDROID_KEYBOARD)
    // (Called from Java UI thread - no need to lock mutex)
    bool repeat = repeat_count > 0;
    // Hardware keys which typically send only keyup's begin to send
    // keydowns when held down for long time. In such case the first
    // keydown already has repeat_count == 1. But Qt app needs first
    // keydown to have autorep == false or it may be handled incorrectly.
    if (repeat && !s_pressed_keys.contains(key)) {
        repeat = false;
    }
    QtAndroid::keyEvent(true, key, unicode, modifier, repeat);
    s_pressed_keys.insert(key);
#else
    Q_UNUSED(key);
    Q_UNUSED(unicode);
    Q_UNUSED(modifier);
    Q_UNUSED(repeat_count);
#endif
}

Q_DECL_EXPORT void qt_android_key_up(JNIEnv*, jobject, jint key, jint unicode, jint modifier, jint repeat_count)
{
#if defined(QT_ANDROID_KEYBOARD)
    // (Called from Java UI thread - no need to lock mutex)
    bool repeat = repeat_count > 0;
    // Some hardware keys don't send KeyDown, so we have to simulate it here for Qt.
    if (!s_pressed_keys.contains(key)) {
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<
           QString("Simulating press of 0x%1").arg(int(key), 4, 16, QChar('0'));
        QtAndroid::keyEvent(true, key, unicode, modifier, false);
    }
    QtAndroid::keyEvent(false, key, unicode, modifier, repeat);
    s_pressed_keys.remove(key);
#else
    Q_UNUSED(key);
    Q_UNUSED(unicode);
    Q_UNUSED(modifier);
    Q_UNUSED(repeat_count);
#endif
}

Q_DECL_EXPORT void qt_android_input_key_event(JNIEnv*, jobject, jint keyCode, jint metaState)
{
#if defined(QT_ANDROID_KEYBOARD)
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return;

        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<(int)keyCode;
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(
                        input_context,
                        "inputKeyEvent",
                        Qt::QueuedConnection,
                        Q_ARG(int,keyCode),
                        Q_ARG(int,metaState));
            locker.unlock();
            QtAndroid::keyboard_semaphore.wait(__FUNCTION__);
        }
    }
#else
    Q_UNUSED(keyCode);
#endif
}

// Basically, it's a wrapper for QAndroidInputContext::inputComposingText()
Q_DECL_EXPORT void qt_android_input_composing_text(JNIEnv* env, jobject, jstring text, jint newCursorPosition)
{
#if defined(QT_ANDROID_KEYBOARD)
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return;

        QString str = QtAndroid::jstringToQString(env, text);
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__
               <<(QString("inputComposingText: \"")+str+"\"")<<"Cursor:"<<newCursorPosition;
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(input_context, "inputComposingText",
                                      Qt::QueuedConnection,
                                      Q_ARG(const QString&,str),
                                      Q_ARG(int, newCursorPosition));
            locker.unlock();
            QtAndroid::keyboard_semaphore.wait(__FUNCTION__);
        }
    }
#else
    Q_UNUSED(env);
    Q_UNUSED(text);
#endif
}

Q_DECL_EXPORT void qt_android_input_commit_text(JNIEnv* env, jobject, jstring text, jint newCursorPosition)
{
#if defined(QT_ANDROID_KEYBOARD)
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return;

        QString str = QtAndroid::jstringToQString(env, text);
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__
               <<(QString("inputCommitText: \"")+str+"\"")<<"Cursor:"<<newCursorPosition;
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(input_context, "inputCommitText",
                                      Qt::QueuedConnection,
                                      Q_ARG(const QString&,str),
                                      Q_ARG(int,newCursorPosition),
                                      Q_ARG(bool,true));
            locker.unlock();
            QtAndroid::keyboard_semaphore.wait(__FUNCTION__);
        }
    }
#else
    Q_UNUSED(env);
    Q_UNUSED(text);
#endif
    qDebug()<<QtAndroid::keyboard_log_tag<<"<<<<"<<__FUNCTION__;
}

Q_DECL_EXPORT void qt_android_input_apply_text(JNIEnv*, jobject)
{
#if defined(QT_ANDROID_KEYBOARD)
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return;
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"APPLY TEXT!";
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if (input_context && input_context->isComposing()) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(input_context, "commitCurrentText",
                                      Qt::QueuedConnection,
                                      Q_ARG(bool,true));
            locker.unlock();
            QtAndroid::keyboard_semaphore.wait(__FUNCTION__);
        }
    }
#endif
}

Q_DECL_EXPORT void qt_android_input_reset_text(JNIEnv*, jobject)
{
#if defined(QT_ANDROID_KEYBOARD)
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return;
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__;
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(input_context, "resetCurrentText",
                                      Qt::QueuedConnection,
                                      Q_ARG(bool,true));
            locker.unlock();
            QtAndroid::keyboard_semaphore.wait(__FUNCTION__);
        }
    }
#endif
}

// Basically, it's a wrapper for input_context->editorText()
Q_DECL_EXPORT jstring qt_android_editor_text(JNIEnv* env, jobject)
{
#if defined(QT_ANDROID_KEYBOARD)
    jstring ret = 0;
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return QtAndroid::QStringToJstring(env, "");

        //qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<">>>>";
        QString text;
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context)
            text = input_context->editorText();
        ret = QtAndroid::QStringToJstring(env, text);
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Result:"<<text;
    }
    return ret;
#else
    return QtAndroid::QStringToJstring(env, "");
#endif
}

// Basically, it's a wrapper for input_context->composingText()
Q_DECL_EXPORT jstring qt_android_composing_text(JNIEnv* env, jobject)
{
#if defined(QT_ANDROID_KEYBOARD)
    jstring ret = 0;
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return QtAndroid::QStringToJstring(env, "");

        // qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<">>>>";
        QString text;
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context)
            text = input_context->composingText();
        ret = QtAndroid::QStringToJstring(env, text);
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<"Result:"<<text;
    }
    return ret;
#else
    return QtAndroid::QStringToJstring(env, "");
#endif
}

Q_DECL_EXPORT jint qt_android_cursor_position(JNIEnv*, jobject)
{
#if defined(QT_ANDROID_KEYBOARD)
    int ret = 0;
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return 0;

        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<">>>>";
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context)
            ret = input_context->cursorPosition();
        qDebug()<<QtAndroid::keyboard_log_tag<<"<<<<"<<__FUNCTION__<<ret;
    }
    return ret;
#else
    return 0;
#endif
}

Q_DECL_EXPORT void qt_android_delete_surrounding_text(JNIEnv*, jobject, jint leftLength, jint rightLength)
{
#if defined(QT_ANDROID_KEYBOARD)
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return;

        // qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<">>>>";
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(input_context, "deleteSurroundingText",
                                      Qt::QueuedConnection,
                                      Q_ARG(int,leftLength),
                                      Q_ARG(int,rightLength));
            locker.unlock();
            QtAndroid::keyboard_semaphore.wait(__FUNCTION__);
        }
    }
#endif
}

static int sSelStart = 0, sSelLength = 0;
static bool sMultilineEditor = false;
static QString sSelText;

Q_DECL_EXPORT jboolean qt_android_update_selection_data(JNIEnv*, jobject)
{
#if defined(QT_ANDROID_KEYBOARD)
    jboolean ret = false;
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return false;

        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<">>>>";

        sSelStart = -1;
        sSelLength = 0;
        sMultilineEditor = false;
        sSelText.clear();
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context) {
            ret = input_context->getSelection(&sSelStart, &sSelLength, &sSelText, &sMultilineEditor);
        } // else "ret" stays "false"
        if (QtAndroid::keyboard_verbose_logs)
            qDebug()<<QtAndroid::keyboard_log_tag<<"<<<<"<<__FUNCTION__<<ret;
    }
    return ret;
#else
    return false;
#endif
}

Q_DECL_EXPORT jint qt_android_get_selection_start(JNIEnv*, jobject)
{
    return sSelStart;
}

Q_DECL_EXPORT jint qt_android_get_selection_length(JNIEnv*, jobject)
{
    return sSelLength;
}

Q_DECL_EXPORT jboolean qt_android_get_is_multiline_edit(JNIEnv*, jobject)
{
    return sMultilineEditor;
}

Q_DECL_EXPORT jstring qt_android_get_selection_text(JNIEnv* env, jobject)
{
    QMutexLocker locker(&QtAndroid::jniMutex); // Is it necessary?
    return QtAndroid::QStringToJstring(env, sSelText);
}

Q_DECL_EXPORT void qt_android_set_selection(JNIEnv*, jobject, jint start, jint length)
{
#if defined(QT_ANDROID_KEYBOARD)
    {
        QMutexLocker locker(&QtAndroid::jniMutex);
        if (soft_keyboard_disable())
            return;
        qDebug()<<QtAndroid::keyboard_log_tag<<__FUNCTION__<<start<<length;
        QPointer<QAndroidInputContext> input_context = QAndroidInputContext::instance();
        if(input_context) {
            QtAndroid::keyboard_semaphore.close();
            QMetaObject::invokeMethod(input_context, "setSelection",
                                      Qt::QueuedConnection,
                                      Q_ARG(int,start),
                                      Q_ARG(int,length));
            locker.unlock();
            QtAndroid::keyboard_semaphore.wait(__FUNCTION__);
        }
    }
#endif
}

