/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtCore/QDebug>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtGui/QWidget>
#include <QtGui/QApplication>
#include <QtGui/QMenu>
#include <qandroidutils.h>
#include "qzorder.h"

#ifndef QT_ANDROID_CORE_NOZORDER

// #define ZSORT_DEBUG

#if defined(ZSORT_DEBUG)
    #define ZSORT_DEBUG_FLAG(x) qDebug()<<"########"<<x;
#else
    #define ZSORT_DEBUG_FLAG(x)
#endif

#if defined(ZSORT_DEBUG)
    static QString CatWidgetNames(QWidgetList& list)
    {
        QString s;
        foreach(QWidget* w, list) {
            s += w->metaObject()->className();
            s += "/";
            s += w->objectName();
            s += " ";
        }
        return s;
    }
#endif

static const QWidget *sActiveWindow=0, *sActivePopupWidget=0;

// Return "true" if "b" must lay over "a", or they have "equal Z".
static bool zOrderAiComparator(const QWidget* a, const QWidget* b)
{
    /*
#ifndef QT_NO_MENU
    // QMenu
    if( qobject_cast<const QMenu*>(b)!=0 )
        return true;
    if( qobject_cast<const QMenu*>(a)!=0 )
        return false;
#endif
     */

    /* Doesn't work properly
    if( b->windowFlags() & Qt::ToolTip == Qt::ToolTip ){
        ZSORT_DEBUG_FLAG("ToolTip");
        return true;
    }
    if( a->windowFlags() & Qt::ToolTip == Qt::ToolTip ){
        ZSORT_DEBUG_FLAG("ToolTip");
        return false;
    }
    */

    // Popup windows, e.g. menus
    if( b==sActivePopupWidget )
        return true;
    if( a==sActivePopupWidget )
        return false;
    /* Doesn't work properly
    if( b->windowFlags() & Qt::Popup == Qt::Popup){
        ZSORT_DEBUG_FLAG("Popup");
        return true;
    }
    if( a->windowFlags() & Qt::Popup == Qt::Popup){
        ZSORT_DEBUG_FLAG("Popup");
        return false;
    }

    // Stay-on-top windows
    if( b->windowFlags() & Qt::WindowStaysOnTopHint == Qt::WindowStaysOnTopHint ){
        ZSORT_DEBUG_FLAG("WindowStaysOnTopHint");
        return true;
    }
    if( a->windowFlags() & Qt::WindowStaysOnTopHint == Qt::WindowStaysOnTopHint ){
        ZSORT_DEBUG_FLAG("WindowStaysOnTopHint");
        return false;
    }

    // Splash screen
    if( b->windowFlags() & Qt::SplashScreen == Qt::SplashScreen ){
        ZSORT_DEBUG_FLAG("SplashScreen");
        return true;
    }
    if( a->windowFlags() & Qt::SplashScreen == Qt::SplashScreen ){
        ZSORT_DEBUG_FLAG("SplashScreen");
        return false;
    }

    // Toolboxes
    if( b->windowFlags() & Qt::Tool == Qt::Tool ){
        ZSORT_DEBUG_FLAG("Tool");
        return true;
    }
    if( a->windowFlags() & Qt::Tool == Qt::Tool ){
        ZSORT_DEBUG_FLAG("Tool");
        return false;
    }
    */

    // Then active window dominates
    if( b==sActiveWindow )
        return true;
    if( a==sActiveWindow )
        return false;

    /*
    if( b->windowFlags() & Qt::SubWindow == Qt::SubWindow ){
        ZSORT_DEBUG_FLAG("SubWindow");
        return true;
    }
    if( a->windowFlags() & Qt::SubWindow == Qt::SubWindow ){
        ZSORT_DEBUG_FLAG("SubWindow");
        return false;
    }*/

    ZSORT_DEBUG_FLAG("Non-swapping");
    return true;
}

namespace QtAndroid {

QWeakPointer<QZOrder> QZOrder::s_Instance;

QZOrder::~QZOrder()
{
}

void QZOrder::zSort(QWidgetList*)
{
}

void QZOrder::setInstance(QSharedPointer<QZOrder> zo)
{
    #if defined(ZSORT_DEBUG)
        qDebug()<<"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Setting Z-Order sort class:"<<zo->metaObject()->className();
    #endif
    s_Instance = zo;
}

void QZOrder::sort(QWidgetList* tlwlist)
{
    QSharedPointer<QZOrder> inst = s_Instance.toStrongRef();
    if( !inst.isNull() ){
        #if defined(ZSORT_DEBUG)
            qDebug()<<"@@@@@@@@@@@@@@@@@@@@@@@ Z-Order sort class:"<<inst->metaObject()->className();
        #endif
        inst->zSort(tlwlist);
    }
}


QZOrderAi::~QZOrderAi()
{
}

void QZOrderAi::zSort(QWidgetList* tlwlist)
{
    if( tlwlist->size() < 2 )
        return;
    // This function is not reenterable, so let's make it locked.
    // QMutexLocker locker(&QtAndroid::jniMutex);
    sActiveWindow = QApplication::activeWindow();
    sActivePopupWidget = QApplication::activePopupWidget();
    #if defined(ZSORT_DEBUG)
        qDebug()<<"@@@@@@@@@@@@@@@@@@@@@@@@@@@@ BEFORE:"<<CatWidgetNames(*tlwlist);
    #endif
    qSort(tlwlist->begin(), tlwlist->end(), ::zOrderAiComparator);
    #if defined(ZSORT_DEBUG)
        qDebug()<<"@@@@@@@@@@@@@@@@@@@@@@@@@@@@ AFTER:"<<CatWidgetNames(*tlwlist);
    #endif
}

} // namespace QtAndroid

#endif
