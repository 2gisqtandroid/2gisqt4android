/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QANDROIDDPIWORKAROUNDS_H
#define QANDROIDDPIWORKAROUNDS_H

#include "qandroidcoreexports.h"

namespace QtAndroid {

// Fix insane DPI values by changing them to some working value.
// This is necessary for some Chinese tablets.
// Better have wrong but working DPI than something which can't work at all.
Q_ANDROID_EXPORT qreal fixDpi(qreal dpi);

// Choose which DPI to use: physical or logical.
// There are many devices with physical DPI set to totally wrong value.
// This function detects such situation and returns logical value.
// If physical DPI seems correct, it returns physical.
Q_ANDROID_EXPORT qreal chooseDpi(qreal physical, qreal logical);

// Fix DPI settings using device name / vendor / returned settings.
// Input: pass w/h/xdpi/ydpi/density reported by OS.
// Output: if xdpi / ydpi require fixing, the variables will be modified.
Q_ANDROID_EXPORT void fixDpi(int w, int h, qreal* xdpi, qreal* ydpi, qreal logicalDpi);

}

#endif
