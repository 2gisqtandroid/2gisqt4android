TARGET = QAndroidCore

QT = core gui
DEFINES = QT_STATICPLUGIN
CONFIG += dll
CONFIG -= opengl
QT_CONFIG -= opengl
DESTDIR = $$QMAKE_LIBDIR_QT
target.path=$$[QT_INSTALL_LIBS]
INSTALLS += target

HEADERS += \
    qandroidclipboard.h \
    qandroidcontextmenu.cpp \
    qandroidcoreexports.h \
    qandroidcorejava.h \
    qandroiddpiworkarounds.h \
    qandroidfontdatabase.h \
    qandroidinputcontext.h \
    qandroidkeyboard.h \
    qandroidlongclicktimer.h \
    qandroidpointingdevices.h \
    qandroidstorages.h \
    qandroidutils.h \
    qzorder.h

SOURCES += \
    qandroidclipboard.cpp \
    qandroidcontextmenu.cpp \
    qandroidcorejava.cpp \
    qandroiddpiworkarounds.cpp \
    qandroidfontdatabase.cpp \
    qandroidinputcontext.cpp \
    qandroidkeyboard.cpp \
    qandroidlongclicktimer.cpp \
    qandroidpointingdevices.cpp \
    qandroidstorages.cpp \
    qandroidutils.cpp \
    qzorder.cpp

QMAKE_CXXFLAGS_RELEASE -= -Os
QMAKE_CXXFLAGS_RELEASE += -O3

include(../../../../qpluginbase.pri)
include(../../../fontdatabases/basicunix/basicunix.pri)
include(../common.pri)

