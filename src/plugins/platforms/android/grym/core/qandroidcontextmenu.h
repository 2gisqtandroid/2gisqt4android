/*
  Qt/Android Integration Core Library

  Authors:
    Alexander A. Saytgalin <s.saytgalin@2gis.ru>
    Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2012, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef QANDROIDCONTEXTMENU_H
#define QANDROIDCONTEXTMENU_H

#include <QPointer>
#include <QList>
#include <jni.h>
#include "qandroidcoreexports.h"

namespace QtAndroid
{

/*
 * Static object which keeps current context menu data.
 * Please note that all context menu functions should be called from QApplication thread!
 */
class QAndroidContextMenu
{
public:
    typedef long ItemId;

    static QAndroidContextMenu* GetInstance() { return &instance; }

    // Set caption for future context menu
    void SetCaption(const QString &caption);

    // Set QObject slot which will be invoked when the menu is closed.
    void SetOnCloseHandler(QObject *obj, const char *on_close);

    // Add menu item with specified caption and QObject slot which
    // will be invoked when the item will be selected.
    ItemId AddItem(const QString &caption, QObject *obj, const char *on_click);

    // Clears all data (internally) (caption, menu items, slots).
    void Clear();

    // Should be called from Java when context menu item has been clicked.
    // Wrapper for (private) Clicked(Item&).
    bool Clicked(const ItemId &id);

    // Should be called from Java when context menu has been closed.
    bool Closed();

    // Call context menu item by ID. Used via qt_android_context_menu_call() from Java.
    // (Wrapper for GetInstance()->Clicked()).
    static bool ContextMenuClicked(long item_id);

    // Call "on closed" handler. Used via qt_android_context_menu_closed() from Java.
    // (Wrapper for GetInstance()->Closed()).
    static bool ContextMenuClosed();

private:
    Q_DISABLE_COPY(QAndroidContextMenu)

    QAndroidContextMenu();
    virtual ~QAndroidContextMenu();

    // Keeps information about one menu item.
    struct Item
    {
        Item()
        {}
        Item(const QString &caption, QObject *obj, const char *on_click)
            : caption(caption)
            , obj(obj)
            , on_click(on_click)
        {}
        QString caption;
        QPointer<QObject> obj; // Object whose slot to be invoked
        QByteArray on_click; // Name of the slot to be invoked
    };

    typedef QList<Item> Items;

    bool Clicked(Item &item);

    QString caption; // Menu caption
    Items menu; // Container with menu items
    QPointer<QObject> onclose_obj; // Object whose slot to be invoked when menu is closed
    QByteArray onclose_slot; // Name of the slot to be invoked when menu is closed

    static QAndroidContextMenu instance;
};

} // namespace QtAndroid

//
// Context menu exported functions.
// Note: all these functions should be called from QApplication thread!
// Please always keep these for backward compatibility.
//

// Start new context menu. This is a backward compatibility / convenience
// wrapper for qt_android_context_menu_create(const QString &, 0, 0).
Q_ANDROID_EXPORT void qt_android_context_menu_create(const QString & caption);

// Start new context menu with "on close" handler.
Q_ANDROID_EXPORT void qt_android_context_menu_create(const QString & caption, QObject * obj, const char * on_close);

// Add new item. Returns item ID (which is quite useless now - except for
// debugging purposes, but kept for compatibility).
Q_ANDROID_EXPORT long qt_android_context_menu_add(const QString & caption, QObject * obj, const char * on_click);

// Show context menu.
Q_ANDROID_EXPORT void qt_android_context_menu_exec();

// Cancel (close) context menu. Safe to call if no context menu is shown.
Q_ANDROID_EXPORT void qt_android_context_menu_cancel();

#endif // QANDROIDCONTEXTMENU_H
