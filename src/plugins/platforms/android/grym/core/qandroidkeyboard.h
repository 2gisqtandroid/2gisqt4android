/*
  Qt/Android Integration Core Library

  Author: Sergey A. Galin <sergey.galin@gmail.com>

  Distrbuted under The BSD License

  Copyright (c) 2010, 2011, DoubleGIS, LLC.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the DoubleGIS, LLC nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QANDROIDKEYBOARD_H
#define QANDROIDKEYBOARD_H

#include <unistd.h>
#include <jni.h>
#include <qglobal.h>

namespace QtAndroid {


extern const char * const keyboard_log_tag;
extern bool keyboard_verbose_logs;

// Simple sinlock-based semaphore used to wait
// for Qt keyboard event processing in Qt's UI thread.
// Usage is so much simple to understand:
// In Android thread:
//   0. close()
//   1. Post event to UI thread
//   2. wait()
// In Qt UI thread:
//   0. When event is processed, open()
class KeyboardSemaphore
{
private:
    static const long c_timeout_usec = 2*1000000;
    static const long c_sleep_usec = 250;
public:
    KeyboardSemaphore()
        : mClosed(false)
    {}

    // Close the semaphore.
    void close()
    {
        mClosed = true;
    }

    // Open the semaphore.
    void open()
    {
        mClosed = false;
    }

    // Stop execution until the semaphore is open (or timeout).
    void wait(const char * const description = "")
    {
        long count = c_timeout_usec;
        while (mClosed) {
            usleep(c_sleep_usec);
            count -= c_sleep_usec;
            if (count <=0) {
                qWarning()<<keyboard_log_tag
                        <<"Keyboard semaphore timeout!"
                        <<description;
                break;
            }
        }
    }

private:
    volatile bool mClosed;
};

extern KeyboardSemaphore keyboard_semaphore;


// Convert Android keycode to Qt keycode; returns 0 if input code is not supported
int mapAndroidKey(int key);

// Convert Android meta key state to Qt's keyboard modifiers
Qt::KeyboardModifiers mapAndroidModifiers(int metaState);

// Send key event to Qt application:
// down==true - key pressed, down==false - key released
// key - Android key code
// unicode - UNICODE character which correspons to the key, as sent from Android OS
// modifier - Android modifier keys flag
// repeat_count - number of key auto repeat (0 for initial key press)
void keyEvent(bool down, jint key, jint unicode, jint modifier=0, bool repeat=false);

} // namespace QtAndroid

Q_DECL_EXPORT bool qt_android_is_keyboard_enabled();
Q_DECL_EXPORT void qt_android_set_keyboard_enabled(bool enable);

Q_DECL_EXPORT void qt_android_keyboard_verbose_logs(bool verbose);

Q_DECL_EXPORT void qt_android_key_down(JNIEnv*, jobject, jint key, jint unicode, jint modifier=0, jint repeat_count=0);
Q_DECL_EXPORT void qt_android_key_up(JNIEnv*, jobject, jint key, jint unicode, jint modifier=0, jint repeat_count=0);
Q_DECL_EXPORT void qt_android_input_key_event(JNIEnv*, jobject, jint keyCode, jint MetaState);
Q_DECL_EXPORT void qt_android_input_composing_text(JNIEnv* env, jobject, jstring text, jint newCursorPosition);
Q_DECL_EXPORT void qt_android_input_commit_text(JNIEnv* env, jobject, jstring text, jint newCursorPosition);
Q_DECL_EXPORT void qt_android_input_apply_text(JNIEnv* env, jobject);
Q_DECL_EXPORT void qt_android_input_reset_text(JNIEnv* env, jobject);
Q_DECL_EXPORT jstring qt_android_editor_text(JNIEnv* env, jobject);
Q_DECL_EXPORT jstring qt_android_composing_text(JNIEnv* env, jobject);
Q_DECL_EXPORT jint qt_android_cursor_position(JNIEnv*, jobject);
Q_DECL_EXPORT void qt_android_delete_surrounding_text(JNIEnv*, jobject, jint leftLength, jint rightLength);

// Returns false if no widget selected or selection is not supported.
Q_DECL_EXPORT jboolean qt_android_update_selection_data(JNIEnv* env, jobject);

// qt_android_update_selection_data() should be called immediately before these 3 functions
// Returns -1 if no selection.
Q_DECL_EXPORT jint qt_android_get_selection_start(JNIEnv* env, jobject); // Update data first!
Q_DECL_EXPORT jint qt_android_get_selection_length(JNIEnv* env, jobject); // Update data first!
Q_DECL_EXPORT jboolean qt_android_get_is_multiline_edit(JNIEnv*, jobject);  // Update data first!
Q_DECL_EXPORT jstring qt_android_get_selection_text(JNIEnv* env, jobject); // Update data first!

Q_DECL_EXPORT void qt_android_set_selection(JNIEnv* env, jobject, jint start, jint length);

#define QANDROIDKEYBOARD_JNI_EXPORTS \
    {"qt_android_key_down", "(IIII)V", (void*)qt_android_key_down}, \
    {"qt_android_key_up", "(IIII)V", (void*)qt_android_key_up}, \
    {"qt_android_input_key_event", "(II)V", (void*)qt_android_input_key_event}, \
    {"qt_android_input_composing_text", "(Ljava/lang/String;I)V", (void*)qt_android_input_composing_text}, \
    {"qt_android_input_commit_text", "(Ljava/lang/String;I)V", (void*)qt_android_input_commit_text}, \
    {"qt_android_input_apply_text", "()V", (void*)qt_android_input_apply_text}, \
    {"qt_android_input_reset_text", "()V", (void*)qt_android_input_reset_text}, \
    {"qt_android_editor_text", "()Ljava/lang/String;", (void*)qt_android_editor_text}, \
    {"qt_android_composing_text", "()Ljava/lang/String;", (void*)qt_android_composing_text}, \
    {"qt_android_cursor_position", "()I", (void*)qt_android_cursor_position}, \
    {"qt_android_delete_surrounding_text", "(II)V", (void*)qt_android_delete_surrounding_text}, \
    {"qt_android_update_selection_data", "()Z", (void*)qt_android_update_selection_data}, \
    {"qt_android_get_selection_start", "()I", (void*)qt_android_get_selection_start}, \
    {"qt_android_get_selection_length", "()I", (void*)qt_android_get_selection_length}, \
    {"qt_android_get_is_multiline_edit", "()Z", (void*)qt_android_get_is_multiline_edit}, \
    {"qt_android_get_selection_text", "()Ljava/lang/String;", (void*)qt_android_get_selection_text}, \
    {"qt_android_set_selection", "(II)V", (void*)qt_android_set_selection}

#endif
