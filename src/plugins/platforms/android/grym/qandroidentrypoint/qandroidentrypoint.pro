TEMPLATE = lib
TARGET = QAndroidEntryPoint
QT = core
include($$QT_SOURCE_TREE/src/qbase.pri)
VERSION=1.0.0
LIBS += -lQAndroidCore
CONFIG+= static staticlib warn_on
CONFIG-= qt shared create_prl
SOURCES= qandroidentrypoint.cpp
INCLUDEPATH+= $$QMAKE_INCDIR_QT $$QMAKE_INCDIR_QT/QtCore
DEPENDS += $$QMAKE_LIBDIR_QT/libQAndroidCore.so
DESTDIR = $$QMAKE_LIBDIR_QT
target.path=$$[QT_INSTALL_LIBS]
INSTALLS += target
