
core - QAndroidCore library used as a base of Grym plugins. It implements all
Qt-Android integration except for framebuffer forwarding (QPA plugin).

lite - a plugin which uses QAndroidCore.

qandroidentrypoint - a static library which must be linked to your Qt
applicaion and provides a JNI entry point for it.

UNSUPPORTED PLUGINS:

mw_grym - version of multi-window plugin written by BogDan Vatra and very
heavily patched by Grym team.


