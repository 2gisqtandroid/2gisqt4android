
CONFIG(android_ndk4) {

    # NDK4
    # message("**** Android JNI graphics: using NDK4")
    CONFIG(android-8){
        LIBS += -ljnigraphics -L$$ANDROID_PLATFORM_PATH_8/lib
    }else{
        # API Level 4-5
        INCLUDEPATH += $$QT_SOURCE_TREE/src/plugins/platforms/android/common/native/include
        SOURCES += $$QT_SOURCE_TREE/src/plugins/platforms/android/common/native/graphics/jni/bitmap.cpp
        CONFIG(android-4){
            LIBS += -landroid_runtime -lsgl -L$$ANDROID_PLATFORM_PATH_4/lib
        } else {
            LIBS += -landroid_runtime -lskia -L$$ANDROID_PLATFORM_PATH_5/lib
        }
    }

} else {

    # NDK5
    # message("**** Android JNI graphics: using NDK5")
    CONFIG(android-4) | CONFIG(android-5) {
        INCLUDEPATH += $$QT_SOURCE_TREE/src/plugins/platforms/android/common/native/include
        SOURCES += $$QT_SOURCE_TREE/src/plugins/platforms/android/common/native/graphics/jni/bitmap.cpp
        CONFIG(android-4) {
             INCLUDEPATH += $$QT_SOURCE_TREE/src/3rdparty/android/precompiled/android-4/arch-arm/include
             INCLUDEPATH += $$QT_SOURCE_TREE/src/3rdparty/android/precompiled/android-4/arch-arm/include/core
             LIBS += -L$$QT_SOURCE_TREE/src/3rdparty/android/precompiled/android-4/arch-arm/lib -landroid_runtime -lsgl
        } else {
             INCLUDEPATH += $$QT_SOURCE_TREE/src/3rdparty/android/precompiled/android-5/arch-arm/include
             INCLUDEPATH += $$QT_SOURCE_TREE/src/3rdparty/android/precompiled/android-5/arch-arm/include/core
             LIBS += -L$$QT_SOURCE_TREE/src/3rdparty/android/precompiled/android-5/arch-arm/lib -landroid_runtime -lskia
        }
    }
    else: LIBS += -ljnigraphics -L$$ANDROID_PLATFORM_PATH_8/lib

}
