
==============================================================================
FEATURES OF mw_grym PLUGIN
==============================================================================

- Numerous bugfixes.
- A number of useful low-level exported functions (see androidexports.h).
- Much better keyboard support (any languages, voice input, predictive input).
- You can put .so files into apk, simply put them into your
  <Android project dir>/libs/armeabi directory, near to your Qt application's
  APK file.
- Application and its main window are activated/deactivated when user
switches from/to application.
- Some optimizations.
- Fixed Java reference leaks.
- Long-press handler (see void qt_android_set_long_click_enabled(bool)), sends
  QContextMenuEvent events.
- OOP-style locking of mutexes.
- ...And some more I can't remember.

==============================================================================
KNOWN ISSUES IN mw_grym PLUGIN
==============================================================================

There are two bugs inherited from "mw" plugin:

1) You can have only one top-level window. This also means that you cannot use
QMessageBox and other standard dialogs. 
Also, note that in Qt any widget with NULL parent spawns a top-level window. 
To workaround that, use this exported function added to mw-grym:

#if defined(Q_OS_ANDROID)
void qt_android_set_filter_tlw_widgets(bool filter);
#endif

int main(int argc, char* argv[])
{
  // Do not create top-level windows for widgets not descendant from 
  // QMainWindow or QDialog:
  qt_android_set_filter_tlw_widgets(true);
  ...
}

Also, you have to use a descendant of QMainWindow or QDialog for your main
window (please note that, for instance, some Qt demos use QWidget for that).

2) You cannot do showFullScreen() on windows, it causes them to go black.

