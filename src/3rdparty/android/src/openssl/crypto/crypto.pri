DEFINES += NO_WINDOWS_BRAINDEATH

INCLUDEPATH *= $$PWD/asn1 $$PWD/evp

SOURCES += $$PWD/aes/aes_core.c \
$$PWD/cryptlib.c \
$$PWD/mem.c \
$$PWD/mem_clr.c \
$$PWD/mem_dbg.c \
$$PWD/cversion.c \
$$PWD/ex_data.c \
$$PWD/cpt_err.c \
$$PWD/ebcdic.c \
$$PWD/uid.c \
$$PWD/o_time.c \
$$PWD/o_str.c \
$$PWD/o_dir.c \
$$PWD/aes/aes_cbc.c \
$$PWD/aes/aes_cfb.c \
$$PWD/aes/aes_ctr.c \
$$PWD/aes/aes_ecb.c \
$$PWD/aes/aes_misc.c \
$$PWD/aes/aes_ofb.c \
$$PWD/aes/aes_wrap.c \
$$PWD/asn1/a_bitstr.c \
$$PWD/asn1/a_bool.c \
$$PWD/asn1/a_bytes.c \
$$PWD/asn1/a_d2i_fp.c \
$$PWD/asn1/a_digest.c \
$$PWD/asn1/a_dup.c \
$$PWD/asn1/a_enum.c \
$$PWD/asn1/a_gentm.c \
$$PWD/asn1/a_i2d_fp.c \
$$PWD/asn1/a_int.c \
$$PWD/asn1/a_mbstr.c \
$$PWD/asn1/a_object.c \
$$PWD/asn1/a_octet.c \
$$PWD/asn1/a_print.c \
$$PWD/asn1/a_set.c \
$$PWD/asn1/a_sign.c \
$$PWD/asn1/a_strex.c \
$$PWD/asn1/a_strnid.c \
$$PWD/asn1/a_time.c \
$$PWD/asn1/a_type.c \
$$PWD/asn1/a_utctm.c \
$$PWD/asn1/a_utf8.c \
$$PWD/asn1/a_verify.c \
$$PWD/asn1/ameth_lib.c \
$$PWD/asn1/asn1_err.c \
$$PWD/asn1/asn1_gen.c \
$$PWD/asn1/asn1_lib.c \
$$PWD/asn1/asn1_par.c \
$$PWD/asn1/asn_mime.c \
$$PWD/asn1/asn_moid.c \
$$PWD/asn1/asn_pack.c \
$$PWD/asn1/bio_asn1.c \
$$PWD/asn1/bio_ndef.c \
$$PWD/asn1/d2i_pr.c \
$$PWD/asn1/d2i_pu.c \
$$PWD/asn1/evp_asn1.c \
$$PWD/asn1/f_enum.c \
$$PWD/asn1/f_int.c \
$$PWD/asn1/f_string.c \
$$PWD/asn1/i2d_pr.c \
$$PWD/asn1/i2d_pu.c \
$$PWD/asn1/n_pkey.c \
$$PWD/asn1/nsseq.c \
$$PWD/asn1/p5_pbe.c \
$$PWD/asn1/p5_pbev2.c \
$$PWD/asn1/p8_pkey.c \
$$PWD/asn1/t_bitst.c \
$$PWD/asn1/t_crl.c \
$$PWD/asn1/t_pkey.c \
$$PWD/asn1/t_req.c \
$$PWD/asn1/t_spki.c \
$$PWD/asn1/t_x509.c \
$$PWD/asn1/t_x509a.c \
$$PWD/asn1/tasn_dec.c \
$$PWD/asn1/tasn_enc.c \
$$PWD/asn1/tasn_fre.c \
$$PWD/asn1/tasn_new.c \
$$PWD/asn1/tasn_prn.c \
$$PWD/asn1/tasn_typ.c \
$$PWD/asn1/tasn_utl.c \
$$PWD/asn1/x_algor.c \
$$PWD/asn1/x_attrib.c \
$$PWD/asn1/x_bignum.c \
$$PWD/asn1/x_crl.c \
$$PWD/asn1/x_exten.c \
$$PWD/asn1/x_info.c \
$$PWD/asn1/x_long.c \
$$PWD/asn1/x_name.c \
$$PWD/asn1/x_nx509.c \
$$PWD/asn1/x_pkey.c \
$$PWD/asn1/x_pubkey.c \
$$PWD/asn1/x_req.c \
$$PWD/asn1/x_sig.c \
$$PWD/asn1/x_spki.c \
$$PWD/asn1/x_val.c \
$$PWD/asn1/x_x509.c \
$$PWD/asn1/x_x509a.c \
$$PWD/bf/bf_cfb64.c \
$$PWD/bf/bf_ecb.c \
$$PWD/bf/bf_enc.c \
$$PWD/bf/bf_ofb64.c \
$$PWD/bf/bf_skey.c \
$$PWD/bio/b_dump.c \
$$PWD/bio/b_print.c \
$$PWD/bio/b_sock.c \
$$PWD/bio/bf_buff.c \
$$PWD/bio/bf_nbio.c \
$$PWD/bio/bf_null.c \
$$PWD/bio/bio_cb.c \
$$PWD/bio/bio_err.c \
$$PWD/bio/bio_lib.c \
$$PWD/bio/bss_acpt.c \
$$PWD/bio/bss_bio.c \
$$PWD/bio/bss_conn.c \
$$PWD/bio/bss_dgram.c \
$$PWD/bio/bss_fd.c \
$$PWD/bio/bss_file.c \
$$PWD/bio/bss_log.c \
$$PWD/bio/bss_mem.c \
$$PWD/bio/bss_null.c \
$$PWD/bio/bss_sock.c \
$$PWD/bn/bn_add.c \
$$PWD/bn/bn_asm.c \
$$PWD/bn/bn_blind.c \
$$PWD/bn/bn_ctx.c \
$$PWD/bn/bn_div.c \
$$PWD/bn/bn_err.c \
$$PWD/bn/bn_exp.c \
$$PWD/bn/bn_exp2.c \
$$PWD/bn/bn_gcd.c \
$$PWD/bn/bn_gf2m.c \
$$PWD/bn/bn_kron.c \
$$PWD/bn/bn_lib.c \
$$PWD/bn/bn_mod.c \
$$PWD/bn/bn_mont.c \
$$PWD/bn/bn_mpi.c \
$$PWD/bn/bn_mul.c \
$$PWD/bn/bn_nist.c \
$$PWD/bn/bn_prime.c \
$$PWD/bn/bn_print.c \
$$PWD/bn/bn_rand.c \
$$PWD/bn/bn_recp.c \
$$PWD/bn/bn_shift.c \
$$PWD/bn/bn_sqr.c \
$$PWD/bn/bn_sqrt.c \
$$PWD/bn/bn_word.c \
$$PWD/buffer/buf_err.c \
$$PWD/buffer/buffer.c \
$$PWD/comp/c_rle.c \
$$PWD/comp/c_zlib.c \
$$PWD/comp/comp_err.c \
$$PWD/comp/comp_lib.c \
$$PWD/conf/conf_api.c \
$$PWD/conf/conf_def.c \
$$PWD/conf/conf_err.c \
$$PWD/conf/conf_lib.c \
$$PWD/conf/conf_mall.c \
$$PWD/conf/conf_mod.c \
$$PWD/conf/conf_sap.c \
$$PWD/des/cbc_cksm.c \
$$PWD/des/cbc_enc.c \
$$PWD/des/cfb64ede.c \
$$PWD/des/cfb64enc.c \
$$PWD/des/cfb_enc.c \
$$PWD/des/des_enc.c \
$$PWD/des/des_old.c \
$$PWD/des/des_old2.c \
$$PWD/des/ecb3_enc.c \
$$PWD/des/ecb_enc.c \
$$PWD/des/ede_cbcm_enc.c \
$$PWD/des/enc_read.c \
$$PWD/des/enc_writ.c \
$$PWD/des/fcrypt.c \
$$PWD/des/fcrypt_b.c \
$$PWD/des/ofb64ede.c \
$$PWD/des/ofb64enc.c \
$$PWD/des/ofb_enc.c \
$$PWD/des/pcbc_enc.c \
$$PWD/des/qud_cksm.c \
$$PWD/des/rand_key.c \
$$PWD/des/read2pwd.c \
$$PWD/des/rpc_enc.c \
$$PWD/des/set_key.c \
$$PWD/des/str2key.c \
$$PWD/des/xcbc_enc.c \
$$PWD/dh/dh_ameth.c \
$$PWD/dh/dh_asn1.c \
$$PWD/dh/dh_check.c \
$$PWD/dh/dh_depr.c \
$$PWD/dh/dh_err.c \
$$PWD/dh/dh_gen.c \
$$PWD/dh/dh_key.c \
$$PWD/dh/dh_lib.c \
$$PWD/dh/dh_pmeth.c \
$$PWD/dsa/dsa_ameth.c \
$$PWD/dsa/dsa_asn1.c \
$$PWD/dsa/dsa_depr.c \
$$PWD/dsa/dsa_err.c \
$$PWD/dsa/dsa_gen.c \
$$PWD/dsa/dsa_key.c \
$$PWD/dsa/dsa_lib.c \
$$PWD/dsa/dsa_ossl.c \
$$PWD/dsa/dsa_pmeth.c \
$$PWD/dsa/dsa_prn.c \
$$PWD/dsa/dsa_sign.c \
$$PWD/dsa/dsa_vrf.c \
$$PWD/dso/dso_dl.c \
$$PWD/dso/dso_dlfcn.c \
$$PWD/dso/dso_err.c \
$$PWD/dso/dso_lib.c \
$$PWD/dso/dso_null.c \
$$PWD/dso/dso_openssl.c \
$$PWD/dso/dso_vms.c \
$$PWD/dso/dso_win32.c \
$$PWD/ec/ec2_mult.c \
$$PWD/ec/ec2_smpl.c \
$$PWD/ec/ec_ameth.c \
$$PWD/ec/ec_asn1.c \
$$PWD/ec/ec_check.c \
$$PWD/ec/ec_curve.c \
$$PWD/ec/ec_cvt.c \
$$PWD/ec/ec_err.c \
$$PWD/ec/ec_key.c \
$$PWD/ec/ec_lib.c \
$$PWD/ec/ec_mult.c \
$$PWD/ec/ec_pmeth.c \
$$PWD/ec/ec_print.c \
$$PWD/ec/eck_prn.c \
$$PWD/ec/ecp_mont.c \
$$PWD/ec/ecp_nist.c \
$$PWD/ec/ecp_smpl.c \
$$PWD/ecdh/ech_err.c \
$$PWD/ecdh/ech_key.c \
$$PWD/ecdh/ech_lib.c \
$$PWD/ecdh/ech_ossl.c \
$$PWD/ecdsa/ecs_asn1.c \
$$PWD/ecdsa/ecs_err.c \
$$PWD/ecdsa/ecs_lib.c \
$$PWD/ecdsa/ecs_ossl.c \
$$PWD/ecdsa/ecs_sign.c \
$$PWD/ecdsa/ecs_vrf.c \
$$PWD/err/err.c \
$$PWD/err/err_all.c \
$$PWD/err/err_prn.c \
$$PWD/evp/bio_b64.c \
$$PWD/evp/bio_enc.c \
$$PWD/evp/bio_md.c \
$$PWD/evp/bio_ok.c \
$$PWD/evp/c_all.c \
$$PWD/evp/c_allc.c \
$$PWD/evp/c_alld.c \
$$PWD/evp/digest.c \
$$PWD/evp/e_aes.c \
$$PWD/evp/e_bf.c \
$$PWD/evp/e_des.c \
$$PWD/evp/e_des3.c \
$$PWD/evp/e_null.c \
$$PWD/evp/e_old.c \
$$PWD/evp/e_rc2.c \
$$PWD/evp/e_rc4.c \
$$PWD/evp/e_rc5.c \
$$PWD/evp/e_xcbc_d.c \
$$PWD/evp/encode.c \
$$PWD/evp/evp_acnf.c \
$$PWD/evp/evp_enc.c \
$$PWD/evp/evp_err.c \
$$PWD/evp/evp_key.c \
$$PWD/evp/evp_lib.c \
$$PWD/evp/evp_pbe.c \
$$PWD/evp/evp_pkey.c \
$$PWD/evp/m_dss.c \
$$PWD/evp/m_dss1.c \
$$PWD/evp/m_ecdsa.c \
$$PWD/evp/m_md4.c \
$$PWD/evp/m_md5.c \
$$PWD/evp/m_mdc2.c \
$$PWD/evp/m_null.c \
$$PWD/evp/m_ripemd.c \
$$PWD/evp/m_sha1.c \
$$PWD/evp/m_sigver.c \
$$PWD/evp/m_wp.c \
$$PWD/evp/names.c \
$$PWD/evp/p5_crpt.c \
$$PWD/evp/p5_crpt2.c \
$$PWD/evp/p_dec.c \
$$PWD/evp/p_enc.c \
$$PWD/evp/p_lib.c \
$$PWD/evp/p_open.c \
$$PWD/evp/p_seal.c \
$$PWD/evp/p_sign.c \
$$PWD/evp/p_verify.c \
$$PWD/evp/pmeth_fn.c \
$$PWD/evp/pmeth_gn.c \
$$PWD/evp/pmeth_lib.c \
$$PWD/hmac/hm_ameth.c \
$$PWD/hmac/hm_pmeth.c \
$$PWD/hmac/hmac.c \
$$PWD/krb5/krb5_asn.c \
$$PWD/lhash/lh_stats.c \
$$PWD/lhash/lhash.c \
$$PWD/md4/md4_dgst.c \
$$PWD/md4/md4_one.c \
$$PWD/md5/md5_dgst.c \
$$PWD/md5/md5_one.c \
$$PWD/modes/cbc128.c \
$$PWD/modes/cfb128.c \
$$PWD/modes/ctr128.c \
$$PWD/modes/ofb128.c \
$$PWD/objects/o_names.c \
$$PWD/objects/obj_dat.c \
$$PWD/objects/obj_err.c \
$$PWD/objects/obj_lib.c \
$$PWD/objects/obj_xref.c \
$$PWD/ocsp/ocsp_asn.c \
$$PWD/ocsp/ocsp_cl.c \
$$PWD/ocsp/ocsp_err.c \
$$PWD/ocsp/ocsp_ext.c \
$$PWD/ocsp/ocsp_ht.c \
$$PWD/ocsp/ocsp_lib.c \
$$PWD/ocsp/ocsp_prn.c \
$$PWD/ocsp/ocsp_srv.c \
$$PWD/ocsp/ocsp_vfy.c \
$$PWD/pem/pem_all.c \
$$PWD/pem/pem_err.c \
$$PWD/pem/pem_info.c \
$$PWD/pem/pem_lib.c \
$$PWD/pem/pem_oth.c \
$$PWD/pem/pem_pk8.c \
$$PWD/pem/pem_pkey.c \
$$PWD/pem/pem_seal.c \
$$PWD/pem/pem_sign.c \
$$PWD/pem/pem_x509.c \
$$PWD/pem/pem_xaux.c \
$$PWD/pem/pvkfmt.c \
$$PWD/pkcs12/p12_add.c \
$$PWD/pkcs12/p12_asn.c \
$$PWD/pkcs12/p12_attr.c \
$$PWD/pkcs12/p12_crpt.c \
$$PWD/pkcs12/p12_crt.c \
$$PWD/pkcs12/p12_decr.c \
$$PWD/pkcs12/p12_init.c \
$$PWD/pkcs12/p12_key.c \
$$PWD/pkcs12/p12_kiss.c \
$$PWD/pkcs12/p12_mutl.c \
$$PWD/pkcs12/p12_npas.c \
$$PWD/pkcs12/p12_p8d.c \
$$PWD/pkcs12/p12_p8e.c \
$$PWD/pkcs12/p12_utl.c \
$$PWD/pkcs12/pk12err.c \
$$PWD/pkcs7/pk7_asn1.c \
$$PWD/pkcs7/pk7_attr.c \
$$PWD/pkcs7/pk7_doit.c \
$$PWD/pkcs7/pk7_lib.c \
$$PWD/pkcs7/pk7_mime.c \
$$PWD/pkcs7/pk7_smime.c \
$$PWD/pkcs7/pkcs7err.c \
$$PWD/rand/md_rand.c \
$$PWD/rand/rand_egd.c \
$$PWD/rand/rand_err.c \
$$PWD/rand/rand_lib.c \
$$PWD/rand/rand_unix.c \
$$PWD/rand/randfile.c \
$$PWD/rc2/rc2_cbc.c \
$$PWD/rc2/rc2_ecb.c \
$$PWD/rc2/rc2_skey.c \
$$PWD/rc2/rc2cfb64.c \
$$PWD/rc2/rc2ofb64.c \
$$PWD/rc4/rc4_enc.c \
$$PWD/rc4/rc4_skey.c \
$$PWD/ripemd/rmd_dgst.c \
$$PWD/ripemd/rmd_one.c \
$$PWD/rsa/rsa_ameth.c \
$$PWD/rsa/rsa_asn1.c \
$$PWD/rsa/rsa_chk.c \
$$PWD/rsa/rsa_eay.c \
$$PWD/rsa/rsa_err.c \
$$PWD/rsa/rsa_gen.c \
$$PWD/rsa/rsa_lib.c \
$$PWD/rsa/rsa_none.c \
$$PWD/rsa/rsa_null.c \
$$PWD/rsa/rsa_oaep.c \
$$PWD/rsa/rsa_pk1.c \
$$PWD/rsa/rsa_pmeth.c \
$$PWD/rsa/rsa_prn.c \
$$PWD/rsa/rsa_pss.c \
$$PWD/rsa/rsa_saos.c \
$$PWD/rsa/rsa_sign.c \
$$PWD/rsa/rsa_ssl.c \
$$PWD/rsa/rsa_x931.c \
$$PWD/sha/sha1_one.c \
$$PWD/sha/sha1dgst.c \
$$PWD/sha/sha256.c \
$$PWD/sha/sha512.c \
$$PWD/sha/sha_dgst.c \
$$PWD/stack/stack.c \
$$PWD/ts/ts_err.c \
$$PWD/txt_db/txt_db.c \
$$PWD/ui/ui_compat.c \
$$PWD/ui/ui_err.c \
$$PWD/ui/ui_lib.c \
$$PWD/ui/ui_openssl.c \
$$PWD/ui/ui_util.c \
$$PWD/x509/by_dir.c \
$$PWD/x509/by_file.c \
$$PWD/x509/x509_att.c \
$$PWD/x509/x509_cmp.c \
$$PWD/x509/x509_d2.c \
$$PWD/x509/x509_def.c \
$$PWD/x509/x509_err.c \
$$PWD/x509/x509_ext.c \
$$PWD/x509/x509_lu.c \
$$PWD/x509/x509_obj.c \
$$PWD/x509/x509_r2x.c \
$$PWD/x509/x509_req.c \
$$PWD/x509/x509_set.c \
$$PWD/x509/x509_trs.c \
$$PWD/x509/x509_txt.c \
$$PWD/x509/x509_v3.c \
$$PWD/x509/x509_vfy.c \
$$PWD/x509/x509_vpm.c \
$$PWD/x509/x509cset.c \
$$PWD/x509/x509name.c \
$$PWD/x509/x509rset.c \
$$PWD/x509/x509spki.c \
$$PWD/x509/x509type.c \
$$PWD/x509/x_all.c \
$$PWD/x509v3/pcy_cache.c \
$$PWD/x509v3/pcy_data.c \
$$PWD/x509v3/pcy_lib.c \
$$PWD/x509v3/pcy_map.c \
$$PWD/x509v3/pcy_node.c \
$$PWD/x509v3/pcy_tree.c \
$$PWD/x509v3/v3_akey.c \
$$PWD/x509v3/v3_akeya.c \
$$PWD/x509v3/v3_alt.c \
$$PWD/x509v3/v3_bcons.c \
$$PWD/x509v3/v3_bitst.c \
$$PWD/x509v3/v3_conf.c \
$$PWD/x509v3/v3_cpols.c \
$$PWD/x509v3/v3_crld.c \
$$PWD/x509v3/v3_enum.c \
$$PWD/x509v3/v3_extku.c \
$$PWD/x509v3/v3_genn.c \
$$PWD/x509v3/v3_ia5.c \
$$PWD/x509v3/v3_info.c \
$$PWD/x509v3/v3_int.c \
$$PWD/x509v3/v3_lib.c \
$$PWD/x509v3/v3_ncons.c \
$$PWD/x509v3/v3_ocsp.c \
$$PWD/x509v3/v3_pci.c \
$$PWD/x509v3/v3_pcia.c \
$$PWD/x509v3/v3_pcons.c \
$$PWD/x509v3/v3_pku.c \
$$PWD/x509v3/v3_pmaps.c \
$$PWD/x509v3/v3_prn.c \
$$PWD/x509v3/v3_purp.c \
$$PWD/x509v3/v3_skey.c \
$$PWD/x509v3/v3_sxnet.c \
$$PWD/x509v3/v3_utl.c \
$$PWD/x509v3/v3err.c
